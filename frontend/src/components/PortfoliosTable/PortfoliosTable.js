import React from "react";
import { NonStyledLink } from "../../components";
import {
  Table,
  TableBody,
  TableHead,
  TableRow,
  TableCell
} from "@material-ui/core";

const columns = [
  "Symbols",
  "Market value",
  "Day chg.",
  "Day chg. %",
  "Total chg.",
  "Total chg. %"
];

const headerToValueMap = {
  Symbols: "Symbols",
  "Market value": "MarketValue",
  "Day chg.": "DayChangeV",
  "Day chg. %": "DayChangeP",
  "Total chg.": "TotalDayChangeV",
  "Total chg. %": "TotalChangeP"
};

const rows = [
  {
    name: "#1 porfolioname",
    id: "aasfasfdsfd",
    Symbols: 2,
    MarketValue: 123,
    DayChangeV: 123,
    DayChangeP: "12%",
    TotalDayChangeV: "12",
    TotalChangeP: "12"
  },
  {
    name: "#2 porfolioname",
    id: "aasfasfsdsfd",
    Symbols: 2,
    MarketValue: 123,
    DayChangeV: 123,
    DayChangeP: "12%",
    TotalDayChangeV: "12",
    TotalChangeP: "12"
  }
];

export const PortfoliosTable = props => (
  <Table>
    <TableHead>
      <TableRow>
        <TableCell>Portfolio Name</TableCell>
        {columns.map(column => (
          <TableCell align="right" key={column}>
            {column}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
    <TableBody>
      {rows.map(row => (
        <TableRow key={row.id}>
          <TableCell component="th" scope="row">
            <NonStyledLink to={`/portfolios/${row.id}`}>
              {row.name}
            </NonStyledLink>
          </TableCell>

          {columns.map(column => (
            <TableCell align="right" key={`${row.id}_${column}`}>
              {row[headerToValueMap[column]]}
            </TableCell>
          ))}
        </TableRow>
      ))}
    </TableBody>
  </Table>
);
