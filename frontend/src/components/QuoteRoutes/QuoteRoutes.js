import React from "react";
import { Route } from "react-router-dom";

import { SearchInput } from "../../components";
import { QuotePage } from "../../containers";
import styled from "styled-components";

const SearchBoxContainer = styled.div`
  background-color: #cce5e5;
  padding: 20px;
  border-radius: 3px;
`;

export const QuoteRoutes = props => {
  return (
    <>
      <Route path={`${props.match.path}/:symbol`} component={QuotePage} />
      <Route
        exact
        path={props.match.path}
        render={() => (
          <SearchBoxContainer>
            <SearchInput />
          </SearchBoxContainer>
        )}
      />
    </>
  );
};
