import React from "react";
import { PortfoliosTable } from "../../containers";
import styled from "styled-components";

const Container = styled.div`
  margin: 20px 15px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: stretch;
`;

export const MyPortfolioPage = () => (
  <Container>
    <PortfoliosTable />
  </Container>
);
