import React from "react";
import { connect } from "react-redux";
import { authenticationActions } from "../../reducers/actions";

export const unconnectedLogoutButton = props => (
  <button onClick={props.signOut}>sign out</button>
);

const mapStateToProps = () => ({});
const mapDispatchToProps = dispatch => ({
  signOut: () => {
    dispatch(authenticationActions.LOGOUT());
  }
});

export const LogoutButton = connect(
  mapStateToProps,
  mapDispatchToProps
)(unconnectedLogoutButton);
