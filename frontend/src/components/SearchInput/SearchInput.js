import React from "react";
import Input from "@material-ui/core/Input";
import Search from "@material-ui/icons/Search";
import "./SearchInput.css";

export const SearchInput = props => (
  <div className="searchInput_container">
    <Search />
    <Input
      placeholder={props.placeholder || "Filter..."}
      onChange={props.handleSearchInputChange}
      classes={{ root: "searchinput_input" }}
    />
  </div>
);
