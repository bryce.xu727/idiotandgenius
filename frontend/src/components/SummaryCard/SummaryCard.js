import React from "react";
import Card from "@material-ui/core/Card";
import Typography from "@material-ui/core/Typography";
import styled from "styled-components";

import { TableBody } from "@material-ui/core";
import { geniusfinanceThemeString } from "../../theme/theme";

const Title = styled.div`
  background-color: ${geniusfinanceThemeString.primary};
  padding: 10px;
  border-radius: 3px 3px 0 0;
  border-bottom: 11px solid #f1f1f1;
`;

const TableRow = styled.tr`
  height: 2em;
  border-bottom: 1px solid #f1f1f1;
  width: 95%;
`;
const TableHead = styled.th`
  font-weight: bold;
  text-align: left;
  padding: 0 20px;
`;

const TableData = styled.td`
  padding-left: 20px;

  @media only screen and (max-width: 700px) {
    width: 50%;
  }
`;

const Table = styled.table`
  width: 100%;
`;

const TableContainer = styled.div`
  display: flex;
  @media only screen and (max-width: 700px) {
    flex-direction: column;
  }
`;
export const SummaryCard = props => {
  return (
    <Card>
      <Title>
        <Typography variant="h3" component="h1" color="secondary">
          Stock Information
        </Typography>
      </Title>
      {props.data && generateTable(props.data)}
    </Card>
  );
};
const usefuldata = [
  "1 Year Target",
  "Forward P/E (1y)",
  "Annualized Dividend",
  "Dividend Payment Date",
  "Earnings Per Share (EPS)",
  "Ex Dividend Date",
  "IPO year",
  "Industry",
  "Name",
  "P/E Ratio",
  "Forward P/E (1y)",
  "Sector",
  "Symbol"
];
const generateTable = data => {
  console.log(data);
  const keys = Object.keys(data)
    .filter(k => usefuldata.includes(k))
    .sort();
  const leftTable = [];
  const rightTable = [];
  const middleValue = Math.ceil(keys.length / 2);
  for (let i = 0; i < middleValue; i++) {
    leftTable.push(
      <TableRow key={i}>
        <TableHead>{keys[i]}</TableHead>
        <TableData>{data[keys[i]] && data[keys[i]]}</TableData>
      </TableRow>
    );
    rightTable.push(
      <TableRow key={i + "2"}>
        <TableHead>
          {i + middleValue < keys.length && keys[i + middleValue]}
        </TableHead>
        <TableData>
          {i + middleValue < keys.length && data[keys[i + middleValue]]}
        </TableData>
      </TableRow>
    );
  }
  return (
    <TableContainer>
      <Table>
        <TableBody>{leftTable}</TableBody>
      </Table>
      <Table>
        <TableBody>{rightTable}</TableBody>
      </Table>
    </TableContainer>
  );
};
