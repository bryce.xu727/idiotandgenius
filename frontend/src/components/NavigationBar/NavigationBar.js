import React from "react";
import "./NavigationBar.css";
import { NavigationItem } from "../NavigationItem/NavigationItem";
import { connect } from "react-redux";
import { authenticationActions } from "../../reducers/actions";
import ExitToApp from "@material-ui/icons/ExitToApp";
import CompanySearch from "./CompanySearch/CompanySearch";
import { DropdownNavigationItem } from "../../containers";

export const unconnectedNavigationBar = props => (
  <>
    <div className="NavigationBar-header">
      <span>
        <h1 className="NavigationBar-title">Genius Finance</h1>
      </span>
      <span>
        <h4 className="NavigationBar-sub-title">
          Your personal stock portfolio management system
        </h4>
      </span>
    </div>
    <div className="NavigationBar">
      <div className="NavigationBarLeft">
        {props.isAuthenticated && (
          <>
            <NavigationItem>Home</NavigationItem>
            <DropdownNavigationItem>Portfolios</DropdownNavigationItem>
            <NavigationItem to="/monitoring">Stock monitoring</NavigationItem>
          </>
        )}
      </div>
      <div className="NavigationBarRight">
        {props.isAuthenticated && (
          <>
            <CompanySearch />
            <ExitToApp
              className="NavigationBar_Logout"
              onClick={props.signOut}
            />
          </>
        )}
      </div>
    </div>
  </>
);

const mapStateToProps = state => ({
  isAuthenticated: true && state.authenticationReducer.authentication.token
});
const mapDispatchToProps = dispatch => ({
  signOut: () => {
    dispatch(authenticationActions.LOGOUT());
  }
});

export const NavigationBar = connect(
  mapStateToProps,
  mapDispatchToProps
)(unconnectedNavigationBar);
