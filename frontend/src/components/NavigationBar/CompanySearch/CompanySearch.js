import React, { Component } from "react";
import { FormControl } from "@material-ui/core";
import Autosuggest from "react-autosuggest";
import "./CompanySearch.css";
import "./companyList.json";
import Search from "@material-ui/icons/Search";
import { withRouter } from "react-router";

var data = require("./companyList.json");
const companys = data;

// Teach Autosuggest how to calculate suggestions for any given input value.
const getSuggestions = value => {
  const inputValue = value.trim().toLowerCase();
  const inputLength = inputValue.length;

  return inputLength === 0
    ? []
    : companys
        .filter(company => company.Name.toLowerCase().includes(inputValue))
        .slice(0, 4);
};

// When suggestion is clicked, Autosuggest needs to populate the input
// based on the clicked suggestion. Teach Autosuggest how to calculate the
// input value for every given suggestion.
const getSuggestionValue = suggestion => suggestion.Symbol;

const renderSuggestion = suggestion => <div>{suggestion.Name}</div>;

class CompanySearch extends Component {
  constructor() {
    super();
    this.state = {
      value: "",
      suggestions: []
    };
  }

  onChange = (event, { newValue }) => {
    this.setState({
      value: newValue
    });
  };

  // Autosuggest will call this function every time you need to update suggestions.
  // You already implemented this logic above, so just use it.
  onSuggestionsFetchRequested = ({ value }) => {
    this.setState({
      suggestions: getSuggestions(value)
    });
  };

  // Autosuggest will call this function every time you need to clear suggestions.
  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: [],
      value: ""
    });
  };

  onSuggestionSelected = (event, { suggestionValue, method }) => {
    const { history, location } = { ...this.props };
    if (method === "click" || method === "enter") {
      if (location.pathname !== `/quote/${suggestionValue}`) {
        history.push("/quote/" + suggestionValue);
      }
      this.setState({ value: "" });
    }
  };

  render() {
    const { value, suggestions } = this.state;
    const inputProps = {
      placeholder: "Search company ...",
      value,
      onChange: this.onChange
    };

    return (
      <div className="company_searchInput_container">
        <Search />
        <FormControl>
          <Autosuggest
            suggestions={suggestions}
            onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
            onSuggestionsClearRequested={this.onSuggestionsClearRequested}
            getSuggestionValue={getSuggestionValue}
            renderSuggestion={renderSuggestion}
            inputProps={inputProps}
            onSuggestionSelected={this.onSuggestionSelected}
          />
        </FormControl>
      </div>
    );
  }
}

const CompanySearchWithRouter = withRouter(CompanySearch);
export default CompanySearchWithRouter;
