import React from "react";
import "./LoginPage.css";
import { connect } from "react-redux";
import Card from "../../containers/Card/Card";

function unconnectedLoginPage() {
  return (
    <div className="container">
      <div className="LoginPage">
        <div style={{ height: "100%" ,image:{
          backgroundImage: 'url(https://source.unsplash.com/user/brycexu/likes)',
        }}}>
          <h1 className="LoginPage-header ">Genius Finance</h1>
          <Card />
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = state => ({
  mystate: state
});

const mapDispatchToProps = () => ({});
export const LoginPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(unconnectedLoginPage);
