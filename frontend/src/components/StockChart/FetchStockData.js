import React, {Component} from 'react';
import HighChartRender from './HighChartRender';

class FetchStockData extends Component {
  constructor(props){
    super(props);
    this.state = {
      timestamp: 'no timestamp yet',
      data: [],
      stock: 'MSFT'
    };
  }

  sortFunction(a, b) {
    if (a[0] === b[0]) {
        return 0;
    }
    else {
        return (a[0] < b[0]) ? -1 : 1;
    }
  }

  componentDidMount(){
  }

  componentWillReceiveProps(newProps){
    let keys = ["CWRTIKDP5BSCITCQ","HOFON6NCZGGNG78W","NIFJZT5X3OWPRKKR"];
    let key = keys[(Math.random()*10).round%keys.length];
    let url2 = 'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol='+newProps.stock+'&outputsize=full&apikey='+key;
    let array = [];

      fetch(url2, {
        headers: new Headers({
          'Content-Type': 'application/json'
        })
      }).then(res => (res.json()))
      .catch(error => console.error('Error:', error))
      .then(response => {
          let data = response["Time Series (Daily)"];
          data && Object.keys(data)
                    .map(i => array.push(
                      [new Date(i).getTime(), 
                      Number(data[i]['1. open']), 
                      Number(data[i]['2. high']), 
                      Number(data[i]['3. low']), 
                      Number(data[i]['4. close']), 
                      Number(data[i]['6. volume'])]
                      ));
          this.setState({
            data: array.sort(this.sortFunction),
            stock: newProps.stock
          });
      });

  }

  render(){
    

    return (<HighChartRender data = {this.state.data} type={this.props.type} stock={this.props.stock} companyName={this.props.companyName} ></HighChartRender>);
  }
}

export default FetchStockData;