import React from "react";
import Add from "@material-ui/icons/Add";
import "./SymbolResultOption.css";
export const SymbolResultOption = props => (
  <div
    className={[props.className, "SymbolResultOption_container"].join(" ")}
    onClick={() => {
      props.onClick(props.symbol);
    }}
  >
    <span className="SymbolResultOption_left">
      <Add className="SymbolResultOption_plus_sign" />
      <span className="SymbolResultOption_symbol">{props.symbol}</span>
    </span>
    <span className="SymbolResultOption_right">{props.industry}</span>
  </div>
);
