import React from "react";
import { Link } from "react-router-dom";

export const NonStyledLink = props => (
  <Link
    to={props.to || "/"}
    style={{ textDecoration: "inherit", color: "inherit" }}
  >
    {props.children}
  </Link>
);
