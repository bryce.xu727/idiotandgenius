import React from "react";
import { Route } from "react-router-dom";

import { MyPortfolioPage } from "../../components";
import { PortfolioPage } from "../../containers";
export const PortfolioRoutes = props => (
  <>
    <Route path={`${props.match.url}`} exact component={MyPortfolioPage} />
    <Route path={`${props.match.url}/:portfolioID`} component={PortfolioPage} />
  </>
);
