import React from "react";
import Card from "@material-ui/core/Card";
import Delete from "@material-ui/icons/Delete";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import { Link } from "react-router-dom";

import styled from "styled-components";

const Row = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
  margin-bottom: 20px;
`;
const DeleteButton = styled(Delete)`
  &:hover {
    color: #ff5546;
    cursor: pointer;
  }
`;

export const MonitoringTaskCard = props => {
  const { onDelete, symbol, condition, target, conditionType, email } = {
    ...props
  };

  return (
    <Card>
      <CardContent>
        <Row>
          <Typography
            variant="h5"
            component="h2"
            color="inherit"
            className="PortfolioTable_title"
          >
            <Link
              to={`/quote/${symbol}`}
              style={{ textDecoration: "none", color: "inherit" }}
            >
              {symbol}
            </Link>
          </Typography>
          <DeleteButton onClick={onDelete} />
        </Row>
        <Typography variant="body3" color="inherit" component="p">
          When the {conditionType === "price" ? "price" : "change rate"} of this
          stock{" "}
          {condition === ">=" ? "larger or equal to " : "less or equal to "}
          {conditionType === "price" ? `$${target}` : `${target}%`}
        </Typography>
        <br />
        <Typography variant="body3" color="inherit" component="p">
          Sent email to {email}
        </Typography>
      </CardContent>
    </Card>
  );
};
