import React from "react";
import "./NavigationItem.css";
import { NavLink } from "react-router-dom";

export const NavigationItem = props => (
  <NavLink
    to={props.to || "/"}
    style={{ textDecoration: "none", color: "inherit" }}
  >
    <div className="NavigationItem"> {props.children}</div>
  </NavLink>
);
