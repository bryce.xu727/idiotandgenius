import React from "react";
import styled from "styled-components";

import Edit from "@material-ui/icons/Edit";

const Container = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
  cursor: pointer;
`;

const Label = styled.label`
  cursor: pointer;
  @media only screen and (max-width: 700px) {
    width: 50%;
    display: none;
  }
`;
export const EditableButton = props => (
  <Container onClick={props.onClick}>
    <Edit />
    <Label>Edit</Label>
  </Container>
);
