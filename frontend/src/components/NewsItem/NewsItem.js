import React from "react";
import styled from "styled-components";

import Typography from "@material-ui/core/Typography";

const Container = styled.div`
  display: flex;
  height: 100px;
  align-items: center;
  padding: 10px;
  border-bottom: 1px solid #f1f1f1;

  &:hover {
    cursor: pointer;
    background-color: #f8f8f8;
  }
`;

const NewsLink = styled.a`
  text-decoration: none;
  color: black;
  &:visited {
    color: grey;
  }
`;

const NewsImg = styled.img`
  height: 95%;
  width: 150px;
  object-fit: cover;
  border-radius: 2px;
  box-shadow: 0 0 5px -2px black;
`;

const NewsText = styled.div`
  display: flex;
  flex-direction: column;
  height: 95%;
  width: calc(100% - 150px - 10px);
  padding: 0 0 0 10px;
`;

const NewsTitle = styled.div`
  width: 100%;
  position: relative;
  flex: 1;
  overflow: hidden;
  word-break: break-all;
  &:before {
    content: "";
    width: 100%;
    height: 100%;
    position: absolute;
    left: 0;
    top: 0;
    background: linear-gradient(to right, transparent calc(100% - 40px), white);
    border-radius: 5px;
  }
`;
const NewsDescription = styled.div`
  position: relative;
  width: 100%;
  flex: 2;
  overflow: hidden;
  &:before {
    content: "";
    width: 100%;
    height: 100%;
    position: absolute;
    left: 0;
    top: 0;
    background: linear-gradient(transparent 95%, #f8f8f8);
  }
`;
export const NewsItem = props => (
  <NewsLink href={props.url}>
    <Container>
      <NewsImg
        src={props.img || "http://jewel1067.com/wp-content/uploads/news.jpg"}
      />
      <NewsText>
        <NewsTitle>
          <Typography variant="h6" component="h1">
            {props.title || "Title"}
          </Typography>
        </NewsTitle>
        <NewsDescription>{props.description || "Description"}</NewsDescription>
      </NewsText>
    </Container>
  </NewsLink>
);
