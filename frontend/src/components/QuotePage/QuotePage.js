import React from "react";
import { geniusfinanceTheme } from "../../theme/theme";
import { SummaryCard } from "../../components";
import { CandleChartCard, NewsSectionCard } from "../../containers";

import styled from "styled-components";
import { ThemeProvider } from "@material-ui/styles";

const JustifyContent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
`;

const CandleChartCardContainer = styled.div`
  width: 95%;
  margin: 20px 0 10px 0;
`;

const NormalContainer = styled.div`
  width: 95%;
  margin: 10px 0;
`;

const StockSymbol = styled.h1`
  font-family: "Roboto", "Helvetica", "Arial", sans-serif;
  font-size: 1.4em;
  font-weight: bold;
  margin: 5px 0;
`;

const StockPrice = styled.h1`
  display: inline-block;
  font-family: "Roboto", "Helvetica", "Arial", sans-serif;
  font-size: 2em;
  width: fit-content;
  margin: auto 10px auto 0;
`;

const StockSubtitle = styled.h2`
  font-family: "Roboto", "Helvetica", "Arial", sans-serif;
  font-size: 0.9em;
  color: grey;
  margin: 5px 0;
`;


export const QuotePage = props => (
  <ThemeProvider theme={geniusfinanceTheme}>
    <JustifyContent>
      <CandleChartCardContainer>
        <CandleChartCard>
          <StockSymbol>Bilibili Inc. (BILI)</StockSymbol>
          <StockPrice>20</StockPrice>
          <StockSubtitle>At close: 5 September 4:00PM AEDT</StockSubtitle>
          <StockSubtitle>USD</StockSubtitle>
        </CandleChartCard>
      </CandleChartCardContainer>
      <NormalContainer>
        <SummaryCard />
      </NormalContainer>
      <NormalContainer>
        <NewsSectionCard
          symbols={[`${props.match.params.symbol}`]}
          emptyWarning="Currently there is no related news"
        />
      </NormalContainer>
    </JustifyContent>
  </ThemeProvider>
);
