import React from "react";
import { connect } from "react-redux";
import { Route, Redirect } from "react-router-dom";

const unconnectedPrivateRoute = ({
  component: Component,
  isAuthenticated,
  ...rest
}) => (
  <Route
    {...rest}
    render={props =>
      isAuthenticated ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: "/login",
            state: { from: props.location }
          }}
        />
      )
    }
  />
);

const mapStateToProps = state => ({
  isAuthenticated: true && state.authenticationReducer.authentication.token
});
const mapDispatchToProps = () => ({});

export const PrivateRoute = connect(
  mapStateToProps,
  mapDispatchToProps
)(unconnectedPrivateRoute);
