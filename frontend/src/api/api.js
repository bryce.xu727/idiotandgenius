import axios from "axios";

export const Registraion = user =>
  axios
    .post("http://localhost:9111/registration", {
      username: user.username,
      password: user.password
    })
    .then(function(response) {
      console.log(response.status);
      return response;
    })
    .catch(function(error) {
      console.log(error);
      throw error;
    });

export const Login = user =>
  axios
    .post("http://localhost:9111/login", {
      username: user.username,
      password: user.password
    })
    .then(function(response) {
      const { access_token: token } = { ...JSON.parse(response.data) };
      return { token };
    })
    .catch(function(error) {
      throw error;
    });

export const getSimplifiedStock = () =>
  axios
    .get("http://localhost:9333/stocks")
    .then(function(response) {
      console.log("success");
      console.log(response.status);
      return response.data;
    })
    .catch(function(error) {
      console.log("fail");
      console.log(error);
      throw error;
    });

export const getStockInfor = symbol =>
  axios
    .get(`http://localhost:9333/stocks/${symbol}/information`)
    .then(response => response.data)
    .catch(error => {
      throw error;
    });

export const getPortfoliosOwnedByUser = ({ userID, token }) =>
  axios
    .get(`http://127.0.0.1:9222/${userID}/portfolio`, {
      headers: { Authorization: "Bearer " + token }
    })
    .then(function(response) {
      console.log("success");
      console.log(response.status);
      if (response.status === 200) {
        return JSON.parse(response.data);
      } else {
        throw response.status;
      }
    })
    .catch(function(error) {
      console.log("fail");
      console.log(error);
      throw error;
    });

export const createPortfoliosForUser = ({ userID, token, portfolioName }) =>
  axios
    .post(
      `http://127.0.0.1:9222/${userID}/portfolio`,
      { portfolioName },{headers: { Authorization: "Bearer " + token }})
    .then(function(response) {
      console.log("success");
      console.log(response.status);
      if (response.status === 200) {
        return response.data;} 
      else {
        throw response.status;}})
    .catch(function(error) {
      console.log("fail");
      console.log(error);
      throw error;
    });

export const deletePortfolioByUser = ({ userID, token, portfolioID }) =>
  axios
    .delete(`http://127.0.0.1:9222/${userID}/portfolio/${portfolioID}`, {
      headers: { Authorization: "Bearer " + token }
    })
    .then(function(response) {
      console.log("success");
      console.log(response.status);
      if (response.status === 200) {
        return response.data;
      } else {
        throw response.status;
      }
    })
    .catch(function(error) {
      console.log("fail");
      console.log(error);
      throw error;
    });

export const getPortfolioOwnedByUser = ({ username, token, portfolioID }) =>
  axios
    .get(`http://127.0.0.1:9222/${username}/portfolio/${portfolioID}`, {
      headers: { Authorization: "Bearer " + token }
    })
    .then(function(response) {
      console.log("success");
      console.log(response.status);
      if (response.status === 200) {
        return JSON.parse(response.data);
      } else {
        throw response.status;
      }
    })
    .catch(function(error) {
      console.log("fail");
      console.log(error);
      throw error;
    });

export const addStockToPortfolioOwnedByUser = ({
  username,
  token,
  portfolioID,
  symbol
}) =>
  axios
    .post(
      `http://127.0.0.1:9222/${username}/portfolio/${portfolioID}/stockwatchlist`,
      {
        operation: "stock",
        symbol
      },
      {
        headers: { Authorization: "Bearer " + token }
      }
    )
    .then(function(response) {
      console.log("success");
      console.log(response.status);
      if (response.status === 200) {
        return response.data;
      } else {
        throw response.status;
      }
    })
    .catch(function(error) {
      console.log("fail");
      console.log(error);
      throw error;
    });

export const deleteStockToPortfolioOwnedByUser = ({
  username,
  token,
  portfolioID,
  symbol
}) =>
  axios
    .delete(
      `http://127.0.0.1:9222/${username}/portfolio/${portfolioID}/stockwatchlist/${symbol}`,
      {
        headers: { Authorization: "Bearer " + token }
      }
    )
    .then(function(response) {
      console.log("success");
      console.log(response.status);
      if (response.status === 200) {
        return response.data;
      } else {
        throw response.status;
      }
    })
    .catch(function(error) {
      console.log("fail");
      console.log(error);
      throw error;
    });

export const addPositionToPortfolio = ({
  username,
  token,
  portfolioID,
  symbol,
  positionSize,
  purchasePrice
}) =>
  axios
    .post(
      `http://127.0.0.1:9222/${username}/portfolio/${portfolioID}/position`,
      {
        positionSize,
        purchasePrice,
        symbol
      },
      {
        headers: { Authorization: "Bearer " + token }
      }
    )
    .then(function(response) {
      console.log("success");
      console.log(response.status);
      if (response.status === 200) {
        return response.data;
      } else {
        throw response.status;
      }
    })
    .catch(function(error) {
      console.log("fail");
      console.log(error);
      throw error;
    });

export const getPortfolioList = ({ username, token }) =>
  axios
    .get(`http://127.0.0.1:9222/${username}/gainlist`, {
      headers: { Authorization: "Bearer " + token }
    })
    .then(function(response) {
      console.log("success");
      console.log(response.status);
      if (response.status === 200) {
        return response.data;
      } else {
        throw response.status;
      }
    })
    .catch(function(error) {
      console.log("fail");
      console.log(error);
      throw error;
    });

export const getPortfolioEquityAllocationUser = ({
  username,
  token,
  portfolioID
}) =>
  axios
    .get(
      `http://127.0.0.1:9222/${username}/portfolio/${portfolioID}/equityallocation`,
      {
        headers: { Authorization: "Bearer " + token }
      }
    )
    .then(function(response) {
      console.log("success");
      console.log(response.status);
      if (response.status === 200) {
        return response.data;
      } else {
        throw response.status;
      }
    })
    .catch(function(error) {
      console.log("fail");
      console.log(error);
      throw error;
    });

export const getPositionGainList = ({ username, portfolioID, token }) =>
  axios
    .get(
      `http://127.0.0.1:9222/${username}/portfolio/${portfolioID}/positiongain`,
      {
        headers: { Authorization: "Bearer " + token }
      }
    )
    .then(function(response) {
      console.log("success");
      console.log(response.status);
      if (response.status === 200) {
        return response.data;
      } else {
        throw response.status;
      }
    })
    .catch(function(error) {
      console.log("fail");
      console.log(error);
      throw error;
    });

export const getPortfolioIdNamePair = ({ username, token }) =>
  axios
    .get(`http://127.0.0.1:9222/${username}/pair`, {
      headers: { Authorization: "Bearer " + token }
    })
    .then(function(response) {
      console.log("success");
      console.log(response.status);
      if (response.status === 200) {
        return response.data;
      } else {
        throw response.status;
      }
    })
    .catch(function(error) {
      console.log("fail");
      console.log(error);
      throw error;
    });

export const getAlarmTasksList = ({ username }) =>
  axios
    .get(`http://127.0.0.1:9333/users/${username}/tasks`)
    .then(function(response) {
      console.log("success");
      console.log(response.status);
      if (response.status === 200) {
        return response.data;
      } else {
        throw response.status;
      }
    })
    .catch(function(error) {
      console.log("fail");
      console.log(error);
      throw error;
    });

export const deleteAlarmTask = ({ username, taskID }) =>
  axios
    .delete(`http://127.0.0.1:9333/users/${username}/tasks/${taskID}`)
    .then(function(response) {
      console.log("success");
      console.log(response.status);
      if (response.status === 200) {
        return response.data;
      } else {
        throw response.status;
      }
    })
    .catch(function(error) {
      console.log("fail");
      console.log(error);
      throw error;
    });

export const addAlarmTask = ({
  username,
  symbol,
  condition,
  target,
  conditionType,
  email}) =>
  axios
    .post(`http://127.0.0.1:9333/users/${username}/tasks`, {
      symbol,
      condition,
      target,
      conditionType,
      email})
    .then(function(response) {
      console.log("success");
      console.log(response.status);
      if (response.status === 200) {
        return response.data;
      } else {
        throw response.status;}})
    .catch(function(error) {
      console.log("fail");
      console.log(error);
      throw error;
    });

export const deletePosition = ({ username, positionID, portfolioID, token }) =>
  axios
    .delete(
      ` http://127.0.0.1:9222/${username}/portfolio/${portfolioID}/position/${positionID}`,
      {
        headers: { Authorization: "Bearer " + token }
      }
    )
    .then(function(response) {
      console.log("success");
      console.log(response.status);
      if (response.status === 200) {
        return response.data;
      } else {
        throw response.status;
      }
    })
    .catch(function(error) {
      console.log("fail");
      console.log(error);
      throw error;
    });

    export const getMovementAndPricePrediction = symbol =>
    axios
      .post(`http://localhost:9999/Stock/${symbol}`)
      .then(function(response) {
        console.log("success");
        console.log(response.status);
        if (response.status === 200) {
          return response.data;
        } else {
          throw response.status;
        }
      })
      .catch(function(error) {
        console.log("fail");
        console.log(error);
        throw error;
      });

