import { createMuiTheme } from "@material-ui/core/styles";

export const geniusfinanceTheme = createMuiTheme({
  palette: {
    primary: { main: "#4fc1e9" },
    secondary: { main: "#feffe7" }
  }
});

export const geniusfinanceThemeString = {
  primary: "#4fc1e9",
  secondary: "#feffe7",
  tertiary: "#f1f1f1"
};
