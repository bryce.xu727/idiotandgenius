export const authenticationActions = {
  LOGIN_SUCCESS: ({ token, username }) => ({
    type: "LOGIN_SUCCESS",
    token: token,
    username: username
  }),
  LOGOUT: () => ({
    type: "LOGOUT"
  })
};

export const stockSymbolActions = {
  SET_SYMBOLS: symbols => ({
    type: "SET_SYMBOLS",
    symbols: symbols
  })
};
