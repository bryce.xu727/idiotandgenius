import { combineReducers } from "redux";

// https://redux.js.org/recipes/structuring-reducers/initializing-state
// https://www.npmjs.com/package/redux-persist

const initialAuthenticationState = { authentication: { token: undefined } };

function authenticationReducer(state = initialAuthenticationState, action) {
  const newState = { ...state };
  newState.authentication = { ...state.authentication };
  switch (action.type) {
    case "LOGIN_SUCCESS":
      newState["authentication"]["token"] = action.token; //TODO: obtain token from api call
      newState["authentication"]["username"] = action.username;
      console.log(newState);
      return newState;
    case "LOGIN_FAIL":
      delete newState.authentication.token;
      return newState;
    case "LOGOUT":
      delete newState.authentication.token;
      return newState;
    default:
      return state;
  }
}

const initialSymbolsState = { symbols: [] };
function symbolsReducer(state = initialSymbolsState, action) {
  const newState = { ...state };
  switch (action.type) {
    case "SET_SYMBOLS":
      newState["symbols"] = action.symbols; //TODO: obtain token from api call
      console.log(newState);
      return newState;
    default:
      return state;
  }
}

export const reducer = combineReducers({
  authenticationReducer,
  symbolsReducer
});
