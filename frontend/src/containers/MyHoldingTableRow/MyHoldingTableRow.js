import React from "react";
import { TableRow, TableCell } from "@material-ui/core";
import { KeyboardArrowDown, KeyboardArrowRight } from "@material-ui/icons";
import { NonStyledLink } from "../../components";
import { PositionTable } from "../../containers";
import styled from "styled-components";

const PositionContainer = styled.div`
  background-color: #f1f1f1;
  border-radius: 3px;
`;

const AngelContainer = styled.div`
  cursor: pointer;
  padding: auto;
`;

const ColoredTableCell = styled.div`
  color: ${props => {
    if (props.change === 0) {
      return "grey";
    } else if (props.change >= 0) {
      return "green";
    } else {
      return "red";
    }
  }};
`;

const columnsWithColor = [
  "Total gain",
  "Total gain%"
];

const columnsWithPercentage = ["Total gain%"];

export class MyHoldingTableRow extends React.Component {
  state = {
    expanded: false
  };
  render() {
    const {
      symbol,
      portfolioID,
      columns,
      detail,
      mapColumnsToDetail,
      updateTable,
      onLoading,
      onLoaded
    } = {
      ...this.props
    };
    return (
      <>
        {detail && detail["positions"].length > 0 ? (
          <>
            <TableRow>
              <TableCell align="left">
                <AngelContainer onClick={this.toggleExpanded}>
                  {this.state.expanded ? (
                    <KeyboardArrowDown />
                  ) : (
                    <KeyboardArrowRight />
                  )}
                </AngelContainer>
              </TableCell>
              {columns &&
                columns.map(column =>
                  column === "Symbol" ? (
                    <TableCell
                      key={`${symbol}_${column}`}
                      style={{ fontSize: "1.2em" }}
                      align="right"
                    >
                      <NonStyledLink to={`/quote/${symbol}`}>
                        {detail[mapColumnsToDetail[column]]}
                      </NonStyledLink>
                    </TableCell>
                  ) : (
                    <TableCell key={`${symbol}_${column}`} align="right">
                      {detail["positions"].length === 0 &&
                      detail[mapColumnsToDetail[column]] === 0
                        ? "-"
                        : columnsWithColor.includes(column)
                        ? this.generateColoredCell({
                            data: detail[mapColumnsToDetail[column]],
                            column
                          })
                        : detail[mapColumnsToDetail[column]]}
                    </TableCell>
                  )
                )}
            </TableRow>
            {this.state.expanded && (
              <TableRow>
                <TableCell colSpan={columns.length + 1}>
                  <PositionContainer>
                    <PositionTable
                      symbol={symbol}
                      portfolioID={portfolioID}
                      positions={detail["positions"]}
                      updateTable={updateTable}
                      onLoading={onLoading}
                      onLoaded={onLoaded}
                    />
                  </PositionContainer>
                </TableCell>
              </TableRow>
            )}
          </>
        ) : (
          <TableRow style={{ display: "none" }} />
        )}
      </>
    );
  }
  toggleExpanded = () => {
    this.setState({ expanded: !this.state.expanded });
  };
  generateColoredCell = ({ data, column }) => {
    let dataCleaned;
    if (
      typeof data === "string" &&
      data.length > 0 &&
      data[data.length - 1] === "%"
    ) {
      dataCleaned = data.slice(0, data.length - 1);
    } else {
      dataCleaned = data;
    }

    return (
      <ColoredTableCell change={dataCleaned}>
        {column === "Intraday gain"
          ? `${dataCleaned} %`
          : columnsWithPercentage.includes(column)
          ? `${(dataCleaned * 100).toFixed(2)} %`
          : `${dataCleaned}`}
      </ColoredTableCell>
    );
  };
}
