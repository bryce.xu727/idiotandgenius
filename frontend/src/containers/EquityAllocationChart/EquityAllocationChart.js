import React from "react";
import ReactApexChart from "react-apexcharts";
import { Paper } from "@material-ui/core";
import styled from "styled-components";

import { getPortfolioEquityAllocationUser } from "../../api/api";
import { CircularProgress } from "@material-ui/core";

const LoadingContainer = styled.div`
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Title = styled.div`
  color: #0e2f44;
  background-color: #f1f1f1;
  padding: 10px;
  margin: auto;
`;
const ChartContainer = styled.div`
  height: 380px;
  text-align: center;
`;
const EmptyWarningContainer = styled.div`
  color: #0e2f44;
  background-color: #f5f5f5;
  height: 380px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export class EquityAllocationChart extends React.Component {
  state = {
    options: {
      responsive: [
        {
          breakpoint: 0,
          options: {
            chart: {
              height: 300
            },
            legend: {
              position: "bottom"
            }
          }
        },
        {
          breakpoint: 600,
          options: {
            chart: {
              height: 300
            },
            legend: {
              position: "bottom"
            }
          }
        }
      ],
      labels: []
    },

    series: []
  };

  async componentDidMount() {
    const { credential, portfolioID } = { ...this.props };
    const { options } = { ...this.state };
    console.log(credential);
    this.setState({ isLoading: true });
    await getPortfolioEquityAllocationUser({
      portfolioID,
      username: credential.username,
      token: credential.token
    })
      .then(data => {
        console.log(data);
        options["labels"] = data.labels;
        this.setState({ series: data.series, options, isLoading: false });
      })
      .catch(() => {
        this.setState({ isLoading: false });
      });
  }
  render() {
    return (
      <Paper>
        <Title> Equity Allocation By Sector</Title>

        {this.state.isLoading ? (
          <EmptyWarningContainer>
            <LoadingContainer>
              <CircularProgress />
            </LoadingContainer>
          </EmptyWarningContainer>
        ) : this.state.series.length === 0 ? (
          <EmptyWarningContainer>
            <h3>Please Add Some Positions First</h3>
          </EmptyWarningContainer>
        ) : (
          <ChartContainer>
            <ReactApexChart
              options={this.state.options}
              series={this.state.series}
              labels={this.state.labels}
              type="donut"
              height="380"
            />
          </ChartContainer>
        )}
      </Paper>
    );
  }
}
