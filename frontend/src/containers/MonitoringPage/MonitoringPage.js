import React from "react";
import { geniusfinanceThemeString } from "../../theme/theme";
import styled from "styled-components";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import { connect } from "react-redux";
import { MonitoringTaskCard } from "../../components";
import { getAlarmTasksList, deleteAlarmTask } from "../../api/api";

const Container = styled.div`
  width: 95%;
  margin: 20px;
`;

const Title = styled.div`
  padding: 20px 0 15px 20px;
  background-color: ${geniusfinanceThemeString.primary}
  border-radius: 3px 3px 0 0;
  border-bottom: 11px solid ${geniusfinanceThemeString.tertiary};
`;

const TasksContainer = styled.div`
  min-height: 30vh;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const TicketContainer = styled.div`
  width: 95%;
  margin-bottom: 10px;
`;

const EmptyWarningContainer = styled.div`
  color: #0e2f44;
  background-color: #f5f5f5;
  height: 380px;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  border-radius: 3px;
`;
export class unconnectedMonitoringPage extends React.Component {
  state = { finished: [], unfinished: [] };
  componentDidMount() {
    const { username } = { ...this.props };
    this.getTasksList({ username });
  }
  render() {
    const { finished, unfinished } = { ...this.state };
    const { username } = { ...this.props };
    return (
      <Container>
        <Paper>
          <Title>
            <Typography variant="h3" component="h1" color="secondary">
              Active Alarms
            </Typography>
          </Title>

          <TasksContainer>
            {unfinished.length > 0 ? (
              unfinished.map(task => (
                <TicketContainer key={`container_${task.taskID}`}>
                  <MonitoringTaskCard
                    key={task.taskID}
                    symbol={task.symbol}
                    condition={task.condition}
                    target={task.target}
                    conditionType={task.conditionType}
                    email={task.email}
                    username={username}
                    onDelete={() => {
                      this.deleteTask({ username, taskID: task.taskID });
                    }}
                  />
                </TicketContainer>
              ))
            ) : (
              <EmptyWarningContainer>
                <h3> There is no active alarm</h3>
              </EmptyWarningContainer>
            )}
          </TasksContainer>
          <hr style={{ width: "98%" }} />
          <Title>
            <Typography variant="h3" component="h1" color="secondary">
              Completed Alarms
            </Typography>
          </Title>
          <TasksContainer>
            {finished.length > 0 ? (
              finished.map(task => (
                <TicketContainer key={`container_${task.taskID}`}>
                  <MonitoringTaskCard
                    key={task.taskID}
                    symbol={task.symbol}
                    condition={task.condition}
                    target={task.target}
                    conditionType={task.conditionType}
                    email={task.email}
                    username={username}
                    onDelete={() => {
                      this.deleteTask({ username, taskID: task.taskID });
                    }}
                  />
                </TicketContainer>
              ))
            ) : (
              <EmptyWarningContainer>
                <h3> There is no completed alarm</h3>
              </EmptyWarningContainer>
            )}
          </TasksContainer>
        </Paper>
      </Container>
    );
  }
  getTasksList = ({ username }) => {
    console.log("update");
    getAlarmTasksList({ username })
      .then(data => {
        this.setState({
          finished: [...data.finished],
          unfinished: [...data.unfinished]
        });
      })
      .catch();
  };
  deleteTask = ({ username, taskID }) => {
    deleteAlarmTask({ username, taskID })
      .then(() => {
        this.getTasksList({ username });
      })
      .catch();
  };
}

const mapStateToProps = state => ({
  token: state.authenticationReducer.authentication.token,
  username: state.authenticationReducer.authentication.username
});
const mapDispatchToProps = Dispatch => ({});

export const MonitoringPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(unconnectedMonitoringPage);
