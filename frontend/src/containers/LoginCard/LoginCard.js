import React from "react";
import "./LoginCard.css";
import { Login } from "../../api/api";
import { authenticationActions } from "../../reducers/actions";
import { connect } from "react-redux";
import jwtDecode from "jwt-decode";

class unconnectedLoginCard extends React.Component {
  state = { errorInfor: "" };
  userName = React.createRef();
  passWord = React.createRef();
  render() {
    const { errorInfor } = { ...this.state };
    return (
      <div className="LoginCard-card">
        <div className="LoginCard-title"> Login</div>
        {errorInfor && <div className="LoginCard-error">{errorInfor}</div>}
        <input
          className="LoginCard-input"
          placeholder=" username"
          ref={this.userName}
        />
        <input
          className="LoginCard-input"
          placeholder=" password"
          type="password"
          ref={this.passWord}
        />
        <button className="LoginCard-button" onClick={this.loginOnClickHandler}>
          Log in
        </button>
      </div>
    );
  }
  loginOnClickHandler = () => {
    // 1.check the input fromat
    // 2.try to login
    Login({
      username: this.userName.current.value,
      password: this.passWord.current.value
    })
      .then(result => {
        const username = JSON.parse(jwtDecode(result.token).identity).username;
        this.props.onLoginSuccess({ token: result.token, username });
      })
      .catch(error => {
        this.setState({ errorInfor: "Username or Password wrong" });
        throw error;
      });
  };
}

const mapStateToProps = state => ({});
const mapDispatchToPros = dispatch => ({
  onLoginSuccess: ({ token, username }) => {
    dispatch(authenticationActions.LOGIN_SUCCESS({ token, username }));
  }
});

export const LoginCard = connect(
  mapStateToProps,
  mapDispatchToPros
)(unconnectedLoginCard);
