import React from "react";
import "./PortfolioPage.css";
import { PortfolioTable, NewsSectionCard } from "../../containers";

import styled from "styled-components";

const PortfolioPageContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
`;
const ContentContainer = styled.div`
  width: 95%;
  margin-bottom: 20px;
`;

export class PortfolioPage extends React.Component {
  state = {
    symbols: []
  };
  render() {
    return (
      <>
        <PortfolioPageContainer>
          <ContentContainer>
            <PortfolioTable
              portfolioID={this.props.match.params.portfolioID}
              passSymbolToParent={this.addSymbols}
            />
          </ContentContainer>
          <ContentContainer>
            {this.state.symbols && this.state.symbols.length > 0 && (
              <NewsSectionCard
                symbols={this.state.symbols}
                emptyWarning="There is no related news"
              />
            )}
          </ContentContainer>
        </PortfolioPageContainer>
      </>
    );
  }

  addSymbols = symbols => {
    this.setState({ symbols: symbols || [] });
  };
}
