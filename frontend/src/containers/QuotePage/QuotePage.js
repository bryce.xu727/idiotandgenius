import React from "react";
import { geniusfinanceTheme } from "../../theme/theme";
import { SummaryCard } from "../../components";
import {
  CandleChartCard,
  NewsSectionCard,
  QuoteSummarySocketIO,
  BuyStockButton,
  SellStockButton,
  MonitorStockButton,
  PredictionCard,
} from "../../containers";
import { getStockInfor, getPredictedPriceAndMovement } from "../../api/api";
import styled from "styled-components";
import { ThemeProvider } from "@material-ui/styles";
import { withRouter } from "react-router-dom";
import AppBar from "@material-ui/core/AppBar";

const JustifyContent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
`;

const CandleChartCardContainer = styled.div`
  width: 95%;
  margin: 20px 0 10px 0;
`;

const NormalContainer = styled.div`
  width: 95%;
  margin: 10px auto;
`;

const StockSymbol = styled.h1`
  font-family: "Roboto", "Helvetica", "Arial", sans-serif;
  font-size: 1.4em;
  font-weight: bold;
  margin: 5px 0;
`;

const StockSubtitle = styled.h2`
  font-family: "Roboto", "Helvetica", "Arial", sans-serif;
  font-size: 0.9em;
  color: grey;
  margin: 5px 0;
`;
const CardHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-item: space-between;
`;
const CardHeaderText = styled.div`
  display: flex;
  flex-direction: column;
  min-width: max-content;
`;

const CardHeaderTools = styled.div`
  display: flex;
  flex-direction: row-reverse;
  align-items: center;
  width: 100%;
`;

const ButtonContainer = styled.div`
  margin-left: 20px;
  @media only screen and (max-width: 600px) {
    display: none;
  }
`;

const Panel = styled.div`
  display: ${props => (props.show ? "block" : "none")} !important;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
`;

class unroutedQuotePage extends React.Component {
  state = {
    summaryData: null,
    change: 0,
    changePercentage: "",
    currentPrice: "",
    tabNo: 0,
    Accuracy: 0,
    CurrentPrice: 0,
    predictData:null,
    Pattern: [],
    Pattern_name: "",
    Pattern_time: [],
    PeriodPrice:[],
    Period_time:[],
    PredictPrice: 267,
    Raise_Fall:"Fall",
    Symbol:"",
  };
  componentDidMount() {
    this.addSymbolToList(this.props.match.params.symbol);
    this.predict(this.props.match.params.symbol);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.location !== this.props.location) {
      console.log("[location]", this.props.location);
      console.log("symbol", this.props.match.params.symbol);
      this.predict(this.props.match.params.symbol);
      this.setState({
        summaryData: null,
        change: 0,
        changePercentage: "",
        currentPrice: "",
        tabNo: 0
      });
      this.addSymbolToList(this.props.match.params.symbol);
      
    }
  }
  render() {
    return (
      <ThemeProvider theme={geniusfinanceTheme}>
        <JustifyContent>
          <CandleChartCardContainer>
            <CandleChartCard symbol={this.props.match.params.symbol}>
              <CardHeader>
                <CardHeaderText>
                  <StockSymbol>
                    {this.state.summaryData && this.state.summaryData.Name} (
                    {this.props.match.params.symbol})
                  </StockSymbol>
                  <StockSubtitle>USD</StockSubtitle>
                </CardHeaderText>
                <CardHeaderTools>
                  <ButtonContainer>
                    <SellStockButton symbol={this.props.match.params.symbol} />
                  </ButtonContainer>
                  <ButtonContainer>
                    <BuyStockButton symbol={this.props.match.params.symbol} />
                  </ButtonContainer>
                  <ButtonContainer>
                    <MonitorStockButton
                      symbol={this.props.match.params.symbol}
                    />
                  </ButtonContainer>
                </CardHeaderTools>
              </CardHeader>
            </CandleChartCard>
          </CandleChartCardContainer>
          
          <AppBar position="static" color="tertiary" style={{ width: "95%" }}>
          </AppBar>
          <Panel show={this.state.tabNo === 0}>
            <NormalContainer>
              <SummaryCard data={this.state.summaryData} />
            </NormalContainer>
            <NormalContainer>
              <PredictionCard symbol={this.props.match.params.symbol} />
            </NormalContainer>
          </Panel>
          <NormalContainer>
            <NewsSectionCard
              symbols={[`${this.props.match.params.symbol}`]}
              emptyWarning="Currently there is no related news"
            />
          </NormalContainer>
        </JustifyContent>
        <QuoteSummarySocketIO
          urlPath="/quote_summary"
          symbol={this.props.match.params.symbol}
          onUpdate={this.updateQuoteSummary}
        />
      </ThemeProvider>
    );
  }
  addSymbolToList = symbol => {
    getStockInfor(symbol)
      .then(result => {
        console.log("success");
        this.setState({ summaryData: result });
      })
      .catch();
  };
  predict = symbol => {
    getPredictedPriceAndMovement(symbol)
      .then(result => {
        console.log("success");
        console.log(result.Raise_Fall);
        this.setState({ predictData: result.PredictPrice, movement: result.Raise_Fall, Accuracy: result.Accuracy });
      })
      .catch();
 };
  updateQuoteSummary = ({ currentPrice, change, changePercentage }) => {
    this.setState({ currentPrice, change, changePercentage });
  };

  handleTabChange = (e, v) => {
    this.setState({ tabNo: v });
  };
}

export const QuotePage = withRouter(unroutedQuotePage);
