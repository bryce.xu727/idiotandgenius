import React from "react";
import { connect } from "react-redux";
import Button from "@material-ui/core/Button";

import { amber } from "@material-ui/core/colors";
import { ThemeProvider } from "@material-ui/styles";
import { createMuiTheme } from "@material-ui/core/styles";
import Snackbar from "@material-ui/core/Snackbar";
import { SnackbarContentWrapper } from "../../components";
import { MonitorStockForm } from "../../containers";

const buttonTheme = createMuiTheme({
  palette: {
    primary: {
      main: amber[500]
    }
  }
});

class unconnectedMonitorStockButton extends React.Component {
  state = {
    showForm: false,
    portfolios: [],
    showSnackbar: false,
    showSnackbarInfor: undefined,
    showSnackbarType: undefined
  };
  render() {
    const { symbol } = { ...this.props };
    const { showSnackbar, showSnackbarType, showSnackbarInfor, showForm } = {
      ...this.state
    };
    return (
      <>
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left"
          }}
          open={showSnackbar}
          autoHideDuration={4000}
          onClose={this.closeSnackbar}
        >
          <SnackbarContentWrapper
            variant={showSnackbarType || "error"}
            message={showSnackbarInfor}
            onClose={this.closeSnackbar}
          />
        </Snackbar>
        <ThemeProvider theme={buttonTheme}>
          <Button variant="contained" onClick={this.showForm} color="primary">
            SET ALERTING
          </Button>
        </ThemeProvider>
        <MonitorStockForm
          open={showForm}
          handleClose={this.closeForm}
          openSnackbar={this.openSnackbar}
          symbol={symbol}
        />
      </>
    );
  }
  showForm = () => {
    this.setState({ showForm: true });
  };

  closeForm = () => {
    this.setState({ showForm: false });
  };
  closeSnackbar = () => {
    this.setState({ showSnackbar: false, showSnackbarInfor: undefined });
  };
  openSnackbar = ({ showSnackbarInfor, showSnackbarType }) => {
    this.setState({ showSnackbar: true, showSnackbarInfor, showSnackbarType });
  };
}

const mapStateToProps = state => ({
  token: state.authenticationReducer.authentication.token,
  username: state.authenticationReducer.authentication.username
});
const mapDispatchToProps = Dispatch => ({});

export const MonitorStockButton = connect(
  mapStateToProps,
  mapDispatchToProps
)(unconnectedMonitorStockButton);
