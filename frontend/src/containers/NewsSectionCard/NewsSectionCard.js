import React from "react";
import { withRouter } from "react-router-dom";

import { NewsItem } from "./../../components";

import Paper from "@material-ui/core/Paper";
import styled from "styled-components";

const Container = styled.div`
  display: flex;
  justify-content: space-around;
  margin-bottom: 20px;
`;
const PaperContainer = styled.div`
  width: 100%;
`;

const stockNewsApikey = "234vyljxxvpif6axowteliligqaagshheqp1chpv";
class unroutedNewsSectionCard extends React.Component {
  state = { news: [] };
  componentDidMount() {
    this.updateNews();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.location.pathname !== this.props.location.pathname) {
      this.setState({ news: [] });
      this.updateNews();
    }
  }

  render() {
    const { news } = { ...this.state };
    return (
      <>
        {news && news.length > 0 ? (
          <Container>
            <PaperContainer>
              <Paper>
                {news.map(({ title, img, description, url }) => (
                  <NewsItem
                    key={title}
                    title={title}
                    img={img}
                    description={description}
                    url={url}
                  />
                ))}
              </Paper>
            </PaperContainer>
          </Container>
        ) : (
          <></>
        )}
      </>
    );
  }
  updateNews = () => {const symbols = this.props.symbols;
    fetch(`https://stocknewsapi.com/api/v1?tickers=${symbols.join(",")}&items=50&token=${stockNewsApikey}`)
      .then(response => response.json())
      .then(result => {console.log(result);
        if (result.data.length > 0) {
          console.log("success");
          console.log(result.data.length > 5);
          const articles =
            result.data.length > 5 ? result.data.slice(0, 5) : result.data;
          console.log(articles);
          const { news } = { ...this.state };
          const newsEntries = articles.map(article => ({
            title: article.title,
            description: article.text,
            img: article.image_url,
            url: article.news_url
          }));
          const updatedNewsList = news.concat(newsEntries);
          console.log(updatedNewsList);
          this.setState({ news: updatedNewsList });}})
      .catch(error => {
        console.log(error);
      });
  };
}

export const NewsSectionCard = withRouter(unroutedNewsSectionCard);
