import React from "react";
import "./DropdownNavigationItem.css";
import { NavLink } from "react-router-dom";
import styled from "styled-components";
import { getPortfolioIdNamePair } from "../../api/api";
import { connect } from "react-redux";
import { CancelModal } from "../../containers";

const PositionedNavLink = styled.div`
  position: relative;
`;

const DropDown = styled.div`
  position: absolute;
  left: 0;
  top: 51px;
  box-shadow: 0 0 2px grey;
  width: fit-content;
  background: white;
  min-width: 100px;
  max-width: 30vw;
  width: max-content;
  border-radius: 3px;
  z-index: 1300;
  word-break: break-all;
`;

const DropDownItem = styled.div`
  font-family: "Roboto", "Helvetica", "Arial", sans-serif;
  font-weight: 300;
  font-size: 20px;
  color: #4fc1e9;
  padding: 10px 20px;

  &:hover {
    background-color: #f8f8f8;
  }
`;

class unconnectedDropdownNavigationItem extends React.Component {
  state = { showDropDown: false, portfolios: [] };
  componentDidMount() {
    this.updateDropdownContent();
  }
  render() {
    const { showDropDown, portfolios } = { ...this.state };
    return (
      <>
        <PositionedNavLink
          style={{ textDecoration: "none", color: "inherit" }}
          onClick={() => {
            this.toggleDropdown();
            if (!this.state.showDropDown) {
              this.updateDropdownContent();
            }
          }}
        >
          <div className="DropdownNavigationItem"> {this.props.children}</div>
          {showDropDown && (
            <DropDown>
              {portfolios.map(p => (
                <NavLink
                  to={`/portfolios/${p.portfolioId}`}
                  key={`link_${p.portfolioId}`}
                  style={{ textDecoration: "none", color: "inherit" }}
                >
                  <DropDownItem key={p.portfolioId}>
                    {p.portfolioName}
                  </DropDownItem>
                </NavLink>
              ))}
            </DropDown>
          )}
        </PositionedNavLink>
        {this.state.showDropDown && (
          <CancelModal handleClick={this.toggleDropdown} />
        )}
      </>
    );
  }
  toggleDropdown = () => {
    this.setState({ showDropDown: !this.state.showDropDown });
  };

  updateDropdownContent = () => {
    const { token, username } = { ...this.props };
    return getPortfolioIdNamePair({ token, username })
      .then(data => {
        this.setState({ portfolios: data });
      })
      .catch();
  };
}

const mapStateToProps = state => ({
  token: state.authenticationReducer.authentication.token,
  username: state.authenticationReducer.authentication.username
});
const mapDispatchToProps = Dispatch => ({});

export const DropdownNavigationItem = connect(
  mapStateToProps,
  mapDispatchToProps
)(unconnectedDropdownNavigationItem);
