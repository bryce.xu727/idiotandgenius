import React from "react";
import { connect } from "react-redux";
import { CircularProgress } from "@material-ui/core";
import {
  getPortfolioList,
  createPortfoliosForUser,
  deletePortfolioByUser
} from "../../api/api";
import { NonStyledLink, EditableButton } from "../../components";
import { AddPortfolioButton } from "../../containers";
import {
  Paper,
  Typography,
  Table,
  TableBody,
  TableHead,
  TableRow,
  TableCell
} from "@material-ui/core";
import { Delete } from "@material-ui/icons";
import styled from "styled-components";
import { geniusfinanceThemeString } from "../../theme/theme";

const Title = styled.div`
  color: ${geniusfinanceThemeString.secondary};
  background-color: ${geniusfinanceThemeString.primary};
  padding: 10px;
  margin: auto;
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-radius: 3px 3px 0 0;
  border-bottom: 11px solid #f1f1f1;
`;

const TableSheet = styled(Paper)``;

const TableContent = styled.div`
  overflow-x: scroll;
  min-height: 55vh;
`;

const ToolSection = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
  width: fit-content;
`;

const ToolContainer = styled.div`
  margin: 0px 5px;
`;

const LoadingContainer = styled.div`
  position: absolute;
  width: 100%;
  hight: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const DeleteButton = styled(Delete)`
  cursor: pointer;
  &:hover {
    color: #ff5546;
  }
`;

const columns = [
  "Symbols",
  "Market value",
  "Day chg.",
  "Day chg. %",
  "Total chg.",
  "Total chg. %"
];

const columnNeedsColor = [
  "Day chg.",
  "Day chg. %",
  "Total chg.",
  "Total chg. %"
];

const columnNeedsPercentage = ["Day chg. %", "Total chg. %"];

const headerToValueMap = {
  Symbols: "numberofStocks",
  "Market value": "marketValue",
  "Day chg.": "dailyGain",
  "Day chg. %": "dailyGainPercentage",
  "Total chg.": "totalGain",
  "Total chg. %": "totalGainPercentage"
};

const ColoredTableCell = styled.div`
  color: ${props => {
    if (props.change === 0) {
      return "grey";
    } else if (props.change >= 0) {
      return "green";
    } else {
      return "red";
    }
  }};
`;

class unconnectedPortfoliosTable extends React.Component {
  state = {
    rows: [],
    showAction: false,
    isLoading: false
  };

  componentDidMount() {
    this.getPortfolios();
  }
  render() {
    const { rows, showAction, isLoading } = { ...this.state };
    return (
      <TableSheet>
        <Title>
          <Typography variant="h3" component="h1">
            {this.props.username}'s Portfolio
          </Typography>
          <ToolSection>
            <ToolContainer>
              <EditableButton onClick={this.toggleShowAction} />
            </ToolContainer>
            <ToolContainer>
              <AddPortfolioButton onCreate={this.createPortfolio} />
            </ToolContainer>
          </ToolSection>
        </Title>
        <TableContent>
          <Table style={{ tableLayout: "auto", minWidth: "max-content" }}>
            <TableHead>
              <TableRow>
                {showAction && <TableCell>Action</TableCell>}

                <TableCell>Portfolio Name</TableCell>
                {columns.map(column => (
                  <TableCell align="right" key={column}>
                    {column}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            {isLoading && (
              <LoadingContainer>
                <CircularProgress />
              </LoadingContainer>
            )}
            <TableBody>
              {rows.map(row => (
                <TableRow key={row.portfolioId}>
                  {showAction && (
                    <TableCell component="td">
                      {/* row.name*/}
                      <DeleteButton
                        onClick={() => {
                          this.deletePortfolio({
                            portfolioID: row.portfolioId
                          });
                        }}
                      />
                    </TableCell>
                  )}

                  <TableCell component="th" scope="row">
                    <NonStyledLink to={`/portfolios/${row.portfolioId}`}>
                      <div style={{ fontSize: "1.2em" }}>
                        {row.portfolioName.length > 23
                          ? `${row.portfolioName.slice(0, 25)}..`
                          : row.portfolioName}
                      </div>
                    </NonStyledLink>
                  </TableCell>

                  {columns.map(column => (
                    <TableCell align="right" key={`${row.id}_${column}`}>
                      {columnNeedsColor.includes(column) ? (
                        <ColoredTableCell
                          change={row[headerToValueMap[column]]}
                        >
                          {columnNeedsPercentage.includes(column)
                            ? `${row[headerToValueMap[column]].toFixed(2)}%`
                            : `$${row[headerToValueMap[column]].toFixed(2)}`}
                        </ColoredTableCell>
                      ) : (
                        row[headerToValueMap[column]]
                      )}
                    </TableCell>
                  ))}
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContent>
      </TableSheet>
    );
  }

  getPortfolios = () => {
    this.setState({ isLoading: true });
    getPortfolioList({
      username: this.props.username,
      token: this.props.token
    }) //todo: get username
      .then(result => {
        this.setState({ isLoading: false });
        console.log(result);
        if (result.portfolioList === null) {
          this.setState({ rows: [] });
        } else {
          this.setState({ rows: result });
        }
      })
      .catch(error => {
        this.setState({ isLoading: false });
        throw error;
      });
  };

  createPortfolio = ({ portfolioName }) => {
    createPortfoliosForUser({
      token: this.props.token,
      portfolioName,
      userID: this.props.username
    })
      .then(() => {
        this.getPortfolios();
      })
      .catch();
  };

  deletePortfolio = ({ portfolioID }) => {
    deletePortfolioByUser({
      token: this.props.token,
      portfolioID,
      userID: this.props.username
    })
      .then(() => {
        this.getPortfolios();
      })
      .catch();
  };

  toggleShowAction = () => {
    this.setState({ showAction: !this.state.showAction });
  };
}

const mapStateToProps = state => ({
  token: state.authenticationReducer.authentication.token,
  username: state.authenticationReducer.authentication.username
});
const mapDispatchToProps = Dispatch => ({});

export const PortfoliosTable = connect(
  mapStateToProps,
  mapDispatchToProps
)(unconnectedPortfoliosTable);
