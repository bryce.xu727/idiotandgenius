import React from "react";
import Card from "@material-ui/core/Card";
import styled from "styled-components";
import FetchStockData from '../../components/StockChart/FetchStockData';
import StockList from '../../components/StockChart/StocksData'; //stockList


const Container = styled.div`
  width: 100%;
`;

const ChildrenContainer = styled.div`
  margin-bottom: 15px;
`;

export class CandleChartCard extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      type: 'default',
      stock: 'MSFT',
      companyName: 'Microsoft',
      stockList: StockList
    };
  }
  
  changeType(e){
    this.setState({
      type: e.target.value || 'default'
    });
  }

  changeStock(e){
    let value = e.target.textContent.split(' ')[0];
    let b = this.state.stockList.filter(i => i[0] == value);

    if(!b.length){
      return;
    }

    this.setState({
      stock: value,
      companyName: b[0][1]
    });    
  }

  render(){
      return (
      <>
        {this.props.children && (
          <ChildrenContainer>{this.props.children}</ChildrenContainer>
        )}
        <Container>
          <Card>
          <div>
          <FetchStockData type={this.state.type} stock={this.props.symbol} companyName={this.state.companyName} />
          <div className="row">
            <div className="col-md-6 col-lg-6 col-sm-12 col-xs-12">
              <b>Select Chart Type  </b>
          <select name="type" onChange={e => this.changeType(e)}>
            <option value="default">CandleChart</option>
            <option value="line">Line</option>
            <option value="column">Column</option>
            <option value="scatter">Scatter</option>
          </select>
            </div>
          </div>
        </div>
          </Card>
        </Container>
      </>
    );

  }
}

