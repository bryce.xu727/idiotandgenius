import React from "react";
import Table from "@material-ui/core/Table";
import { TableBody, TableHead, TableRow, TableCell } from "@material-ui/core";
import { connect } from "react-redux";

import { PositionTableRow } from "../../containers";
import { addPositionToPortfolio } from "../../api/api";

//the first empty element create space for the toggle button

class unconnectedPositionTable extends React.Component {
  state = {
    showEditableRow: false,
    positions: this.props.positions || []
  };

  tableColumns = [
    "Trade date",
    "Size",
    "Cost base",
    "Total gain",
    "Total gain%"
  ];
  render() {
    const { token, username, portfolioID, updateTable, onLoading, onLoaded } = {
      ...this.props
    };
    const { positions } = { ...this.state };
    return (
      <Table>
        <TableHead>
          <TableRow>
            {this.tableColumns.map(column => (
              <TableCell key={`title_${column}`}>{column}</TableCell>
            ))}
            <TableCell />
            {/**used for delete */}
          </TableRow>
        </TableHead>
        <TableBody>
          {positions &&
            positions.map((position, index) => (
              <PositionTableRow
                key={position["position id"]}
                position={position}
                columns={this.tableColumns}
                updateTable={() => {
                  updateTable({ token, username, portfolioID });
                }}
                deletePosition={() => {
                  onLoading();
                  this.deletePosition(index);
                  onLoaded();
                }}
                portfolioID={portfolioID}
              />
            ))}

          {/* {this.state.showEditableRow && (
            <PositionEditableRow
              onClose={this.hideEditableRow}
              onSubmit={this.addPosition}
            />
          )} */}
        </TableBody>

        {/* <AddContainer onClick={this.showEditableRow}>
          <Add /> <Label>Add lot</Label>
        </AddContainer> */}
      </Table>
    );
  }

  showEditableRow = () => {
    this.setState({ showEditableRow: true });
  };
  hideEditableRow = () => {
    this.setState({ showEditableRow: false });
  };

  addPosition = ({ positionSize, purchasePrice }) => {
    const { token, username, portfolioID, symbol, updateTable } = {
      ...this.props
    };
    addPositionToPortfolio({
      token,
      username,
      portfolioID,
      symbol,
      positionSize,
      purchasePrice
    }).then(() => {
      updateTable({ token, username, portfolioID }).then(() => {
        this.hideEditableRow();
      });
    });
  };

  deletePosition = index => {
    const { positions: newPositions } = { ...this.state };
    newPositions.splice(index, 1);
    this.setState(newPositions);
  };
}

const mapStateToProps = state => ({
  token: state.authenticationReducer.authentication.token,
  username: state.authenticationReducer.authentication.username
});
const mapDispatchToProps = dispatch => ({});

export const PositionTable = connect(
  mapStateToProps,
  mapDispatchToProps
)(unconnectedPositionTable);
