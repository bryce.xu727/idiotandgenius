import React from "react";
import io from "socket.io-client";

export class QuoteSummarySocketIO extends React.Component {
  componentDidMount() {
    this.initSocket();
  }
  componentDidUpdate(prevProps) {
    if (prevProps.symbol !== this.props.symbol) {
      this.closeSocket();
      this.initSocket();
    }
  }
  componentWillUnmount() {
    this.closeSocket();
  }
  initSocket = () => {
    this.socket = io("http://127.0.0.1:9333" + this.props.urlPath);
    this.socket.on("update", this.update);
    this.socket.emit("connection", this.props.symbol);
    this.socket.on("reconnect", this.reconnection);
    this.socket.on("reconnect_attempt", this.reconnection_attempt);
  };
  closeSocket = () => {
    this.socket.off("update", this.update);
    this.socket.off("reconnect", this.reconnection);
    this.socket.off("reconnect_attempt", this.reconnection_attempt);
    this.socket.close();
  };
  update = data => {
    console.log(data);
    const { now, change, changePercentage } = { ...data };
    this.props.onUpdate({
      currentPrice: now,
      change: parseFloat(change),
      changePercentage
    });
  };

  reconnection = () => {
    this.socket.emit("connection", this.props.symbol);
  };

  reconnection_attempt = attemptNumber => {
    console.log(attemptNumber);
    if (attemptNumber > 10) {
      this.socket.disconnect();
    }
  };
  render() {
    return <></>;
  }
}

export class QuoteSocketIO extends React.Component {
  componentDidMount() {
    this.initSocket();
  }
  componentDidUpdate(prevProps) {
    if (prevProps.symbol !== this.props.symbol) {
      this.closeSocket();
      this.initSocket();
    }
  }
  componentWillUnmount() {
    this.closeSocket();
  }
  render() {
    return <></>;
  }

  initSocket = () => {
    this.socket = io("http://127.0.0.1:9333" + this.props.urlPath);
    this.socket.on("update", this.update);
    this.socket.emit("connection", this.props.symbol);
    this.socket.on("reconnect", this.reconnection);
    this.socket.on("reconnect_attempt", this.reconnection_attempt);
  };

  update = data => {
    console.log("quote", data);
    const { Meta, Series } = { ...data };
    this.props.onUpdate({
      Meta,
      Series
    });
  };
  reconnection = () => {
    this.socket.emit("connection", this.props.symbol);
  };

  reconnection_attempt = attemptNumber => {
    console.log(attemptNumber);
    if (attemptNumber > 10) {
      this.socket.disconnect();
    }
  };

  closeSocket = () => {
    this.socket.off("update", this.update);
    this.socket.off("reconnect", this.reconnectio);
    this.socket.off("reconnect_attempt", this.reconnection_attempt);
    this.socket.close();
  };
}
