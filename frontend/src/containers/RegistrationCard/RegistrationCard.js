import React, { Component } from "react";
import "../LoginCard/LoginCard.css";
import { Registraion } from "../../api/api";
import "./Registration.css";

class RegistrationCard extends Component {
  constructor() {
    super();
    this.state = {
      user: {
        username: undefined,
        password: undefined,
        confirmPassword: undefined
      },
      errorInfor: undefined,
      successInfor: undefined
    };

    this.handleChangeFor = this.handleChangeFor.bind(this);
    this.formSubmition = this.formSubmition.bind(this);
  }

  formSubmition(event) {
    event.preventDefault();
    this.setState({
      errorInfor: undefined,
      successInfor: undefined
    })
    Registraion(this.state.user)
      .then(() => {
        this.setState({ successInfor: "Registration success!"})
      })
      .catch(error => {
        this.setState({ errorInfor: "Username already exist or server error" });
      });
  }

  handleChangeFor = propertyName => event => {
    const { user } = this.state;
    const newUser = {
      ...user,
      [propertyName]: event.target.value
    };
    this.setState({ user: newUser });
  };

  render() {
    const { errorInfor } = { ...this.state };
    const { successInfor } = { ...this.state };
    const inputCheck =
      !this.state.user.username ||
      !this.state.user.password ||
      !this.state.user.confirmPassword ||
      !(this.state.user.password === this.state.user.confirmPassword);
    return (
      <div className="LoginCard-card">
        <div className="LoginCard-title"> Registration </div>
        {errorInfor && <div className="LoginCard-error">{errorInfor}</div>}
        {successInfor && <div className="LoginCard-success">{successInfor}</div>}
        <input
          className="LoginCard-input"
          placeholder=" Username"
          name="username"
          onChange={this.handleChangeFor("username")}
        />
        <input
          className="LoginCard-input"
          placeholder=" Password"
          name="password"
          type="password"
          onChange={this.handleChangeFor("password")}
        />
        <input
          className="LoginCard-input"
          placeholder=" Confirm Password"
          name="confirmPassword"
          type="password"
          onChange={this.handleChangeFor("confirmPassword")}
        />
        <button
          className="LoginCard-button"
          type="submit"
          disabled={inputCheck}
          onClick={this.formSubmition}
        >
          Sign up
        </button>
      </div>
    );
  }
}

export default RegistrationCard;
