import React from "react";
import styled from "styled-components";
import {
  EquityAllocationChart,
  //CurveDataChart
} from "../../containers";

const PortfolioPageContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  margin-top: 20px;
`;
const SplitContentContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 95%;
  margin-bottom: 20px;

  @media only screen and (max-width: 1100px) {
    flex-direction: column;
    margin-bottom: 0px;
  }
`;

const NormalContentContainer = styled.div`
  width: 95%;
`;
const SubContentContainer = styled.div`
  width: 100%;
  @media only screen and (max-width: 1100px) {
    width: 100%;
    margin-bottom: 20px;
  }
`;
export class AnalysisPage extends React.Component {
  render() {
    const { portfolioID, credential } = { ...this.props };
    return (
      <PortfolioPageContainer>
        <SplitContentContainer>
          <SubContentContainer>
            <EquityAllocationChart
              portfolioID={portfolioID}
              credential={credential}
            />
          </SubContentContainer>
        </SplitContentContainer>
        <NormalContentContainer>
        </NormalContentContainer>
      </PortfolioPageContainer>
    );
  }
}
