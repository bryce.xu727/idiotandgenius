import React from "react";
import { connect } from "react-redux";
import Button from "@material-ui/core/Button";
import { OrderStockForm } from "../../containers";
import { getPortfolioIdNamePair } from "../../api/api";
import { red } from "@material-ui/core/colors";
import { ThemeProvider } from "@material-ui/styles";
import { createMuiTheme } from "@material-ui/core/styles";
import Snackbar from "@material-ui/core/Snackbar";
import { SnackbarContentWrapper } from "../../components";

const buttonTheme = createMuiTheme({
  palette: {
    primary: {
      main: red[500]
    }
  }
});

class unconnectedSellStockButton extends React.Component {
  state = {
    showForm: false,
    portfolios: [],
    showSnackbar: false,
    showSnackbarInfor: undefined,
    showSnackbarType: undefined
  };
  componentDidMount() {
    this.getPortfolios();
  }
  render() {
    const { symbol } = { ...this.props };
    const { showSnackbar, showSnackbarType, showSnackbarInfor } = {
      ...this.state
    };
    return (
      <>
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left"
          }}
          open={showSnackbar}
          autoHideDuration={4000}
          onClose={this.closeSnackbar}
        >
          <SnackbarContentWrapper
            variant={showSnackbarType || "error"}
            message={showSnackbarInfor}
            onClose={this.closeSnackbar}
          />
        </Snackbar>
        <ThemeProvider theme={buttonTheme}>
          <Button variant="contained" onClick={this.showForm} color="primary">
            Sell
          </Button>
        </ThemeProvider>
        <OrderStockForm
          open={this.state.showForm}
          handleClose={this.closeForm}
          portfolios={this.state.portfolios}
          symbol={symbol}
          sellForm
          openSnackbar={this.openSnackbar}
        />
      </>
    );
  }

  showForm = () => {
    this.setState({ showForm: true });
  };

  closeForm = () => {
    this.setState({ showForm: false });
  };

  getPortfolios = () => {
    getPortfolioIdNamePair({
      username: this.props.username,
      token: this.props.token
    }) 
      .then(result => {
        console.log(result);
        if (result.portfolioList === null) {
          this.setState({ portfolios: [] });
        } else {
          this.setState({ portfolios: result });
        }
      })
      .catch(error => {
        throw error;
      });
  };

  closeSnackbar = () => {
    this.setState({ showSnackbar: false, showSnackbarInfor: undefined });
  };
  openSnackbar = ({ showSnackbarInfor, showSnackbarType }) => {
    this.setState({ showSnackbar: true, showSnackbarInfor, showSnackbarType });
  };
}

const mapStateToProps = state => ({
  token: state.authenticationReducer.authentication.token,
  username: state.authenticationReducer.authentication.username
});
const mapDispatchToProps = Dispatch => ({});

export const SellStockButton = connect(
  mapStateToProps,
  mapDispatchToProps
)(unconnectedSellStockButton);
