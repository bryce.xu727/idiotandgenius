import React from "react";
import io from "socket.io-client";

export class SocketIoTester extends React.Component {
  componentDidMount() {
    const socket = io("http://127.0.0.1:9333/quote");
    socket.on("update", data => {
      console.log(data);
    });
    socket.emit("connection", this.props.symbol);
    socket.on("reconnect", () => {
      socket.emit("connection", this.props.symbol);
    });
    socket.on("reconnect_attempt", attemptNumber => {
      console.log(attemptNumber);
      if (attemptNumber > 10) {
        socket.disconnect();
      }
    });
  }
  render() {
    return <></>;
  }
}
