import React from "react";
import { TableRow, TableCell, TextField } from "@material-ui/core";

export class PositionEditableRow extends React.Component {
  state = {
    purchasePrice: undefined,
    positionSize: undefined
  };
  render() {
    return (
      <TableRow>
        <TableCell>New lot</TableCell>
        <TableCell>
          <TextField
            type="number"
            placeholder="size"
            onChange={this.handleSizeChange}
          />
        </TableCell>
        <TableCell>
          <TextField
            type="number"
            placeholder="price"
            onChange={this.handlePriceChange}
            style={{ borderColor: "red" }}
          />
        </TableCell>
        <TableCell>
          <button onClick={this.props.onClose}>cancel</button>
          <button onClick={this.handleSubmit}>confirm</button>
        </TableCell>
      </TableRow>
    );
  }

  handlePriceChange = e => {
    this.setState({ purchasePrice: e.target.value });
  };
  handleSizeChange = e => {
    this.setState({ positionSize: e.target.value });
  };

  handleSubmit = () => {
    const { onSubmit } = { ...this.props };
    const { purchasePrice, positionSize } = { ...this.state };
    onSubmit({ purchasePrice, positionSize });
  };
}
