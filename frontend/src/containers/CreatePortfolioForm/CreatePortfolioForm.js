import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

export class CreatePortfolioForm extends React.Component {
  state = { portfolioName: "" };
  render() {
    const { handleClose, handleSubmit, open } = { ...this.props };
    return (
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Create New Portfolio</DialogTitle>
        <DialogContent>
          <DialogContentText>
            To create a new portfolio, please enter a name for that portfolio.
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Portfolio Name"
            type="text"
            fullWidth
            onChange={this.handlePortfolioNameChange}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button
            onClick={() => {
              console.log("creating.." + this.state.portfolioName);
              handleSubmit({ portfolioName: this.state.portfolioName });
              handleClose();
            }}
            color="primary"
          >
            Create
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
  handlePortfolioNameChange = e => {
    this.setState({ portfolioName: e.target.value });
  };
}
