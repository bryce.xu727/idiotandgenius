import React from "react";
import ReactApexChart from "react-apexcharts";
import Paper from "@material-ui/core/Paper";
import styled from "styled-components";
import { connect } from "react-redux";
import { getPortfolioCurveData } from "../../api/api";
import update from "immutability-helper";
import { CircularProgress } from "@material-ui/core";

const ChartContainer = styled.div`
  margin-bottom: 20px;
`;
const LoadingContainer = styled.div`
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Title = styled.div`
  color: #0e2f44;
  background-color: #f1f1f1;
  padding: 10px;
  margin: auto;
`;

const EmptyWarningContainer = styled.div`
  color: #0e2f44;
  background-color: #f5f5f5;
  height: 350px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

class unconnectedCurveDataChart extends React.Component {
  state = {
    isLoading: false,
    isEmpty: true,
    marketNequity: {
      options: {
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: "smooth"
        },

        xaxis: {
          type: "string",
          categories: ["2019-11-23", "2019-11-24", "2019-11-25"]
        }
      },
      series: [
        {
          name: "Market value",
          data: [11260.22, 11260.22, 11260.22]
        },
        {
          name: "Equity",
          data: [11002, 11260.22, 11002]
        }
      ]
    },
    gainCurve: {
      options: {
        chart: {
          zoom: {
            enabled: false
          }
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: "straight"
        },
        grid: {
          row: {
            colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
            opacity: 0.5
          }
        },
        xaxis: {
          categories: []
        },
        yaxis: [
          {
            axisTicks: {
              show: true
            },
            axisBorder: {
              show: true,
              color: "#008FFB"
            },
            labels: {
              style: {
                color: "#008FFB"
              }
            },
            title: {
              text: "Gain (in USD)",
              style: {
                color: "#008FFB"
              }
            },
            tooltip: {
              enabled: true
            }
          },

          {
            seriesName: "Yield",
            opposite: true,
            axisTicks: {
              show: true
            },
            axisBorder: {
              show: true,
              color: "#00E396"
            },
            labels: {
              style: {
                color: "#00E396"
              }
            },
            title: {
              text: "Yield (%)",
              style: {
                color: "#00E396"
              }
            }
          }
        ]
      },
      series: []
    }
  };

  componentDidMount() {
    this.initChart();
  }
  render() {
    return (
      <>
        <ChartContainer>
          <Paper>
            <Title> Market Value and Equity</Title>

            {this.state.isLoading ? (
              <EmptyWarningContainer>
                <LoadingContainer>
                  <CircularProgress />
                </LoadingContainer>
              </EmptyWarningContainer>
            ) : this.state.isEmpty ? (
              <EmptyWarningContainer>
                <h3>Please Add Some Positions First</h3>
              </EmptyWarningContainer>
            ) : (
              <ReactApexChart
                options={this.state.marketNequity.options}
                series={this.state.marketNequity.series}
                type="area"
                height="350"
              />
            )}
          </Paper>
        </ChartContainer>
        <ChartContainer>
          <Paper>
            <Title> Gain and Yield</Title>

            {this.state.isLoading ? (
              <EmptyWarningContainer>
                <LoadingContainer>
                  <CircularProgress />
                </LoadingContainer>
              </EmptyWarningContainer>
            ) : this.state.isEmpty ? (
              <EmptyWarningContainer>
                <h3>Please Add Some Positions First</h3>
              </EmptyWarningContainer>
            ) : (
              <ReactApexChart
                options={this.state.gainCurve.options}
                series={this.state.gainCurve.series}
                type="line"
                height="350"
              />
            )}
          </Paper>
        </ChartContainer>
      </>
    );
  }

  initChart = () => {
    const { credential, portfolioID } = { ...this.props };
    this.setState({ isLoading: true });
    getPortfolioCurveData({
      username: credential.username,
      token: credential.token,
      portfolioID
    })
      .then(result => {
        if (result) {
          const date = result.map(r => r["Date"]);
          const equity = result.map(r => r["Equity"].toFixed(2));
          const marketV = result.map(r => r["MarketValue"].toFixed(2));
          const gain = result.map(r => r["Gain"].toFixed(2));
          const yieldP = result.map(r => (r["Yield"] * 100).toFixed(2));
          if (date.length === 1) {
            // duplicate the data to make it looks like a line
            date.push(date[0]);
            equity.push(equity[0]);
            marketV.push(marketV[0]);
            gain.push(gain[0]);
            yieldP.push(yieldP[0]);
          } else if (date.length <= 0) {
            this.setState({ isLoading: false, isEmpty: true });
            return;
          }
          const newMarketNequity = update(this.state.marketNequity, {
            options: options =>
              update(options, {
                xaxis: xaxis =>
                  update(xaxis, {
                    $set: { type: "string", categories: date }
                  })
              }),

            series: {
              $set: [
                { name: "Market value", data: marketV },
                { name: "Equity", data: equity }
              ]
            }
          });

          const newGainCurve = update(this.state.gainCurve, {
            options: options =>
              update(options, {
                xaxis: xaxis =>
                  update(xaxis, {
                    $set: { type: "string", categories: date }
                  })
              }),
            series: {
              $set: [
                { name: "Gain", type: "line", data: gain },
                { name: "Yield", type: "line", data: yieldP }
              ]
            }
          });
          console.log("[state]", this.state);
          console.log({
            marketNequity: newMarketNequity,
            gainNYeild: newGainCurve
          });
          this.setState({
            marketNequity: newMarketNequity,
            gainCurve: newGainCurve,
            isEmpty: false
          });
          this.setState({ isLoading: false });
        }
      })
      .catch(() => {
        this.setState({ isLoading: false });
      });
  };
}
const mapStateToProps = state => ({
  token: state.authenticationReducer.authentication.token,
  username: state.authenticationReducer.authentication.username
});
const mapDispatchToProps = Dispatch => ({});

export const CurveDataChart = connect(
  mapStateToProps,
  mapDispatchToProps
)(unconnectedCurveDataChart);
