import React from "react";
import styled from "styled-components";
import AddBox from "@material-ui/icons/AddBox";
import { CreatePortfolioForm } from "../../containers";

const AddButton = styled.div`
  cursor: pointer;
  display: flex;
  justify-item: space-between;
  align-items: center;
`;
const AddLabel = styled.div`
  cursor: pointer;
  @media only screen and (max-width: 700px) {
    width: 50%;
    display: none;
  }
`;
export class AddPortfolioButton extends React.Component {
  state = { showModal: false };
  render() {
    const { showModal } = { ...this.state };
    const { onCreate } = { ...this.props };
    return (
      <>
        <AddButton onClick={this.openModal}>
          <AddBox />
          <AddLabel style={{ width: "max-content" }}> Add portfolio</AddLabel>
        </AddButton>
        <CreatePortfolioForm
          open={showModal}
          handleClose={this.closeModal}
          handleSubmit={onCreate}
        />
      </>
    );
  }
  openModal = () => {
    this.setState({ showModal: true });
  };
  closeModal = () => {
    this.setState({ showModal: false });
  };
}
