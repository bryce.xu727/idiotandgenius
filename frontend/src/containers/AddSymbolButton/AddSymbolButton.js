import React from "react";
import "./AddSymbolButton.css";
import Add from "@material-ui/icons/Add";

import { SearchInput, SymbolResultOption } from "../../components/";
import { CancelModal } from "../../containers/";

import { connect } from "react-redux";

class unconnectedAddSymbolButton extends React.Component {
  state = {
    isActivated: false,
    searchString: ""
  };
  render() {
    const { isActivated, searchString } = { ...this.state };
    return (
      <>
        <div className="addSymbolButton">
          <div className="addSymbolInput_button" onClick={this.activate}>
            <Add />
            <label className="addSymbolInput_button_label"> Add Symbol</label>
          </div>
          {isActivated && (
            <div className="addSymbolInput_container">
              <div className="addSymbolInput_input_container">
                <div className="addSymbolInput_pointer_shadow" />
                <div className="addSymbolInput_pointer_color" />
                <SearchInput
                  placeholder="Symbol..."
                  handleSearchInputChange={this.handleSearchInputChange}
                />
                <div className="addSymbolInput_result_container">
                  {searchString &&
                    this.findSymbols().map(result => (
                      <SymbolResultOption
                        className="addSymbolInput_result"
                        symbol={result.Symbol}
                        industry={result.Sector}
                        onClick={symbol => {
                          this.props.onAdd(symbol);
                          this.deactivate();
                        }}
                      />
                    ))}
                </div>
              </div>
            </div>
          )}
        </div>
        {isActivated && <CancelModal handleClick={this.deactivate} />}
      </>
    );
  }
  deactivate = () => {
    this.setState({ isActivated: false, searchString: "" });
  };
  activate = () => {
    this.setState({ isActivated: true });
  };

  findSymbols = () => {
    if (!Array.isArray(this.props.symbols)) {
      return [];
    }
    return this.props.symbols
      .filter(symbol =>
        symbol.Symbol.toLowerCase().includes(
          this.state.searchString.toLowerCase()
        )
      )
      .slice(0, 7);
  };
  handleSearchInputChange = e => {
    console.log(e.target.value);
    this.setState({ searchString: e.target.value });
  };
}

const mapStateToProps = state => ({
  symbols: state.symbolsReducer.symbols || []
});
const mapDispatchToProps = Dispatch => ({});

export const AddSymbolButton = connect(
  mapStateToProps,
  mapDispatchToProps
)(unconnectedAddSymbolButton);
