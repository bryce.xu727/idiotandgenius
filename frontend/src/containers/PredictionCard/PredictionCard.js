import React, { Component } from "react";
import styled from "styled-components";
import { Paper, Typography, Grid, Card, CardContent } from "@material-ui/core";
import { getMovementAndPricePrediction } from "../../api/api";
import ReactApexChart from "react-apexcharts";
import { geniusfinanceThemeString } from "../../theme/theme";


const Title = styled.div`
  display: flex;
  align-items: baseline;
  background-color: ${geniusfinanceThemeString.primary};
  padding: 10px;
  font-weight: bold;
  border-radius: 3px 3px 0 0;
  border-bottom: 11px solid #f1f1f1;
`;

const StyledCard = styled(Card)`
  margin: 20px;
`;

const CardTitle = styled.div`
  margin: 10px 0px 10px 20px;
  text-align: center;
`;


const PredictionCardContent = styled(CardContent)`
  margin: 5px 20px 10px 20px;
  height: 100px;
  position: relative;
`;

const VCenterText = styled.div`
  position: relative;
  top: 25px;
  text-align: center;
`;
const ColumnsContainer = styled.div`
  position: relative;
  display: flex;
  justify-content: space-between;
  @media only screen and (max-width: 800px) {
    flex-direction: column;
  }
`;

const Column = styled.div`
  float: left;
  width: 50%;
  @media only screen and (max-width: 800px) {
    width: 100%;
  }
`;

const RadialBarContainer = styled.div`
  position: relative;
  float: left;
  width: 50%;
  text-align: center;

  @media only screen and (max-width: 800px) {
    display: none;
  }
`;

export class PredictionCard extends Component {
  state = {
    price: 261.77,
    movement: "Fall",
    options: {
      plotOptions: {
        radialBar: {
          startAngle: -90,
          endAngle: 90,
          track: {
            background: "#e7e7e7",
            strokeWidth: "97%",
            margin: 5, // margin is in pixels
            shadow: {
              enabled: true,
              top: 2,
              left: 0,
              color: "#999",
              opacity: 1,
              blur: 2
            }
          },
          dataLabels: {
            name: {
              show: false
            },
            value: {
              offsetY: 15,
              fontSize: "22px"
            }
          }
        }
      },
      fill: {
        type: "gradient",
        gradient: {
          shade: "light",
          shadeIntensity: 0.4,
          inverseColors: false,
          opacityFrom: 1,
          opacityTo: 1,
          stops: [0, 50, 53, 91]
        }
      },
      labels: ["Possibility"]
    },
    series: [],
    possibility: [75.5],
    showAllLoading: true,
    showAll: false
  };
  componentDidMount() {
    const symbol = this.props.symbol;
    this.fetchData(symbol);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.symbol !== this.props.symbol) {
      this.setState({ showAllLoading: true, showAll: false });
      const symbol = this.props.symbol;
      this.fetchData(symbol);
    }
  }

  render() {
    return (
      <Paper>
        <Title>
          <Typography variant="h3" component="h1" color="secondary">
            <div style={{ marginLeft: "20px", display: "inline-block" }}>
              {" "}
              {"Prediction"}
            </div>
          </Typography>
        </Title>

          <Grid container spacing={0}>
            <Grid item xs={6}>
              <Grid item xs={12}>
                <StyledCard>
                  <CardTitle>
                    <Typography
                      color="inherit"
                      variant="h5"
                      style={{ fontWeight: "bold" }}
                    >
                      Price Prediction{" "}
                    </Typography>
                  </CardTitle>
                  <PredictionCardContent>
                    <VCenterText> 
                      {this.state.price !== undefined && (
                        <Typography variant="h3" component="h5" color="primary">
                          $ {this.state.price}
                        </Typography>
                      )}
                    </VCenterText>
                  </PredictionCardContent>
                </StyledCard>
              </Grid>
            </Grid>

            <Grid item xs={6}>
              <Grid item xs={12}>
                <StyledCard>
                  <CardTitle>
                    <Typography
                      color="inherit"
                      variant="h5"
                      style={{ fontWeight: "bold" }}
                    >
                      Movement Prediction{" "}
                    </Typography>
                  </CardTitle>
                  <PredictionCardContent>
                    <ColumnsContainer>
                      <Column>
                        <VCenterText>
                          { (
                            <Typography
                              variant="h3"
                              component="h5"
                              color="error"
                            >
                              Fall
                            </Typography>
                          )}
                          {this.state.movement === "Rise" && (
                            <Typography
                              variant="h3"
                              component="h5"
                              color="primary"
                            >
                              {this.state.movement}
                            </Typography>
                          )}
                        </VCenterText>
                      </Column>

                      <RadialBarContainer>
                        <Typography
                          variant="h6"
                          component="h5"
                          style={{
                            position: "relative",
                            top: "-20px",
                            textAlign: "center"
                          }}
                          color="inherit"
                        >
                          Accuracy
                        </Typography>
                        <div style={{ position: "absolute", top: "0px" }}>
                          <ReactApexChart
                            options={this.state.options}
                            series={this.state.possibility}
                            type="radialBar"
                            height="200"
                          />
                        </div>
                      </RadialBarContainer>
                    </ColumnsContainer>
                  </PredictionCardContent>
                </StyledCard>
              </Grid>
            </Grid>
          </Grid>
      </Paper>
    );
  }

  fetchData(symbol) {

    getMovementAndPricePrediction(symbol)
      .then(data => {
        this.setState({
          price: data.PredictPrice,
          movement: data.movement,
          possibility: [data.possibility],
          showAllLoading: false,
          showAll: true
        });
      })
      .catch(e => {
        this.setState({
          movement: "Prediction is not available",
          showAllLoading: false,
          showAll: false
        });
      });
  }

}
