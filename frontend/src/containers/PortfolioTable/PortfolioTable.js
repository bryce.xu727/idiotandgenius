import React from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import "./PortfolioTable.css";
import styled from "styled-components";
import Delete from "@material-ui/icons/Delete";
import { connect } from "react-redux";

import { SearchInput, NonStyledLink, EditableButton } from "../../components";
import {
  PortfolioViewTab,
  AddSymbolButton,
  MyHoldingTable,
  AnalysisPage
} from "../../containers";

import {
  getStockInfor,
  getPortfolioOwnedByUser,
  addStockToPortfolioOwnedByUser,
  deleteStockToPortfolioOwnedByUser
} from "../../api/api";
const usefulKeys = ["Name", "Share Volume", "Industry", "Sector"];

const DeleteButton = styled(Delete)`
  cursor: pointer;
  &:hover {
    color: #ff5546;
  }
`;

const EmptyWaringContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

const EmptyWaring = styled.h3`
  color: #d7d7d7;
`;

export class unconnectedPortfolioTable extends React.Component {
  state = {
    portfolioName: "",
    symbols: [], //for example:[GOOG,AAPL]
    rows: [], //symbol summary
    columns: usefulKeys,
    searching: "",
    showAction: false,
    tabNumber: 0
  };

  componentDidMount() {
    const { token, username, portfolioID } = { ...this.props };
    const { passSymbolToParent } = { ...this.props };
    console.log(portfolioID);
    getPortfolioOwnedByUser({ username, token, portfolioID })
      .then(result => {
        console.log(result);
        this.setState({ portfolioName: result.portfolioName });
        const symbolsList = result.stockWatchList;
        if (symbolsList) {
          symbolsList.forEach(symbol => {
            this.addSymbolToList(symbol);
          });
        }
        if (passSymbolToParent) {
          passSymbolToParent(symbolsList);
        }
      })
      .catch();
  }
  componentDidUpdate(prevProps, prevState, snapshot) {
    const { portfolioID: prevPortfolioID } = { ...prevProps };
    const { portfolioID: currPrevPortfolioID } = { ...this.props };
    if (prevPortfolioID !== currPrevPortfolioID) {
      const { token, username, portfolioID } = { ...this.props };
      const { passSymbolToParent } = { ...this.props };
      this.setState({
        symbols: [], //[GOOG,AAPL]
        rows: [], //symbol summary
        columns: usefulKeys,
        searching: "",
        tabNumber: 0
      });
      getPortfolioOwnedByUser({ username, token, portfolioID })
        .then(result => {
          console.log(result);
          const symbolsList = result.stockWatchList;
          this.setState({ portfolioName: result.portfolioName });
          if (symbolsList) {
            symbolsList.forEach(symbol => {
              this.addSymbolToList(symbol);
            });
          }
          if (passSymbolToParent) {
            passSymbolToParent(symbolsList);
          }
        })
        .catch();
    }
  }

  render = () => {
    const { username, token, portfolioID } = { ...this.props };
    const { portfolioName, columns, tabNumber, searching } = {
      ...this.state
    };
    delete columns["Symbol"];
    return (
      <div className="PortfolioTable_container">
        <Paper className="PortfolioTable_card">
          <div className="PortfolioTable_title_container">
            <Typography
              variant="h3"
              component="h1"
              className="PortfolioTable_title"
              style={{ overflow: "hidden" }}
            >
              {portfolioName}
            </Typography>
            <SearchInput
              handleSearchInputChange={this.handleSearchInputChange}
            />
          </div>

          <PortfolioViewTab
            onTabChange={this.updateTabNumber}
            portfolioID={portfolioID}
          >
            {tabNumber === 0 && (
              <>
                <EditableButton onClick={this.toggleShowAction} />
                <AddSymbolButton
                  onAdd={symbol => {
                    addStockToPortfolioOwnedByUser({
                      username,
                      token,
                      portfolioID,
                      symbol
                    });
                    this.addSymbolToList(symbol);
                  }}
                />
              </>
            )}
          </PortfolioViewTab>
          <div className="PortfolioTable_content">
            {tabNumber === 0 && (
              <>
                <Table style={{ tableLayout: "auto", minWidth: "max-content" }}>
                  {this.createStocksTable()}
                </Table>
                {this.state.rows && this.state.rows.length === 0 && (
                  <EmptyWaringContainer>
                    <EmptyWaring>Please add some stocks to start</EmptyWaring>
                  </EmptyWaringContainer>
                )}
              </>
            )}
            {tabNumber === 1 && (
              <>
                <Table style={{ tableLayout: "auto", minWidth: "max-content" }}>
                  {searching ? (
                    <MyHoldingTable
                      symbols={this.findMatchSymbols()}
                      portfolioID={portfolioID}
                    />
                  ) : (
                    <MyHoldingTable
                      symbols={this.state.symbols}
                      portfolioID={portfolioID}
                    />
                  )}
                </Table>
                {this.state.rows && this.state.rows.length === 0 && (
                  <EmptyWaringContainer>
                    <EmptyWaring>Please add some stocks to start</EmptyWaring>
                  </EmptyWaringContainer>
                )}
              </>
            )}
            {tabNumber === 2 && (
              <AnalysisPage
                portfolioID={portfolioID}
                credential={{ username, token }}
              />
            )}
          </div>
        </Paper>
      </div>
    );
  };

  deleteFromRow = (rows, rowIndex, symbol) => {
    const { token, username, portfolioID } = { ...this.props };
    deleteStockToPortfolioOwnedByUser({
      portfolioID,
      username,
      token,
      symbol
    }).then(() => {
      const newRows = [...rows];
      newRows.splice(rowIndex, 1);
      const newSymbols = [...this.state.symbols];
      newSymbols.splice(rowIndex, 1);
      this.setState({ rows: newRows, symbols: newSymbols });
    });
  };

  createStocksTable = () => {
    const { showAction, columns, searching, rows } = { ...this.state };
    return (
      <>
        <TableHead>
          <TableRow>
            {showAction && <TableCell>Action</TableCell>}
            {<TableCell key={`title_${Symbol}`}>Symbol</TableCell>}
            {columns.map(key => (
              <TableCell align="center" key={`title_${key}`}>
                {key}
              </TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {searching
            ? this.createTableRows(this.findMatchRows(), columns)
            : this.createTableRows(rows, columns)}
        </TableBody>
      </>
    );
  };

  createTableRows = (rows, columns) => {
    return rows
      ? rows.map((row, rowIndex) => {
          delete columns["Symbol"];
          return (
            <TableRow key={row.Symbol}>
              {this.state.showAction && (
                <TableCell component="td">
                  {/* row.name*/}
                  <DeleteButton
                    onClick={() => {
                      this.deleteFromRow(rows, rowIndex, row.Symbol);
                    }}
                  />
                </TableCell>
              )}
              <TableCell
                component="th"
                scope="row"
                key={`${row.Symbol}_name`}
                style={{ fontSize: "1.2em" }}
              >
                <NonStyledLink to={`/quote/${row["Symbol"]}`}>
                  {row["Symbol"]}
                </NonStyledLink>
              </TableCell>
              {columns.map(colomn => (
                <TableCell align="center" key={`${row.Symbol}_${colomn}`}>
                  {row[colomn]}
                </TableCell>
              ))}
            </TableRow>
          );
        })
      : [];
  };

  updateTabNumber = tabNumber => {
    this.setState({ tabNumber });
  };

  findMatchRows = () =>
    this.state.rows.filter(row =>
      row.Symbol.toLowerCase().includes(this.state.searching.toLowerCase())
    );

  findMatchSymbols = () =>
    this.state.symbols.filter(symbol =>
      symbol.toLowerCase().includes(this.state.searching.toLowerCase())
    );

  handleSearchInputChange = e => {
    console.log(e.target.value);
    this.setState({ searching: e.target.value });
  };

  addSymbolToList = symbol => {
    if (this.state.symbols && this.state.symbols.includes(symbol)) {
      return;
    }
    getStockInfor(symbol)
      .then(result => {
        console.log("success");
        const newRows = [...this.state.rows];
        const newSymbols = [...this.state.symbols];
        newRows.push(result);
        newSymbols.push(symbol);
        this.setState({ rows: newRows, symbols: newSymbols });
      })
      .catch();
  };

  toggleShowAction = () => {
    this.setState({ showAction: !this.state.showAction });
  };
}

const mapStateToProps = state => ({
  token: state.authenticationReducer.authentication.token,
  username: state.authenticationReducer.authentication.username
});
const mapDispatchToProps = dispatch => ({});

export const PortfolioTable = connect(
  mapStateToProps,
  mapDispatchToProps
)(unconnectedPortfolioTable);
