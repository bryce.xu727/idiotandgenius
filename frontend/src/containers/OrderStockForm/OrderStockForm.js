import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import Typography from '@material-ui/core/Typography';
import DialogTitle from "@material-ui/core/DialogTitle";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import { InputLabel, Input } from "@material-ui/core";
import { connect } from "react-redux";

import { addPositionToPortfolio } from "../../api/api";

export class unconnectedOrderStockForm extends React.Component {
  state = {
    portfolioID: "",
    positionSize: undefined,
    purchasePrice: undefined
  };
  render() {
    const { handleClose, open, sellForm, symbol } = { ...this.props };
    return (
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">
          {sellForm ? "Sell Stock" : "Buy Stock"}
          <Typography variant="h4" color="primary">We recommend this stock for you: {symbol === this.state.Pattern_name}</Typography>
        </DialogTitle>
        
        <DialogContent>
          
          {this.createPortfolioDropdown()}
          <TextField
            autoFocus
            margin="dense"
            id="size"
            label="Size"
            type="number"
            fullWidth
            onChange={this.handleSizeChange}
          />
          <TextField
            margin="dense"
            id="name"
            label="Price"
            type="number"
            fullWidth
            onChange={this.handlePriceChange}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button
            onClick={() => {
              this.addPosition();
            }}
            color="primary"
          >
            Create
          </Button>
        </DialogActions>
      </Dialog>
    );
  }

  handleSizeChange = e => {
    this.setState({ positionSize: e.target.value });
  };
  handlePriceChange = e => {
    this.setState({ purchasePrice: e.target.value });
  };
  handlePortfolioNameChange = e => {
    this.setState({ portfolioID: e.target.value });
  };

  updatePortfolioOption = e => {
    console.log(e.target.value);
    this.setState({ portfolioID: e.target.value });
  };
  createPortfolioDropdown = () => {
    const { portfolios } = { ...this.props };
    return (
      <>
        <InputLabel shrink htmlFor="portfolio-label-placeholder">
          Portfolio Name
        </InputLabel>
        <Select
          value={this.state.portfolioID}
          input={<Input name="portfolio" id="portfolio-label-placeholder" />}
          onChange={this.updatePortfolioOption}
          name="Portfolio Name"
          fullWidth
        >
          {portfolios.map(portfolio => (
            <MenuItem value={portfolio.portfolioId} key={portfolio.portfolioId}>
              {portfolio.portfolioName}
            </MenuItem>
          ))}
        </Select>
      </>
    );
  };

  isValidInput = ({ portfolioID, positionSize, purchasePrice }) => {
    if (portfolioID && positionSize && purchasePrice) {
      if (!Number.isInteger(parseFloat(positionSize))) {
        return false;
      }
      if (parseFloat(positionSize) <= 0 || parseFloat(purchasePrice <= 0)) {
        return false;
      }
      return true;
    }
    return false;
  };

  addPosition = () => {
    const { token, username, symbol, handleClose, sellForm, openSnackbar } = {
      ...this.props
    };
    const { positionSize, purchasePrice, portfolioID } = { ...this.state };
    if (!this.isValidInput({ positionSize, purchasePrice, portfolioID })) {
      openSnackbar({
        showSnackbarType: "error",
        showSnackbarInfor: "Invalid Input! Please follow the instruction."
      });
      return;
    }
    console.log("pass check");
    if (sellForm) {
      handleClose();
      addPositionToPortfolio({
        token,
        username,
        portfolioID,
        symbol,
        positionSize: -positionSize,
        purchasePrice
      })
        .then(() => {
          openSnackbar({
            showSnackbarType: "success",
            showSnackbarInfor: "Order Complete!"
          });
        })
        .catch(() => {
          openSnackbar({
            showSnackbarType: "error",
            showSnackbarInfor: "Order Fail!"
          });
        });
    } else {
      // buyform
      handleClose();
      addPositionToPortfolio({
        token,
        username,
        portfolioID,
        symbol,
        positionSize,
        purchasePrice
      })
        .then(() => {
          openSnackbar({
            showSnackbarType: "success",
            showSnackbarInfor: "Order Complete!"
          });
        })
        .catch(() => {
          openSnackbar({
            showSnackbarType: "error",
            showSnackbarInfor: "Order Fail!"
          });
        });
    }
  };
}

const mapStateToProps = state => ({
  token: state.authenticationReducer.authentication.token,
  username: state.authenticationReducer.authentication.username
});
const mapDispatchToProps = Dispatch => ({});

export const OrderStockForm = connect(
  mapStateToProps,
  mapDispatchToProps
)(unconnectedOrderStockForm);
