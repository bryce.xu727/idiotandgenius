import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import { InputLabel, Input } from "@material-ui/core";
import { connect } from "react-redux";

import { addAlarmTask } from "../../api/api";

export class unconnectedMonitorStockForm extends React.Component {
  state = {
    condition: "",
    target: "",
    conditionType: "",
    email: ""
  };
  render() {
    const { handleClose, open } = { ...this.props };
    return (
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Monitor Stock</DialogTitle>
        <DialogContent>
          <DialogContentText>
            {
              "To set an alarm for this stock, please choose a condition type that you want to monitor this stock with, and enter the target value. Please note that if you want to set an alarm for price, the value can't be negative"
            }
          </DialogContentText>
          {this.createConditionTypeDropdown()}
          {this.createConditionDropdown()}
          <TextField
            autoFocus
            margin="dense"
            id="target"
            label="Target Value"
            type="number"
            fullWidth
            onChange={this.handleTarget}
          />
          <TextField
            autoFocus
            margin="dense"
            id="target"
            label="Send email to"
            type="email"
            fullWidth
            onChange={this.handleEmail}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button
            onClick={() => {
              this.addTask();
            }}
            color="primary"
          >
            Create
          </Button>
        </DialogActions>
      </Dialog>
    );
  }

  handleTarget = e => {
    this.setState({ target: e.target.value });
  };

  handleEmail = e => {
    this.setState({ email: e.target.value });
  };

  handleConditionTypeChange = e => {
    this.setState({ conditionType: e.target.value });
  };

  handleConditionChange = e => {
    this.setState({ condition: e.target.value });
  };

  createConditionTypeDropdown = () => {
    return (
      <>
        <InputLabel shrink htmlFor="conditionType-label-placeholder">
          When price / change percentage
        </InputLabel>
        <Select
          value={this.state.conditionType}
          input={
            <Input name="conditionType" id="conditionType-label-placeholder" />
          }
          onChange={this.handleConditionTypeChange}
          name="conditionType"
          fullWidth
        >
          <MenuItem value={"price"}>Price</MenuItem>
          <MenuItem value={"percentage"}>Change Percentage</MenuItem>
          ))}
        </Select>
      </>
    );
  };

  createConditionDropdown = () => {
    return (
      <>
        <InputLabel shrink htmlFor="conditionType-label-placeholder">
          is larger / less than
        </InputLabel>
        <Select
          value={this.state.condition}
          input={
            <Input name="conditionType" id="conditionType-label-placeholder" />
          }
          onChange={this.handleConditionChange}
          name="condition"
          fullWidth
        >
          <MenuItem value={">="}>Larger or equal to</MenuItem>
          <MenuItem value={"<="}>Less or equal to</MenuItem>
          ))}
        </Select>
      </>
    );
  };

  isValidInput = ({ target, email, condition, conditionType }) => {
    console.log({ target, email, condition, conditionType });
    if (target && email && condition && conditionType) {
      if (!email.includes("@")) return false;
      if (conditionType === "price" && parseFloat(target) < 0) return false;
      return true;
    }
    return false;
  };

  addTask = () => {
    const { username, symbol, handleClose, openSnackbar } = {
      ...this.props
    };
    const { email, conditionType, condition, target } = { ...this.state };
    if (!this.isValidInput({ email, conditionType, condition, target })) {
      openSnackbar({
        showSnackbarType: "error",
        showSnackbarInfor: "Invalid Input! Please follow the instruction."
      });
      return;
    }
    console.log("pass check");

    handleClose();
    addAlarmTask({ username, email, conditionType, condition, target, symbol })
      .then(() => {
        openSnackbar({
          showSnackbarType: "success",
          showSnackbarInfor: "Set Alarm Success!"
        });
      })
      .catch(() => {
        openSnackbar({
          showSnackbarType: "error",
          showSnackbarInfor: "Set Alarm Fail!"
        });
      });
  };
}

const mapStateToProps = state => ({
  token: state.authenticationReducer.authentication.token,
  username: state.authenticationReducer.authentication.username
});
const mapDispatchToProps = Dispatch => ({});

export const MonitorStockForm = connect(
  mapStateToProps,
  mapDispatchToProps
)(unconnectedMonitorStockForm);
