import React from "react";
import { connect } from "react-redux";
import { getPositionGainList } from "../../api/api";
import { CircularProgress } from "@material-ui/core";
import { MyHoldingTableRow } from "../MyHoldingTableRow/MyHoldingTableRow";
import {
  Table,
  TableHead,
  TableCell,
  TableBody,
  TableRow
} from "@material-ui/core";
import styled from "styled-components";

const LoadingContainer = styled.div`
  position: absolute;
  width: 100%;
  hight: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export class unconnectedMyHoldingTable extends React.Component {
  columns = [
    "Symbol",
    "Equity",
    "Shares",
    "Market value",
    "Total gain",
    "Total gain%",
    "Lots"
  ];
  mapColumnsToDetail = {
    Symbol: "symbol",
    Equity: "equity",
    Shares: "shares",
    "Market value": "market value",
    "Total gain": "total gain",
    "Total gain%": "total gain (percentage)",
    Lots: "number of lots"
  };
  state = {
    positionObject: undefined,
    isLoading: false
  };
  componentDidMount() {
    const { token, username, portfolioID } = { ...this.props };
    this.updateTable({ token, username, portfolioID });
  }
  render() {
    const { symbols, portfolioID } = { ...this.props };
    const { positionObject, isLoading } = { ...this.state };
    return (
      <Table>
        <TableHead>
          <TableRow>
            <TableCell style={{ width: "10px" }} />
            {this.columns.map(column => (
              <TableCell key={column}>{column}</TableCell>
            ))}
          </TableRow>
        </TableHead>
        {isLoading && (
          <LoadingContainer>
            <CircularProgress />
          </LoadingContainer>
        )}
        <TableBody>
          {symbols &&
            positionObject &&
            symbols
              .sort()
              .map(symbol => (
                <MyHoldingTableRow
                  key={symbol}
                  detail={positionObject[symbol]}
                  mapColumnsToDetail={this.mapColumnsToDetail}
                  columns={this.columns}
                  symbol={symbol}
                  portfolioID={portfolioID}
                  updateTable={this.updateTable}
                  onLoading={this.turnOnLoading}
                  onLoaded={this.turnOffLoading}
                />
              ))}
        </TableBody>
      </Table>
    );
  }
  turnOnLoading = () => {
    this.setState({ isLoading: true });
  };
  turnOffLoading = () => {
    this.setState({ isLoading: false });
  };

  updateTable = ({ token, username, portfolioID }) => {
    this.setState({ isLoading: true });
    getPositionGainList({ token, username, portfolioID })
      .then(data => {
        this.setState({ positionObject: data, isLoading: false });
      })
      .catch(() => {
        this.setState({ isLoading: false });
      });
  };
}
const mapStateToProps = state => ({
  token: state.authenticationReducer.authentication.token,
  username: state.authenticationReducer.authentication.username
});
const mapDispatchToProps = dispatch => ({});

export const MyHoldingTable = connect(
  mapStateToProps,
  mapDispatchToProps
)(unconnectedMyHoldingTable);
