import React from "react";
import Tab from "@material-ui/core/Tab";
import Tabs from "@material-ui/core/Tabs";
import "./PortfolioViewTab.css";
import { withRouter } from "react-router";

import styled from "styled-components";
const ToolSection = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: baseline;
  width: fit-content;
  @media only screen and (max-width: 400px) {
    display: none;
  }
`;

export class unroutedPortfolioViewTab extends React.Component {
  state = {
    value: 0
  };

  componentDidUpdate(PrevProps) {
    if (PrevProps.location.pathname !== this.props.location.pathname) {
      this.setState({
        value: 0
      });
    }
  }
  handleChange = (event, value) => {
    const parentHandleTabChange = this.props.onTabChange;
    this.setState({ value });
    parentHandleTabChange(value);
  };
  render() {
    const { children } = { ...this.props };
    return (
      <div className="PortfolioViewTab_container">
        <Tabs
          value={this.state.value}
          onChange={this.handleChange}
          indicatorColor="primary"
          textColor="primary"
        >
          <Tab label="Watch list" />
          <Tab label="Holding" />
          <Tab label="Sector Analysis" />
        </Tabs>
        <ToolSection>{children}</ToolSection>
      </div>
    );
  }
}

export const PortfolioViewTab = withRouter(unroutedPortfolioViewTab);
