import React, { Component } from 'react';
import { LoginCard } from "../LoginCard/LoginCard" 
import RegistrationCard from '../RegistrationCard/RegistrationCard';
import "../../components/LoginPage/LoginPage.css"
import "./Card.css"

class Card extends Component {
    constructor(){
        super()
        this.state = {
            hideRegistration: true,
            hideLogin: false
        }
    }

    toggleLogin(){
        if (this.state.hideRegistration === false){
            this.setState({
                hideRegistration: !this.setState.hideRegistration
            })
        }        
        this.setState({
            hideLogin: !this.state.hideLogin,
        })
    }

    toggleRegistration(){
        if (this.state.hideLogin === false){
            this.setState({
                hideLogin: !this.setState.hideLogin
            })
        }
        this.setState({
            hideRegistration: !this.state.hideRegistration
        })
    }

    render(){
        return(
            <div className="container">
            <div className="divide-panel">
                <button type="button" className="panel" onClick={this.toggleLogin.bind(this)}>Login</button>               
                <button type="button" className="panel" onClick={this.toggleRegistration.bind(this)}>Registration</button>
            </div>  
            <div className="LoginPage-horizontalContainer">                
                {!this.state.hideLogin && <LoginCard />}
                {!this.state.hideRegistration && <RegistrationCard />}                
            </div>
            </div>
        )
    }   
}

export default Card;