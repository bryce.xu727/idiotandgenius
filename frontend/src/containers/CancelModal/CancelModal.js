import React from "react";
import "./CancelModal.css";

export class CancelModal extends React.Component {
  render() {
    const { handleClick } = { ...this.props };
    return <div className="CancelModal" onClick={handleClick} />;
  }
}
