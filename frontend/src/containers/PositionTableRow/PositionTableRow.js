import React from "react";
import styled from "styled-components";
import { TableRow, TableCell } from "@material-ui/core";
import Delete from "@material-ui/icons/Delete";
import { deletePosition } from "../../api/api";
import { connect } from "react-redux";

const DeleteButton = styled(Delete)`
  color: #f1f1f1;
  &:hover {
    color: #ff5546;
    cursor: pointer;
  }
`;

const StyledTableRow = styled(TableRow)`
  background-color: white;
  display: ${props => (props.show ? "block" : "none")};
`;

const mapColumnToAttribute = {
  "Trade date": "date",
  "Cost base": "cost base",
  Size: "shares",
  "Total gain": "total gain",
  "Total gain%": "total gain (percentage)"
};
class unconnectedPositionTableRow extends React.Component {
  state = { showRow: true };
  render() {
    const { position, columns, username, token, portfolioID } = {
      ...this.props
    };
    return (
      <StyledTableRow show={this.state.showRow}>
        {columns &&
          columns.map(column => (
            <TableCell key={`${position["position id"]}_${column}`}>
              {column === "Total gain%"
                ? `${(position[mapColumnToAttribute[column]] * 100).toFixed(
                    2
                  )}%`
                : position[mapColumnToAttribute[column]]}
            </TableCell>
          ))}
        <TableCell>
          <DeleteButton
            onClick={() => {
              this.deletePositionFromRow({
                positionID: position["position id"],
                portfolioID,
                username,
                token
              });
            }}
          />
        </TableCell>
      </StyledTableRow>
    );
  }
  deletePositionFromRow = ({ portfolioID, positionID, username, token }) => {
    console.log("deleting");
    const { updateTable, deletePosition: frontendDeletePosition } = {
      ...this.props
    };

    frontendDeletePosition();
    deletePosition({ username, positionID, token, portfolioID })
      .then(() => {
        updateTable();
      })
      .catch();
  };
}

const mapStateToProps = state => ({
  token: state.authenticationReducer.authentication.token,
  username: state.authenticationReducer.authentication.username
});
const mapDispatchToProps = Dispatch => ({});

export const PositionTableRow = connect(
  mapStateToProps,
  mapDispatchToProps
)(unconnectedPositionTableRow);
