import React from "react";
import { connect } from "react-redux";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import { PrivateRoute } from "./routes/routes";

import {
  LoginPage,
  PortfolioRoutes,
  QuoteRoutes,
  NavigationBar
} from "./components";

import { MonitoringPage } from "./containers";
import { getSimplifiedStock } from "./api/api";
import { stockSymbolActions } from "./reducers/actions";
import { ThemeProvider } from "@material-ui/styles";
import { geniusfinanceTheme } from "./theme/theme";

const mapStateToProps = state => ({
  token: state.authenticationReducer.authentication.token,
  isAuthenticated: true && state.authenticationReducer.authentication.token,
  symbols: state.symbolsReducer.symbols
});

const mapDispatchToProps = Dispatch => ({
  setSymbols: symbols => Dispatch(stockSymbolActions.SET_SYMBOLS(symbols))
});

class unconnectedApp extends React.Component {
  componentDidMount() {
    getSimplifiedStock()
      .then(result => {
        console.log(result);
        this.props.setSymbols(result);
      })
      .catch();
  }
  render() {
    const { isAuthenticated } = { ...this.props };
    return (
      <>
        <ThemeProvider theme={geniusfinanceTheme}>
          <Router>
            <NavigationBar />
            <Switch>
              <Route
                path="/login"
                render={() =>
                  isAuthenticated ? (
                    <Redirect to="/portfolios" />
                  ) : (
                    <LoginPage />
                  )
                }
              />
              <Route
                exact
                path="/"
                render={() =>
                  isAuthenticated ? (
                    <Redirect to="/portfolios" />
                  ) : (
                    <LoginPage />
                  )
                }
              />
              <PrivateRoute path="/quote" component={QuoteRoutes} />
              <PrivateRoute path="/portfolios" component={PortfolioRoutes} />
              <PrivateRoute path="/monitoring" component={MonitoringPage} />
            </Switch>
          </Router>
        </ThemeProvider>
      </>
    );
  }
}


const App = connect(
  mapStateToProps,
  mapDispatchToProps
)(unconnectedApp);

export default App;
