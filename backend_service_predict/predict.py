#! ./venv/bin/python3
import torch
import lstm.lstm as lstm
import data.preprocess as dp
import numpy as np
from urllib.request import urlopen
import json
import sys
from io import StringIO
import pandas as pd
import pickle


# this file include some api call by server
# predict, recommend, and compute accuracy

# Match predicted stock with 50 known stock and predict
def predict_price(symbol):
    if not check_symbol(symbol):
        return False
    cls, _ = get_similar_stock(symbol)
    m = torch.load("./model/" + cls + "_lstm.pkl")
    u = urlopen("https://api.financialmodelingprep.com/api/v3/historical-price-full/" + symbol + "?serietype=line")
    resp = json.loads(u.read().decode("utf-8"))
    data = "date,close\n"
    for s in resp["historical"]:
        data += s["date"]
        data += ","
        data += str(s["close"])
        data += "\n"
    dataf = StringIO(data)
    temp = pd.read_csv(dataf)
    temp["date"] = pd.to_datetime(temp["date"], format="%Y-%m-%d")
    temp = temp.set_index("date").sort_index()
    max_val = max(temp["close"])
    min_val = min(temp["close"])
    x, y = dp.generate_train_data(dp.min_max_normalise(temp), 10)
    pred = lstm.test(m, x[-1])
    pred = pred * (max_val - min_val) + min_val
    cur_p = temp["close"][-1] * (max_val - min_val) + min_val
    incr = 1
    if cur_p > pred[-1]:
        incr = 0
    return str(pred[-1]), str(cur_p), incr

# using KNN classifer to find similar stock
def get_similar_stock(symbol):
    u = urlopen("https://api.financialmodelingprep.com/api/v3/historical-price-full/" + symbol + "?serietype=line")
    resp = json.loads(u.read().decode("utf-8"))
    data = "date,close\n"
    for s in resp["historical"]:
        data += s["date"]
        data += ","
        data += str(s["close"])
        data += "\n"
    dataf = StringIO(data)
    temp = pd.read_csv(dataf)
    temp["date"] = pd.to_datetime(temp["date"], format="%Y-%m-%d")
    temp = temp.set_index("date").sort_index()
    max_val = max(temp["close"])
    min_val = min(temp["close"])
    t = dp.min_max_normalise(temp)[-30:].to_numpy().reshape(1, -1)
    neigh = pickle.load(open("./model/knn.sav", "rb"))
    result = neigh.predict(t)
    ti = list(np.datetime_as_string(temp.index.values, unit="D"))[-30:]
    return result[0], list(temp[-30:].to_numpy().reshape(-1) * (max_val - min_val) + min_val), ti

# compute accuracy of stock using historical data
def accuracy(symbol):
    if not check_symbol(symbol):
        return False
    cls, _, _ = get_similar_stock(symbol)
    m = torch.load("./model/" + cls + "_lstm.pkl")

    temp = dp.read_data("./data/stocks/" + cls + ".csv")
    temp = dp.min_max_normalise(temp)
    x, y = dp.generate_train_data(temp, 10)
    y = y.numpy().reshape(-1)
    pred = lstm.test(m, x)
    corr = 0
    for i in range(len(pred)):
        if pred[i] > x[i][-1] and y[i] > x[i][-1]:
            corr += 1
        if pred[i] < x[i][-1] and y[i] < x[i][-1]:
            corr += 1
    return str(corr / len(pred))

# get the similar stock price.
def pattern(symbol):
    if not check_symbol(symbol):
        return False
    clr, source, time = get_similar_stock(symbol)
    temp = dp.read_data("./data/stocks/" + clr + ".csv")
    result = temp[-30:].to_numpy().reshape(-1)
    return clr, list(result), time,list(source), list(np.datetime_as_string(temp.index.values, unit="D"))[-30:]

# get the cash flow to debt ratio from api
def ratio(symbol):
    if not check_symbol(symbol):
        return False
    u = urlopen("https://financialmodelingprep.com/api/financial-ratios/" + symbol + "?datatype=json")
    resp = json.loads(u.read().decode("utf-8"))["financialRatios"]
    time = list(resp.keys())[-2]
    return resp[time]["debtRatios"]["cashFlowToDebtRatio"]

# check the symbol is valid
def check_symbol(symbol):
    u = urlopen("https://api.financialmodelingprep.com/api/stock/list/all?datatype=json")
    resp = json.loads(u.read().decode())
    stocks = set()
    for stock in resp:
        stocks |= {stock["Ticker"]}
    if symbol not in stocks:
        return False
    return True


#print(check_symbol("BIDU"))
#print(predict_price("BIDU"))
#print(accuracy("BIDU"))
#print(pattern("BIDU"))
#print(ratio("BIDU"))
