# this is the script for all backend libraries
# just run this code, it could install all libraries
import os

libs = {"flask", "flask_socketio", "torch", "numpy", "pandas", "scikit-learn"}
try:
    for lib in libs:
        os.system(" pip install " + lib)
        print("{}   Install successful".format(lib))
except:
    print("{}   failed install".format(lib))
