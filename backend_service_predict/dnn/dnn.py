import torch
from torch import nn
from torch.autograd import Variable
import matplotlib.pyplot as plt

# DNN model 1 full connected layer from 10 to 1
# and function for training and test

class DNN(nn.Module):
    def __init__(self):
        super(DNN, self).__init__()
        self.linear_1 = nn.Linear(10, 1)

    def forward(self, x):
        x, _ = self.linear_1(x)
        return x


def training(x_train, y_train, num_epochs):
    model = DNN()
    loss_function = nn.MSELoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=1e-2)
    for i in range(num_epochs):
        x = Variable(x_train.type(torch.FloatTensor))
        y = Variable(y_train.type(torch.FloatTensor))
        out = model(x)
        loss = loss_function(out, y)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        if (i + 1) % 200 == 0:
            print('Epoch: {}, Loss:{:.5f}'.format(i + 1, loss.data))
    return model

def test(model, test_x):
    model = model.eval()
    test_x = Variable(test_x.type(torch.FloatTensor))
    pred_test = model(test_x)
    pred_test = pred_test.view(-1).data.numpy()
    return pred_test
