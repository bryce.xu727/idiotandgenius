from sklearn.neighbors import KNeighborsClassifier
import os
import data.preprocess as dp
import pickle

# using 50 stocks to classified stock
# knn model from scikit learn and save model
x = []
y = []
stocks = os.listdir("../data/stocks/")
for i in range(len(stocks)):
    stocks[i] = stocks[i][:-4]

for stock in stocks:
    temp = dp.read_data("../data/stocks/" + stock + ".csv")
    temp = dp.min_max_normalise(temp)[-30:]["close"]
    x.append(temp)
    y.append(stock)

neigh = KNeighborsClassifier(n_neighbors=3)
neigh.fit(x, y)
pickle.dump(neigh, open("../model/knn.sav", "wb"))





