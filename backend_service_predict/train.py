#! ./venv/bin/python3

import data.preprocess as dp
import lstm.lstm as lstm
import pandas as pd
import torch
import csv
import os

# training 50 stocks to 50 LSTM model

x_train = torch.tensor([])
y_train = torch.tensor([])

stocks = os.listdir("./data/stocks")
for i in range(len(stocks)):
    stocks[i] = stocks[i][:-4]

for stock in stocks:
    temp = dp.read_data("./data/stocks/" + stock + ".csv")
    temp = dp.min_max_normalise(temp)
    x_train, y_train = dp.generate_train_data(temp, 10)
    x_train = x_train.reshape(-1, 1, 10)
    y_train = y_train.reshape(-1, 1, 1)
    model = lstm.training(5000, x_train, y_train)
    torch.save(model, "./model/" + stock + "_lstm.pkl")
