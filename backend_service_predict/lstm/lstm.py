import torch
from torch import nn
from torch.autograd import Variable
import matplotlib.pyplot as plt

# LSTM model: LSTM + relu + linear
# and function for training and predict
class LSTM(nn.Module):
    def __init__(self, input_size, hidden_size, output_size=1, num_layers=2):
        super(LSTM, self).__init__()

        self.rnn = nn.LSTM(input_size, hidden_size, num_layers, batch_first=True)  # rnn
        self.reg = nn.Linear(hidden_size, output_size)  # 回归

    def forward(self, x):
        x, _ = self.rnn(x)  # (seq, batch, hidden)
        s, b, h = x.shape
        x = x.view(s * b, h)  # 转换成线性层的输入格式
        x = self.reg(x)
        x = x.view(s, b, -1)
        return x

def training(num_epochs, x_train, y_train):
    model = LSTM(10, 64, output_size=1, num_layers=2)##8,16,40,64
    #model = nn.DataParallel(model, device_ids=[0])

    loss_function = nn.MSELoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=1e-2)

    for i in range(num_epochs):
        x = Variable(x_train.type(torch.FloatTensor))#.cuda()
        y = Variable(y_train.type(torch.FloatTensor))#.cuda()
        out = model(x)
        loss = loss_function(out, y)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        if (i + 1) % 200 == 0:
            print('Epoch: {}, Loss:{:.5f}'.format(i + 1, loss.data))
    return model

def test(model, test_x):
    days_train = 10
    model = model.eval()
    test_x = test_x.reshape(-1, 1, days_train)
    test_x = Variable(test_x.type(torch.FloatTensor))
    pred_test = model(test_x)
    pred_test = pred_test.view(-1).data.numpy()
    return pred_test
