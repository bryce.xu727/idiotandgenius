import pandas as pd
import torch
import sys
import os

# this file is some function about data process include the
# read data min max normalisation and make training data


# read data from training file
def read_data(path):
    if not os.path.exists(path):
        print("File not exist...")
        sys.exit()
    data = pd.read_csv(path)
    data["date"] = pd.to_datetime(data["date"], format="%Y-%m-%d")
    data = data.set_index("date").sort_index()
    return data

# min max normalisation
def min_max_normalise(data):
    max_val = max(data["close"])
    min_val = min(data["close"])
    data["close"] = (data["close"] - min_val) / (max_val - min_val)
    return data

# make trainging format
def generate_train_data(data, feature):
    x_train = []
    y_train = []
    for i in range(len(data) - feature):
        t = []
        for j in range(feature):
            t.append(data["close"][i - j])
        x_train.append(t)
        y_train.append([data["close"][i + feature]])
    x_train = torch.tensor(x_train).type(torch.FloatTensor)
    y_train = torch.tensor(y_train).type(torch.FloatTensor)
    return x_train, y_train
