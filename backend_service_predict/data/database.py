import json
from urllib.request import urlopen
import pandas as pd
from io import StringIO
import re
import random
import sys

#DIR = "./stocks/"
#all_stock_list = pd.read_json("https://api.financialmodelingprep.com/api/stock/list/all?datatype=json")
#random.seed(10)
#selected_stock_list = random.sample(range(len(all_stock_list)), 50)
#for i in range(len(selected_stock_list)):
#    selected_stock_list[i] = all_stock_list["Ticker"][i]
selected_stock_list = []
for i in sys.argv[1:]:
    selected_stock_list.append(i)

result = []
print(selected_stock_list)
for stock in selected_stock_list:
    u = urlopen("https://api.financialmodelingprep.com/api/financial-ratios/" + stock + "?datatype=csv")
    data = u.read().decode("ascii")
    dataf = StringIO(data)
    d = pd.read_csv(dataf)
    head = d.columns

    for i in range(len(head)):
        if re.findall("^2018", head[i]):
            s = head[i]
            print(s)
            break

    d = d.rename(columns={s:stock})
    if not result:
        result = d[["Ratios", stock]].T
    else:
        result = pd.concat([result, d[["Ratios", stock]].T])
print(result)
