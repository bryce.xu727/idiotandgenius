import json
from urllib.request import urlopen
from alpha_vantage.timeseries import TimeSeries
from pprint import pprint
import time

stock_list = ["ATVI", "ADBE", "AMD", "ALXN", "ALGN", "GOOGL", "GOOG",
             "AMZN", "AAL", "AMGN", "ADI", "AAPL", "AMAT", "ASML", "ADSK", "ADP", "BIDU", "BIIB", "BMRN", "BKNG", "AVGO", "CDNS", "CELG", "CERN", "CHTR",
             "CHKP", "CTAS", "CSCO", "CTXS", "CTSH", "CMCSA", "COST", "CSX", "CTRP", "DLTR", "EBAY",
             "EA", "EXPE", "FB", "FAST", "FISV", "FOX", "FOXA", "GILD", "HAS", "HSIC", "IDXX",
             "ILMN", "INCY", "INTC", "INTU", "ISRG", "JBHT", "JD", "KLAC", "LRCX", "LBTYA",
             "LBTYK", "LULU", "MAR", "MXIM", "MELI", "MCHP", "MU", "MSFT", "MDLZ", "MNST", "MYL",
             "NTAP", "NTES", "NFLX", "NVDA", "NXPI", "ORLY", "PCAR", "PAYX", "PYPL", "PEP", "QCOM",
             "REGN", "ROST", "SIRI", "SWKS", "SBUX", "SYMC", "SNPS", "TMUS", "TTWO", "TSLA", "TXN",
             "KHC", "ULTA", "UAL", "VRSN", "VRSK", "VRTX", "WBA", "WDAY", "WDC", "WLTW", "WYNN",
             "XEL", "XLNX"]

f = open("444.json", "w")

for symbol in stock_list[50:]:
    ts = TimeSeries(key='OCH47F1P8KQO99RQ', output_format="json")
    data, meta_data = ts.get_intraday(symbol=symbol,interval='5min', outputsize='compact')
    for i in data:
        data[i].update({"open": data[i].pop("1. open")})
        data[i].update({"high": data[i].pop("2. high")})
        data[i].update({"low": data[i].pop("3. low")})
        data[i].update({"close": data[i].pop("4. close")})
        data[i].update({"volume": data[i].pop("5. volume")})
    data["Symbol"] = symbol
    js = json.dumps(data)
    f.write(js)
    f.write("\n")
    time.sleep(10)
f.close()

