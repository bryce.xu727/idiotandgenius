import pandas as pd
from urllib.request import urlopen
import json
import random
import os
import sys


# mine data from website for training

def read_json(symbol):
    u = urlopen("https://api.financialmodelingprep.com/api/v3/historical-price-full/" + symbol + "?serietype=line")
    resp = json.loads(u.read().decode("utf-8"))
    data = "date,close\n"
    for s in resp["historical"]:
        data += s["date"]
        data += ","
        data += str(s["close"])
        data += "\n"
    with open(DIR + stock +".csv", "w") as f:
        f.write(data)


if len(sys.argv) != 2 and sys.argv[1] not in {0, 1}:
    print("Usage: python stock.py [1/0]")
    sys.exit()

DIR = "./stocks/"

all_stock_list = pd.read_json("https://api.financialmodelingprep.com/api/stock/list/all?datatype=json")

random.seed(10)
selected_stock_list = random.sample(range(len(all_stock_list)), 50)
for i in range(len(selected_stock_list)):
    selected_stock_list[i] = all_stock_list["Ticker"][i]

for stock in selected_stock_list:
    if not os.path.exists(DIR + stock + ".csv"):
        read_json(stock)
    else:
        if sys.argv[1] == "1":
            read_json(stock)

