#for this file, it achieves the server and interaction with front side
 
from flask import Flask, render_template, jsonify, request, make_response
from flask_socketio import SocketIO
import random
import predict as pre
import json

app = Flask(__name__)
socketio = SocketIO(app)

@socketio.on('connect', namespace='/test_conn')
def test_connect():
    socketio.emit('server_response', {'data': 'sucess'}, namespace='/test_conn')

@app.route('/Stock/<stock>', methods=['POST', 'GET'])
def get_stock(stock):
    stocklist = []
    stocklist.append(stock)
    #predictprice, currentprice, raiseandfall = pre.predict_price(stock)
    accuracy = pre.accuracy(stock)
    #pattern_name, pattern, parttern_time, periodprice, period_time = pre.pattern(stock)
    json_file = """
        {
            "Symbol": null,
            "CurrentPrice": null,
            "PredictPrice": null,
            "Pattern": null,
            "Pattern_time": null,
            "PeriodPrice": null,
            "Period_time": null,
            "Pattern_name": null,
            "Accuracy": null,
            "Raise_Fall": null
        }
    """
    json_file = json.loads(json_file)
    if not accuracy:
        return make_response(jsonify({'stock': 'Not found'}), 401)
    else:
        predictprice, currentprice, raiseandfall = pre.predict_price(stock)
        pattern_name, pattern, parttern_time, periodprice, period_time = pre.pattern(stock)
        json_file["Symbol"] = stock
        json_file["CurrentPrice"] = currentprice
        json_file["PredictPrice"] = predictprice
        json_file["Pattern"] = pattern
        json_file["PeriodPrice"] = periodprice
        json_file["Accuracy"] = accuracy
        json_file["Pattern_name"] = pattern_name
        json_file["Pattern_time"] = parttern_time
        json_file["Period_time"] = period_time
        if raiseandfall == 1:
            json_file["Raise_Fall"] = "Raise"
        if raiseandfall == 0:
            json_file["Raise_Fall"] = "Fall"
        return make_response(jsonify(json_file))

# @app.route('/Portfolio', methods=['POST', 'GET'])
# def get_stock():
#     data = request.get_data()
#     data = json.loads(data)
#     stock_list = []
#     for stock in data:
#         stock_list.append(stock['name'])
#     json_data = pre.predict(stock_list)
#     return jsonify(json_data)

@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

if __name__ == '__main__':
    socketio.run(app, debug=True, port = 9999)