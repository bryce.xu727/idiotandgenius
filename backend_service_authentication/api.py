# A service to handle the user registration and login
from flask import Flask
from flask_restful import Resource, Api, reqparse
import requests, datetime, json
from flask_jwt_extended import create_access_token, JWTManager
from werkzeug.security import generate_password_hash, check_password_hash
from flask_cors import CORS
from pymongo import MongoClient
import pymongo

def connect_mongodb():
    """
    A function to connect to mongodb and return user collection
    
    Returns:
        [pymongo.collection.Collection] -- [Collection of user]
    """

    client = MongoClient('mongodb+srv://DBAdmin:GeniusFinanceDBAdmin@geniusfinance-p7ayy.gcp.mongodb.net/test?retryWrites=true&w=majority')
    db = client['comp9900_genius_finance']
    collection = db['user']
    collection.create_index([("username", pymongo.ASCENDING)],unique=True)
    return collection

app = Flask(__name__)
app.config['JWT_SECRET_KEY'] = 'Genius user'
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = datetime.timedelta(days=1)
api = Api(app)
jwt = JWTManager(app)
CORS(app)

# create parser to get the input from request
parser = reqparse.RequestParser()
parser.add_argument('username')
parser.add_argument('password')

class login(Resource):
    def post(self):
        payload = {"message":"Incorrect Username and password"}
        args = parser.parse_args()
        username = args.get('username')
        password = args.get('password')
        user_collection = connect_mongodb()
        #search in the NOSQL and validate the login details
        result = user_collection.find({"username":username})
        if result:
            for user in result:
                if check_password_hash(user["password"],password):
                    token_info = {"username":user["username"]}
                    access_token = create_access_token(identity = json.dumps(token_info))
                    payload.update({"message":"success","access_token":access_token})
                    return json.dumps(payload), 200
        
        return json.dumps(payload), 404

class registration(Resource):
    def post(self):
        args = parser.parse_args()
        username = args.get('username')
        password = generate_password_hash(args.get('password'))
        user_collection = connect_mongodb()
        try:
            user_collection.insert_one({"username":username,"password":password})    
            payload = {
                "message":"The user {} create success".format(username)
            }
            return json.dumps(payload), 201
        except:
            payload = {
                "message":"The user {} has been created before. Please use another name".format(username)
            }
            return json.dumps(payload), 400

api.add_resource(login, '/login')
api.add_resource(registration, '/registration')

if __name__ == '__main__':
    app.run(debug=True, port=9111)
