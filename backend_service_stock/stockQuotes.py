from flask_socketio import send, emit
import time
import requests
import json
import datetime


class StockQuotes:
    def __init__(self):
        self.baseUrl = "http://localhost:8100"

    def get_realtime_quote(self, symbol):
        try:
            r = requests.get(self.baseUrl+"/stocks/{}/quote".format(symbol))
            result = json.loads(r.text)
        except:
            return None
        return result

    def format_realtime_quote(self, quote_dict):
        result = dict()
        symbol = quote_dict.pop("Symbol")
        result["Meta"] = dict()
        result["Meta"]["Symbol"] = symbol

        all_time_stamps = quote_dict.keys()
        formated_series = []
        for time_stamp in all_time_stamps:
            data_entry = {"x": int(datetime.datetime.timestamp(datetime.datetime.strptime(time_stamp, "%Y-%m-%d %H:%M:%S"))),
                          "y": [quote_dict[time_stamp]["open"],
                                quote_dict[time_stamp]["high"],
                                quote_dict[time_stamp]["low"],
                                quote_dict[time_stamp]["close"],
                                quote_dict[time_stamp]["volume"]
                                ]
                          }

            formated_series.append(data_entry)
        result["Series"] = formated_series
        if len(formated_series) == 0:
            now = datetime.datetime.now()  # show default range
            result["Meta"]["openTime"] = int(1000 * datetime.datetime.timestamp(
                now.replace(hour=9, minute=30, second=0)))
            result["Meta"]["closeTime"] = int(1000 * datetime.datetime.timestamp(
                now.replace(hour=16, minute=0, second=0)))
        else:
            tradingDayTimeStamp = formated_series[0]["x"]
            tradingDay = datetime.datetime.fromtimestamp(tradingDayTimeStamp)
            result["Meta"]["openTime"] = int(1000 * datetime.datetime.timestamp(
                tradingDay.replace(hour=9, minute=30, second=0)))
            result["Meta"]["closeTime"] = int(1000 * datetime.datetime.timestamp(
                tradingDay.replace(hour=16, minute=0, second=0)))
        return result


def handle_open_event(symbol):
    print("quote  connect success")
    sq = StockQuotes()
    while True:
        data = sq.get_realtime_quote(symbol)
        if data is None:
            data = dict()
            data["status code"] = 404
        emit("update", sq.format_realtime_quote(data))
        time.sleep(10)
