from flask import Flask
from flask_cors import CORS
from flask_restful import Api,Resource, reqparse
from flask_socketio import send, emit, SocketIO
import time
import requests
import json
from stockQuotes import handle_open_event as handle_quote_open_event
from pymongo import MongoClient

class StockDb:
    _mongodbUrl = "mongodb+srv://DBAdmin:GeniusFinanceDBAdmin@geniusfinance-p7ayy.gcp.mongodb.net/test?retryWrites=true&w=majority"
    _db = None
    _nasdaq100 = ["ATVI", "ADBE", "AMD", "ALXN", "ALGN", "GOOGL", "GOOG",
                "AMZN", "AAL", "AMGN", "ADI", "AAPL", "AMAT", "ASML", "ADSK", "ADP", "BIDU", "BIIB", "BMRN",
                "BKNG", "AVGO", "CDNS", "CELG", "CERN", "CHTR",
                "CHKP", "CTAS", "CSCO", "CTXS", "CTSH", "CMCSA", "COST", "CSX", "CTRP", "DLTR", "EBAY",
                "EA", "EXPE", "FB", "FAST", "FISV", "FOX", "FOXA", "GILD", "HAS", "HSIC", "IDXX",
                "ILMN", "INCY", "INTC", "INTU", "ISRG", "JBHT", "JD", "KLAC", "LRCX", "LBTYA",
                "LBTYK", "LULU", "MAR", "MXIM", "MELI", "MCHP", "MU", "MSFT", "MDLZ", "MNST", "MYL",
                "NTAP", "NTES", "NFLX", "NVDA", "NXPI", "ORLY", "PCAR", "PAYX", "PYPL", "PEP", "QCOM",
                "REGN", "ROST", "SIRI", "SWKS", "SBUX", "SYMC", "SNPS", "TMUS", "TTWO", "TSLA", "TXN",
                "KHC", "ULTA", "UAL", "VRSN", "VRSK", "VRTX", "WBA", "WDAY", "WDC", "WLTW", "WYNN",
                "XEL", "XLNX"]

    def __init__(self):
        mgclient = MongoClient(self._mongodbUrl)
        self._db = mgclient.comp9900_genius_finance

    def getKeyInforBySymbol(self, symbol):
        result = self._db.stocks.find_one({'Symbol': symbol.upper()})
        if result:
            result.pop("_id", None)
        return result

    def getallSearchableSymbols(self):
        results = self._db.stocks.find()
        symbols = []
        for result in results:
            if result['Symbol'] in self._nasdaq100:
                symbols.append(result)
        return [{"Symbol": symbol["Symbol"], "Sector":symbol["Sector"]} for symbol in symbols]

stockdb = StockDb()

errors = {
    "404": {"status code": 404, "message": "resources not found"}
}

class StockInfor(Resource):
    def get(self, symbol):
        result = stockdb.getKeyInforBySymbol(symbol)
        if not result:
            return errors["404"], 404
        return result, 200


class AllSearchableStockInfor(Resource):
    def get(self):
        result = stockdb.getallSearchableSymbols()
        if not result:
            return errors["404"], 404
        return result, 200
class StockSummary:
    def __init__(self):
        self.baseUrl = "http://localhost:8100"

    def get_realtime_quote_summary(self, symbol):
        try:
            r = requests.get(
                self.baseUrl+"/stocks/{}/quote_summary".format(symbol))
            result = json.loads(r.text)
        except:
            return None
        return result


def handle_open_event(symbol):
    print("connect success")
    sq = StockSummary()
    for i in range(5):
        data = sq.get_realtime_quote_summary(symbol)
        if data is None:
            data = dict()
            data["status code"] = 404
        emit("update", data)
        time.sleep(10)


app = Flask(__name__)
socketio = SocketIO(app)
api = Api(app)

CORS(app)
api.add_resource(StockInfor, '/stocks/<string:symbol>/information')
api.add_resource(AllSearchableStockInfor, '/stocks')

if __name__ == '__main__':
    app.run(debug=True, port=9333)
