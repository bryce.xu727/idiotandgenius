# Stock Price Replay Service

- [Stock Price Replay Service](#stock-price-replay-service)
  - [key information of a stock](#key-information-of-a-stock)

> To run this service

```
pip3 install -r requirement.txt
python3 run.py
```
 
## key information of a stock

**Base URL** 127.0.0.1:9333
**PATH**: /stocks/{symbol}/information

For example, if we access this URL "http://127.0.0.1:9333/stocks/YI/information", the following will be the output, if the `symbol` is valid and a status code of `200` is return.

```JSON
{
    "Best Bid / Ask": "N/A / N/A",
    "Today's High / Low": "$ 6.85 / $ 6.51",
    "Share Volume": "21,980",
    "50 Day Avg Daily Volume": "27,332",
    "Previous Close": "$ 6.62",
    "52 Week High / Low": "$ 16.83 / $ 5.60",
    "Market Cap": "540,580,356",
    "Annualized Dividend": "N/A",
    "Ex Dividend Date": "N/A",
    "Dividend Payment Date": "N/A",
    "Current Yield": "0 %",
    "Symbol": "YI",
    "Name": "111, Inc.",
    "IPO year": "2018",
    "Industry": "Medical/Nursing Services",
    "Sector": "Health Care",
    "ADR TSO": "12907195"
}
```

If the symbol you are looking for is not stored in the database, you will receive the folling response with a `404` status code.

```
{
    "status code": 404,
    "message": "resources not found"
}
```
