from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Numeric, Date
from datetime import datetime

Base = declarative_base()


class Company(Base):
    __tablename__ = 'companies'
    symbol = Column(String, primary_key=True)
    fullname = Column(String)
    lastScale = Column(String)
    marketCap = Column(String)
    adr_tso = Column(String)
    ipoyear = Column(String)
    sector = Column(String)
    industry = Column(String)
    nasdaqLink = Column(String)

    # P/E Ratio
    #peRatio = Column(String)
    # Forward P/E
    #forwardPeRatio = Column(String)
    # Forward P/E (1y)
    # Earnings Per Share (EPS)
    # Annualized Dividend
    # Ex Dividend Date
    # Dividend Payment Date
    # Current Yield
    # Beta

    @classmethod
    def verified_numeric(cls, number):
        try:
            number = float(number)
        except:
            number = None
        return number

    @classmethod
    def verified_ipoyear(cls, ipoyear):
        try:
            ipoyear = datetime.strptime(ipoyear, "%Y")
            print("ipoyear", ipoyear)
        except:
            return None
        return ipoyear

    def __repr__(self):
        return "<Company(symbol='%s', fullname='%s', ipoyear='%s')>" % (
            self.symbol, self.fullname, self.ipoyear)
