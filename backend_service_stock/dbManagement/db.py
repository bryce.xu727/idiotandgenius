from utils.crawler.crawler import NasdaqReportCrawler
from sqlalchemy import create_engine, MetaData
from models import Company
import csv
from sqlalchemy.orm import sessionmaker
from pymongo import MongoClient
from datetime import datetime
import time
import requests
import json

nasdaq100 = ["ATVI", "ADBE", "AMD", "ALXN", "ALGN", "GOOGL", "GOOG",
             "AMZN", "AAL", "AMGN", "ADI", "AAPL", "AMAT", "ASML", "ADSK", "ADP", "BIDU", "BIIB", "BMRN", "BKNG", "AVGO", "CDNS", "CELG", "CERN", "CHTR",
             "CHKP", "CTAS", "CSCO", "CTXS", "CTSH", "CMCSA", "COST", "CSX", "CTRP", "DLTR", "EBAY",
             "EA", "EXPE", "FB", "FAST", "FISV", "FOX", "FOXA", "GILD", "HAS", "HSIC", "IDXX",
             "ILMN", "INCY", "INTC", "INTU", "ISRG", "JBHT", "JD", "KLAC", "LRCX", "LBTYA",
             "LBTYK", "LULU", "MAR", "MXIM", "MELI", "MCHP", "MU", "MSFT", "MDLZ", "MNST", "MYL",
             "NTAP", "NTES", "NFLX", "NVDA", "NXPI", "ORLY", "PCAR", "PAYX", "PYPL", "PEP", "QCOM",
             "REGN", "ROST", "SIRI", "SWKS", "SBUX", "SYMC", "SNPS", "TMUS", "TTWO", "TSLA", "TXN",
             "KHC", "ULTA", "UAL", "VRSN", "VRSK", "VRTX", "WBA", "WDAY", "WDC", "WLTW", "WYNN",
             "XEL", "XLNX"]


class StockDatabase:
    _engine = None
    _stock_limit = 101
    _mongodbUrl = "mongodb+srv://DBAdmin:GeniusFinanceDBAdmin@geniusfinance-p7ayy.gcp.mongodb.net/comp9900_genius_finance?retryWrites=true&w=majority"

    def __init__(self):
        self._engine = create_engine('sqlite:///./stockDatabase.db', echo=True)

    @property
    def engine(self):
        return self._engine

    def initCompanyTableWithCsv(self, fileName):
        Company.metadata.drop_all(self.engine)
        Company.metadata.create_all(self.engine)
        Session = sessionmaker(bind=self.engine)
        session = Session()

        companies = []
        with open(fileName) as csvFile:
            reader = csv.reader(csvFile)
            counter = 0
            for row in reader:
                company = Company(
                    symbol=row[0], fullname=row[1], lastScale=row[2],
                    marketCap=row[3],
                    adr_tso=row[4],
                    ipoyear=row[5],
                    sector=row[6],
                    industry=row[7], nasdaqLink=row[8])

                if (len(companies) < self._stock_limit):
                    companies.append(company)
                elif company.symbol in nasdaq100:
                    companies.append(company)
                else:
                    continue
        companies.pop(0)  # remove the tittle
        session.add_all(companies)
        session.commit()
        session.close()

    '''
        For each stock symbol store in the local database, this function will grep information displayed
        on the Nasdaq's official website
    '''

    def importKeyStockDataFromNasdaq(self):
        # open a new seesion
        mgclient = MongoClient(self._mongodbUrl)
        mgdb = mgclient.comp9900_genius_finance
        Session = sessionmaker(bind=self.engine)
        session = Session()

        companies = session.query(Company).all()
        for company in companies:
            if company.symbol in nasdaq100:
                print(company.symbol)
                nc = NasdaqReportCrawler("https://www.nasdaq.com/symbol/{}")
                summary = nc.getSummaryQuote(company.symbol)
                summary['Symbol'] = company.symbol
                summary['Name'] = company.fullname
                summary['IPO year'] = company.ipoyear
                summary['Industry'] = company.industry
                summary['Sector'] = company.sector
                summary['ADR TSO'] = company.adr_tso
                mgdb.stocks.insert_one(summary)
                time.sleep(2)

        # close the session
        session.close()

    def all_company_symbol(self):
        Session = sessionmaker(bind=self.engine)
        session = Session()
        companies = session.query(Company).all()
        session.close()
        return [c.symbol for c in companies]

    def get_mongo_db(self):
        mgclient = MongoClient(self._mongodbUrl)
        mgdb = mgclient.comp9900_genius_finance
        return mgdb

    def obtain_stock_information(self):
        alpha_token = "2F19GPWFNBRUP2ME"

        # intraday "https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol={}&interval=1min&apikey={}&outputsize=full".format(s, alpha_token)
        mgdb = sc.get_mongo_db()
        symbols = sc.all_company_symbol()
        symbols = symbols[34:]  # we fail at [33,56] last time,we
        for s in symbols:
            print("reading {} ...".format(s), end=" ")
            r = requests.get(
                "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol={}&apikey={}".format(s, alpha_token))
            price_dict = json.loads(r.text)
            price_dict.pop('Meta Data', None)
            updated_price_dict = cleanPriceDict(
                price_dict, "Time Series (Daily)")
            updated_price_dict["Symbol"] = s
            print(r.status_code)
            if r.status_code != 200:
                print("fatal")
                break
            # mgdb.stock_prices.insert_one(updated_price_dict)
            mgdb.stock_prices_daily.insert_one(updated_price_dict)
            time.sleep(20)

    def search_symbol_and_save(self, symbols):
        for symbol in symbols:
            print(symbol)
            nc = NasdaqReportCrawler("https://www.nasdaq.com/symbol/{}")
            summary = nc.getSummaryQuote(company.symbol)
            summary['Symbol'] = company.symbol
            summary['Name'] = company.fullname
            summary['IPO year'] = company.ipoyear
            summary['Industry'] = company.industry
            summary['Sector'] = company.sector
            summary['ADR TSO'] = company.adr_tso
            mgdb.stocks.insert_one(summary)
            time.sleep(2)


def cleanPriceDict(price_dict, time_key):
    series = price_dict[time_key]  # time_key looks like "Time Series (1min)"
    times = series.keys()
    result = dict()
    for time in times:
        result[time] = dict()
        result[time]["open"] = series[time]["1. open"]
        result[time]["high"] = series[time]["2. high"]
        result[time]["low"] = series[time]["3. low"]
        result[time]["close"] = series[time]["4. close"]
        result[time]["volume"] = series[time]["5. volume"]
    return result


if __name__ == "__main__":

    sc = StockDatabase()
    sc.obtain_stock_information()
