import csv
from ...models import Company


class StockInforCollector:
    _stockSymbols = []

    def __init__(self, fileName):
        with open(fileName) as csvFile:
            reader = csv.reader(csvFile)
            for row in reader:
                self._stockSymbols.append(row[0])
            self._stockSymbols.pop(0)  # skip the title

    @property
    def symbols(self):
        return self._stockSymbols


if __name__ == "__main__":
    sc = StockInforCollector("companylist.csv")
    print(sc.symbols)
    print(len(sc.symbols))
