import urllib.request
from bs4 import BeautifulSoup
import ssl


class NasdaqReportCrawler:
    url = ""
    gcontext = ssl.SSLContext()  # add the ssl context

    def __init__(self, url):
        self.url = url

    def extractSummaryQuote(self, html_page):
        soup = BeautifulSoup(html_page, 'html.parser')
        table = soup.find(
            'div', attrs={'class': 'row overview-results relativeP'})
        rows = table.findAll('div', attrs={'class': 'table-row'})

        result = dict()
        for row in rows:
            cells = row.findAll('div', attrs={'class': 'table-cell'})
            result[cells[0].text.strip().replace(u'\xa0', u' ').replace('.', '')
                   ] = cells[1].text.strip().replace(u'\xa0', u' ')
        return result

    '''
        This function is used to obtain the stock report
    '''

    def extractSummary(self, html_page):
        soup = BeautifulSoup(html_page, 'html.parser')
        table = soup.find('table', attrs={'class': 'marginB5px'})
        # table_body = table.find('tbody')
        table_body = table
        result = dict()
        for row in table_body.findAll("tr"):
            headers = row.findAll("th")
            # remove the irrelevant tags
            trash = row.find('span')
            trash.extract()

            datas = row.findAll("td")
            for header in headers:
                trash = header.find('span')
                if trash:
                    trash.extract()
                result[header.text.strip()] = datas[0].text.strip()
                datas.pop(0)
        return result

    def accessWebpage(self, symbol):
        url = self.url.format(symbol)
        print('accessing', url)
        req = urllib.request.Request(url, None, {})
        response = urllib.request.urlopen(req, context=self.gcontext)
        html_page = response.read().decode('utf-8')
        return html_page

    def getReportSummary(self, symbol):
        html_page = self.accessWebpage(symbol)
        result = self.extractSummary(html_page)
        return result

    def getSummaryQuote(self, symbol):
        html_page = self.accessWebpage(symbol)
        result = self.extractSummaryQuote(html_page)
        return result


if __name__ == "__main__":
    #nc = NasdaqReportCrawler("https://www.nasdaq.com/symbol/{}/stock-report")
    nc = NasdaqReportCrawler("https://www.nasdaq.com/symbol/{}")
    nc.getSummaryQuote("bcom")
