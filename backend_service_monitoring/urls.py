from flask_restful import Api
from app import app
from flask_cors import CORS
from monitor import Test,UserTasks,UserTask

api = Api(app)
CORS(app)
api.add_resource(Test, '/test')
api.add_resource(UserTasks, '/users/<string:username>/tasks')
api.add_resource(UserTask,'/users/<string:username>/tasks/<string:taskID>')
