from pymongo import MongoClient


class TasksDb:
    _mongodbUrl = "mongodb+srv://DBAdmin:GeniusFinanceDBAdmin@geniusfinance-p7ayy.gcp.mongodb.net/test?retryWrites=true&w=majority"
    _db = None

    def __init__(self):
        mgclient = MongoClient(self._mongodbUrl)
        self._db = mgclient.comp9900_genius_finance_monitoring

    def getAllTasksByUser (self, username):
        tasks = self._db.tasks.find({"username":username})
        result = dict()
        result['finished'] = []
        result['unfinished'] = []
        if tasks:
            for task in tasks:
                task.pop("_id", None)
                if task['isFinished'] :
                    result['finished'].append(task)
                else:
                    result['unfinished'].append(task)
        return result

    def getUnfinishedTasks (self):
        tasks = self._db.tasks.find({'isFinished': False})
        result = []
        if tasks:
            for task in tasks:
                task.pop("_id", None)
                result.append(task)
        return result

    def insertTask(self, task):
        result = self._db.tasks.insert(
            task)
    
    def removeTask(self,taskID):
        self._db.tasks.delete_one({'taskID':taskID})
    
    def finishTask(self,taskID):
        self._db.tasks.update_one({'taskID':taskID},{'$set': {'isFinished':True}})
