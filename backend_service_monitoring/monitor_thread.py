import threading
import time
from email_manager import EmailManager
from db import TasksDb
import requests

class MonitorThread (threading.Thread):
    def __init__(self, threadID, name):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.emailManager = EmailManager()
        self.tasksDb = TasksDb()
        self.quoteurl = "http://127.0.0.1:8100/stocks/{}/quote_summary"

    
    def run(self):
        while True:
            unfinishedTasks = self.tasksDb.getUnfinishedTasks()
            print(unfinishedTasks)
            if (len(unfinishedTasks)>0) :
                for task in unfinishedTasks:
                    print(task['symbol'])
                    symbol = task['symbol']
                    try:
                        r = requests.get(self.quoteurl.format(symbol))
                        print(r)
                        stock_summary = r.json()
                        print(stock_summary)
                        price = float(stock_summary['now'])
                        changePercentage = float(stock_summary['changePercentage'][0:-1])
                        if (self._meetCondition(task,price,changePercentage)):
                           
                            # send email in accordance with a suitable template
                            if task['condition']=='>=':
                                condition = "larger or equal"
                            else:
                                condition = "less or equal"
                            
                            self.emailManager = EmailManager()
                            if task['conditionType'] == 'price':
                                self.emailManager.sendForPriceChange(task['email'],
                                task['username'],task['symbol'],stock_summary['now'],condition,task['target'])
                                self.tasksDb.finishTask(task['taskID'])
                            else:
                                self.emailManager.sendForPercentageChange(task['email'],task['username'],
                                task['symbol'],stock_summary['changePercentage'],condition,task['target'])
                                self.tasksDb.finishTask(task['taskID'])
                            time.sleep(2) #send 1 email per second to avoid flooding the server
                    except:
                        print("?????")
                        continue
                    # get quote summary, try catch
                    # checkout price with condition 
                    # if condition met, then update task as finished
                    # send email
                    # else 
                    # go to the next
            time.sleep(10)

    def _meetCondition(self,task,price,changePercentage):
        if task['conditionType'] == 'percentage':
            return self._compareAccordingToCondition(task,changePercentage)
        else:
            #task['conditionType'] == 'price'
            return self._compareAccordingToCondition(task,price)

    def _compareAccordingToCondition(self,task,value): 
        if task['condition'] == '>=':
            if  value >= float(task['target']) :
                return True
        else:
            #task['condition'] == '<='
            if  value <= float(task['target']) :
                return True
        return False