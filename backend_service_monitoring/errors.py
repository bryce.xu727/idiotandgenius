errors = {
    "404": {"status code": 404, "message": "resources not found"},
    "400": {"status code": 400, "message": "bad request"}
}
