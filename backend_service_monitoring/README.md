# Monitoring Service

- [Monitoring Service](#monitoring-service)

## How to run this service
```
pip3 install -r requirement.txt
python3 run.py
```

## Tasks of a particular user

**Method** GET
**Base URL** http://127.0.0.1:9777
**PATH**: /users/{user}/tasks

For example, if we access this URL "http://127.0.0.1:9777/users/user/tasks", the following will be the output, if the `user` is valid and a status code of `200` is return.

```JSON
{
    "finished": [
        {
            "taskID": "af943e01-b",
            "symbol": "GOOG",
            "username": "user",
            "condition": ">=",
            "target": "1000",
            "conditionType": "price",
            "email": "wipe@mail-card.com",
            "isFinished": true
        },
        {
            "taskID": "57bc1517-1",
            "symbol": "GOOG",
            "username": "user",
            "condition": ">=",
            "target": "10",
            "conditionType": "price",
            "email": "wipe@mail-card.com",
            "isFinished": true
        }
    ],
    "unfinished": []
}
```

Please note that if there is no task in `finished` or `unfinised`， these two list will be empty. Even if the user is not exist.

```JSON
{
    "finished": [],
    "unfinished": []
}
```

## Add tasks for a particular user

**Method** POST
**Base URL** http://127.0.0.1:9777
**PATH**: /users/{user}/tasks

In order to create a task for this user, we need the following information in the payload

```Javascript
{
    "symbol": "GOOG",
    "condition": "<=",  // "<=" or ">=" stand for the less or equal to the target price
    "target": "10",     // it stand for price in term of dollar if `conditionType` is set to price
                        // it stand for percentage if `conditionType` is set to percentage
    "conditionType": "percentage",    // it can be either `price` or `percentage`
    "email": "yuzirihiru@mytmail.in"  // the email address which system will send email to if the target is met
}
```

On success, the folling message will be returned.

```JSON
{
    "status code": 200,
    "message": "add task success"
}
```

However, if user provide input other than the specification, it will return following message

```JSON
{
    "status code": 400,
    "message": "bad request"
}
```

## Delete task of a particular user

**Method** DELETE
**Base URL** http://127.0.0.1:9777
**PATH**: /users/{user}/tasks/{taskID}

Assuming task with taskID of 5fd137e7-5 exists, we can delete it on http://127.0.0.1:9777/users/admin/tasks/5fd137e7-5
On success the following message will be returned:

```JSON
{
    "status code": 200,
    "message": "delete task success"
}
```
