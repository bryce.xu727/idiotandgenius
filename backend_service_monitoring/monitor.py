from flask_restful import Resource,reqparse
from errors import errors
from email_manager import EmailManager
from db import TasksDb
from flask import current_app,request
import uuid

tasksDb = TasksDb()
parser = reqparse.RequestParser()
parser.add_argument('symbol')
parser.add_argument('condition')
parser.add_argument("target")
parser.add_argument("conditionType") 
parser.add_argument("email") 


class Test(Resource):
    def get(self):
        args = parser.parse_args()
        email = args.get('email')
        app = current_app._get_current_object()
        with app.app_context():
            em=EmailManager()
            em.send(email)
        return "success", 200

class UserTasks(Resource):
    def get(self,username):
        return tasksDb.getAllTasksByUser(username) ,200 #if user does not has any task, just return {finished:[],unfinished:[]}
    
    def post(self,username):
        args = parser.parse_args()
        
        symbol = args.get("symbol") 
        condition = args.get("condition") 
        target = args.get("target")
        conditionType =  args.get("conditionType") 
        email = args.get("email") 
        if self._isValidInput(symbol,condition,target,conditionType,email):
            task = {
            "taskID":str(uuid.uuid4())[:10],
            'symbol':symbol.upper(),
            'username':username,
            'condition':condition,
            'target':target,
            'conditionType':conditionType,
            'email': email,
            'isFinished':False
            }
            tasksDb.insertTask(task)
        else:
            return errors["400"],400
        return {"status code": 200, "message": "add task success"},200
        

        

    def _isValidInput(self,symbol,condition,target,conditionType,email):
        if not(symbol and  condition and target and conditionType and email):
            return False
        if not (condition in ['>=','<=']):
            return False
        if not (conditionType  in ['price','percentage']):
            return False
        return True

class UserTask(Resource):
    def delete(self,username,taskID):
        tasksDb.removeTask(taskID)
        return {"status code": 200, "message": "delete task success"},200




