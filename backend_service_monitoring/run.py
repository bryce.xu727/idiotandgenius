from app import app
import urls
from monitor_thread import MonitorThread

if __name__ == '__main__':
    threadID = "1"
    threadName = "monitor_1"
    monitorThread = MonitorThread(threadID, threadName)
    monitorThread.start()
    app.run(debug=True, port=9333,use_reloader=False)
    monitorThread.join()
