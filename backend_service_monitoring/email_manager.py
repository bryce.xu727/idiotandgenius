import multitasking
import smtplib
from email.message import EmailMessage

from jinja2 import Environment


priceChangeTemplate = """
        <!DOCTYPE html>
        <html>
        <body>
            <p>Dear {{username}}:<br><br>
            This email informs you that the stock <b>{{symbol}}</b> hits the target price you set which is ${{target}}.<br><br>
            The price per share of <b>{{symbol}}</b> now is ${{price}}.<br>
            </p>
            <br>
            <p>The best,<br>
                Genius Finance Team
            </p>

        </body>
        </html>
        """
precentageChangeTemplate = """
        <!DOCTYPE html>
        <html>
        <body>
            <p>Dear {{username}}:<br><br>
            This email informs you that the intraday change rate of stock <b>{{symbol}}</b> has hit the alarm setting which is ${{target}}%.<br><br>
            The intraday change rate of <b>{{symbol}}</b> is ${{change}}.<br>
            </p>
            <br>
             <p>The best,<br>
                Genius Finance Team
            </p>
        </body>
        </html>
        """


class EmailManager:
    def __init__(self):
        self.server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
        self.server.login('unsw.iandg@gmail.com', 'nybfex-myGko2-nibtef')
        self.templateEnvironment = Environment()

    def sendForPriceChange(self, recipient, username, symbol, price, condition, target):
        msg = self._define_message(recipient)
        msg['Subject'] = 'Alarm Information Update, stock: {}, price: {}'.format(
            symbol, target)
        msg.set_content(self.templateEnvironment.from_string(priceChangeTemplate).render(
            username=username, symbol=symbol, condition_op=condition, target=target, price=price
        ), subtype='html')
        self.server.send_message(msg)

    def sendForPercentageChange(self, recipient, username, symbol, change, condition, target):
        msg = self._define_message(recipient)
        msg['Subject'] = 'Alarm Information Update, stock: {}, change rate: {}'.format(
            symbol, target)
        msg.set_content(self.templateEnvironment.from_string(precentageChangeTemplate).render(
            username=username, symbol=symbol, condition_op=condition, target=target, change=change
        ), subtype='html')
        self.server.send_message(msg)

    def _define_message(self, recipient):
        msg = EmailMessage()
        msg['From'] = "Genius Finance <unsw.iandg@gmail.com>"
        msg['To'] = [recipient]
        msg.add_header('Content-Type', 'text/html', charset='utf-8')
        return msg

    def send(self, recipient):
        msg = self._define_message(recipient)
        self.server.send_message(msg)
