from timer_thread import now, startTime
import datetime


class StockPriceUtils:
    @classmethod
    def getCurrentPriceSerises(cls, price_dict):
        all_time_stamps = price_dict.keys()
        result = dict()
        if "Symbol" in all_time_stamps:
            result["Symbol"] = price_dict["Symbol"]
            price_dict.pop("Symbol")
            all_time_stamps = price_dict.keys()
        for time_stamp in all_time_stamps:
            try:
                time_object = datetime.datetime.strptime(
                    time_stamp, "%Y-%m-%d %H:%M:%S")
            except:
                break
            if time_object > now[0]:
                continue
            elif time_object <= now[0] and time_object >= startTime:
                result[time_stamp] = price_dict[time_stamp]
            else:
                break
        return result

    @classmethod
    def getCurrentPriceSummary(cls, price_dict):
        # obtain timestamps that we need
        all_time_stamps = price_dict.keys()
        result = dict()
        if "Symbol" in all_time_stamps:
            result["Symbol"] = price_dict["Symbol"]
            price_dict.pop("Symbol")
            all_time_stamps = price_dict.keys()

        # iterate through keys
        for time_stamp in all_time_stamps:
            dateWithoutTime = time_stamp.split()[0]
            todayWithoutTime = now[0].date()  # time should be default to zero
            try:
                time_object = datetime.datetime.strptime(
                    time_stamp, "%Y-%m-%d %H:%M:%S")
            except:
                break

            # set the latest price according to now
            if time_object > now[0]:
                pass
            elif time_object <= now[0] and time_object >= startTime:
                if not "now" in result.keys():
                    result["now"] = "{0:.2f}".format(float
                                                     (price_dict[time_stamp]["open"]))
            else:
                pass

            # find the open price
            if time_object > startTime:
                pass
            else:
                # time_object <= startTime:

                result["today"] = datetime.datetime.strftime(
                    todayWithoutTime, "%Y-%m-%d")

                # the first historical(nontoday) price we met
                result["start"] = "{0:.2f}".format(
                    float(
                        price_dict[time_stamp]["open"]))
                if "now" in result.keys():
                    result["change"] = str(round(float(
                        result["now"]) - float(result["start"]), 2))
                    result["changePercentage"] = "{0:.2f}%".format(round((float(
                        result["now"]) - float(result["start"])) / float(result["start"]), 4)*100)
                else:
                    result["change"] = "0"
                    result["changePercentage"] = "0%"

        # error handling
        if (not "now" in result.keys())and "start" in result.keys():
            result["today"] = datetime.datetime.strftime(
                todayWithoutTime, "%Y-%m-%d")
            result["now"] = result["start"]
            result["change"] = "0"
            result["changePercentage"] = "0%"

        if not "now" in result.keys() and not "start" in result.keys():
            # no historical price found
            result["today"] = datetime.datetime.strftime(
                todayWithoutTime, "%Y-%m-%d")
            # the first historical(non
            result["now"] = "N/A"
            # the first historical(nontoday) price we met
            result["start"] = "N/A"
            result["change"] = "N/A"

        return result
