from pymongo import MongoClient


class StockDb:
    _mongodbUrl = "mongodb+srv://DBAdmin:GeniusFinanceDBAdmin@geniusfinance-p7ayy.gcp.mongodb.net/test?retryWrites=true&w=majority"
    _db = None

    def __init__(self):
        mgclient = MongoClient(self._mongodbUrl)
        self._db = mgclient.comp9900_genius_finance

    def getQuoteBySymbol(self, symbol):
        result = self._db.stock_prices.find_one({'Symbol': symbol.upper()})
        if result:
            result.pop("_id", None)
        return result

    def getDailyQuoteBySymbol(self, symbol):
        result = self._db.stock_prices_daily.find_one(
            {'Symbol': symbol.upper()})
        if result:
            result.pop("_id", None)
        return result
