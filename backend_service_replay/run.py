from app import app
import urls
from timer_thread import now, TimeMachineThread
import time

if __name__ == '__main__':
    threadID = "1"
    threadName = "time_machine_1"
    accelerateFactor = 5  # the speed of time in the server is 5 times as the realworld
    timeMachine = TimeMachineThread(threadID, threadName, accelerateFactor)
    timeMachine.start()
    app.run(debug=True, port=8100)
    while True:
        print("> in main obtain now", now[0])
        time.sleep(10)
    timeMachine.join()
