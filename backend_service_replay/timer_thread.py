import datetime
import time
import threading

startTimeString = "2019-7-09 09:30:00"   # trading hour
closeTimeString = "2019-7-09 16:00:00"   # after market hour
now = [datetime.datetime.strptime(startTimeString, "%Y-%m-%d %H:%M:%S")]
startTime = datetime.datetime.strptime(
    startTimeString, "%Y-%m-%d %H:%M:%S")


class TimeMachineThread (threading.Thread):
    def __init__(self, threadID, name, accelerateFactor):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.accelerateFactor = accelerateFactor

    def run(self):
        print("Starting " + self.name,
              "with accelerate factor", self.accelerateFactor)
        updateTime(self.accelerateFactor)
        print("Exiting " + self.name)


def updateTime(accelerateFactor):
    global now
    closeTime = datetime.datetime.strptime(
        closeTimeString, "%Y-%m-%d %H:%M:%S")
    timedelta = datetime.timedelta(seconds=1)
    while now[0] < closeTime:
        now[0] = now[0] + timedelta
        time.sleep(1/accelerateFactor)
        if (now[0] >= closeTime):
            now[0] = datetime.datetime.strptime(
                startTimeString, "%Y-%m-%d %H:%M:%S")
