### :warning: Warning ! ! !

**Please note that this replay service is not considered as part of our portfolio management tool. It should be considered as an external service (a free version of Alphavantage)**

### Description

- This price replay service can replay historical data stored in the - database as the real-time data. This will beneficial for developing functionality that relies on real-time stock price, for example, price monitoring

> To run this service
```sh
$ python3 run.py 
```

### Sample Usage

#### Get pesudo real-time price

- get `http://localhost:8100/stocks/<symbol>/quote`
- result:

##### status 200

```
{
    "Symbol": "GOOG",
    "2019-07-09 13:21:00": {
        "open": "1121.1400",
        "high": "1121.1700",
        "low": "1121.1400",
        "close": "1121.1700",
        "volume": "563"
    }
}
```

##### status 404

if the symbol we are looking for is not stored in the database

```
{
    "status code": 404,
    "message": "resources not found"
}
```

#### Get pesudo daily price

##### status 200

- get `http://127.0.0.1:8100/stocks/<Symbol>/dailyQuote`
- result:

```
{
    "Symbol": "GOOG"
    "2019-07-09": {
        "open": "1111.8000",
        "high": "1128.0300",
        "low": "1107.1700",
        "close": "1124.8300",
        "volume": "1316324"
    },
    ...
    ...
}
```

##### status 404

```
{
    "status code": 404,
    "message": "resources not found"
}
```

#### Get stock analysis summary

##### status 200
- get `http://127.0.0.1:8100/stocks/<symbol>/quote_summary`
- result:
```json
{
    "Symbol": "AAPL",
    "now": "199.84",
    "today": "2019-11-25",
    "start": "201.41",
    "change": "-1.57",
    "changePercentage": "-0.78%"
}
```
##### status 404

if the symbol we are looking for is not stored in the database

```
{
    "status code": 404,
    "message": "resources not found"
}
```
