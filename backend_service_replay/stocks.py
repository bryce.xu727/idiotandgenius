from flask_restful import Resource
from db import StockDb
from errors import errors
from utils import StockPriceUtils

stockdb = StockDb()


class StockQuote(Resource):
    def get(self, symbol):
        result = stockdb.getQuoteBySymbol(symbol)
        if not result:
            return errors["404"], 404
        result = StockPriceUtils.getCurrentPriceSerises(result)
        return result, 200


class StockQuoteSummary(Resource):
    def get(self, symbol):
        result = stockdb.getQuoteBySymbol(symbol)
        if not result:
            return errors["404"], 404
        result = StockPriceUtils.getCurrentPriceSummary(result)
        return result, 200


class StockDailyQuote(Resource):
    def get(self, symbol):
        result = stockdb.getDailyQuoteBySymbol(symbol)
        if not result:
            return errors["404"], 404
        return result, 200
