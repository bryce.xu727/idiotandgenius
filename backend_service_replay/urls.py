from flask_restful import Api
from app import app
from stocks import StockQuote, StockQuoteSummary, StockDailyQuote
from flask_cors import CORS

api = Api(app)
CORS(app)
api.add_resource(StockQuote, '/stocks/<string:symbol>/quote')
api.add_resource(StockQuoteSummary, '/stocks/<string:symbol>/quote_summary')
api.add_resource(StockDailyQuote, '/stocks/<string:symbol>/dailyQuote')
