from flask import Flask
from flask_restful import Resource
import json, re
from flask_jwt_extended import jwt_required, get_jwt_identity
from error import *
from utils import *
import system.portfolio

class portfolio(Resource):
    @jwt_required
    def get(self, username, portfolio_id):
        portfolio = system.portfolio.Portfolio(portfolio_id)
        owner = portfolio.owner
        if owner:
            if validate_user(username) is False:
                return not_authorized_message, 401
            
            else:
                return json.dumps({
                        "portfolioId": portfolio_id,
                        "owner": portfolio.owner,
                        "portfolioName": portfolio.portfolio_name,
                        "stockWatchList": portfolio.get_stock_watch_list_api(portfolio_id),
                        "positionList": portfolio.get_position_list_api(portfolio_id),
                        "stockHoldingHistory": portfolio.get_stock_holding_list_api(portfolio_id),
                        "equityHistory": portfolio.get_equity_history_api(portfolio_id),
                    }), 200

        return bad_request_message, 404
    
    @jwt_required
    def put(self, username, portfolio_id):
        if validate_user(username) is False:
            return not_authorized_message, 401        
        args = parser.parse_args()
        operation = args.get('operation')
        new_name = args.get('newName')
        portfolio = system.portfolio.Portfolio(portfolio_id)
        if operation=="rename" and portfolio.rename(new_name):
            return {"message": "rename success"}, 201                      
        return bad_request_message, 400
    
    @jwt_required
    def delete(self, username, portfolio_id):  
        if validate_user(username) is False:
            return not_authorized_message, 401               
        portfolio_manager = system.portfolio_manager.PortfolioManager(username)
        result = portfolio_manager.delete_portfolio_api(portfolio_id)
        if result:
            return {"message": "delete portfolio success"}, 200
        return bad_request_message, 400

class portfolio_gain(Resource):
    @jwt_required
    def get(self, username, portfolio_id):
        if validate_user(username) is False:
            return not_authorized_message, 401        
        portfolio = system.portfolio.Portfolio(portfolio_id)
        portfolio.initialize()
        daily_gain, daily_gain_percentage = portfolio.get_daily_gain()
        total_gain, total_gain_percentage = portfolio.get_total_gain()
        return {
            "marketValue": round(portfolio.get_market_value(),2),
            "dailyGain": round(daily_gain,2),
            "dailyGainPercentage": round(daily_gain_percentage,2),
            "totalGain": total_gain,
            "totalGainPercentage": round(total_gain_percentage,2)
        }, 200

class portfolio_volatility(Resource):
    @jwt_required
    def get(self, username, portfolio_id):
        if validate_user(username) is False:
            return not_authorized_message, 401        
        portfolio = system.portfolio.Portfolio(portfolio_id)
        portfolio.initialize()
        volality_dict = portfolio.get_portfolio_volatility()
        result = []
        for key, value, in volality_dict.items():                                 
            volality = {
                "symbol": key,
                "volality": round(value,2)
            }
            result.append(volality)
        return result, 200
class portfolio_stock_watch_list(Resource):
    @jwt_required
    def post(self, username, portfolio_id):
        if validate_user(username) is False:
            return not_authorized_message, 401
        portfolio = system.portfolio.Portfolio(portfolio_id)
        args = parser.parse_args()
        symbol = args.get("symbol")
        if portfolio.add_stock_api(symbol):
            return {"message": "add stock to watchList success"}, 200
        else:
            bad_request_message, 400    

class portfolio_stock(Resource):
    @jwt_required
    def delete(self, username, portfolio_id, symbol):
        if validate_user(username) is False:
            return not_authorized_message, 401
        portfolio = system.portfolio.Portfolio(portfolio_id)
        result = portfolio.find_position_by_symbol(symbol)
        if result is None:
            portfolio.delete_stock_api(symbol)
            return {"message": "delete stock from watchList success"}, 200
        if len(result)==0:
            if symbol not in portfolio.get_stock_watch_list_api(portfolio_id):
                return stock_not_found_message, 404
            else:
                portfolio.delete_stock_api(symbol)
                return {"message": "delete stock from watchList success"}, 200
        else:            
            return {"message": "This stock still in position list. Please remove all relative position first"}, 400

class portfolio_position_list(Resource):
    @jwt_required
    def post(self, username, portfolio_id):
        if validate_user(username) is False:
            return not_authorized_message, 401

        args = parser.parse_args()
        symbol = args.get("symbol")
        purchase_price = float(args.get('purchasePrice'))
        position_size = float(args.get('positionSize'))
        portfolio = system.portfolio.Portfolio(portfolio_id)
        if position_size < 0 and list(portfolio.get_stock_holding_by_symbol_api(symbol).values())[-1] < abs(position_size):
            return {"message": "selling size is larger than the exiting one"}, 400
        portfolio.initialize()       
        if portfolio.add_position(symbol, (purchase_price), (position_size)):
            return {"message": "add position success"}, 200           

        return {"message": "please add the stock in the watch list first"}, 400

class portfolio_curve_data(Resource):
    @jwt_required
    def get(self, username, portfolio_id):
        if validate_user(username) is False:
            return not_authorized_message, 401

        portfolio = system.portfolio.Portfolio(portfolio_id)
        portfolio.initialize()  
        curve_data_df = portfolio.get_portfolio_curve_data()
        column_list = list(curve_data_df.columns.values)
        date_list = curve_data_df['Date'].tolist()
        market_value_list = dataframe_to_list(curve_data_df, column_list[1])
        equity_list = dataframe_to_list(curve_data_df, column_list[2])
        gain_list = dataframe_to_list(curve_data_df, column_list[3])
        yield_list = dataframe_to_list(curve_data_df,column_list[4])
        result = []
        for i in range(len(market_value_list)):
            curve_data = {
                "Date": date_list[i].strftime('%Y-%m-%d'),
                "MarketValue": market_value_list[i],
                "Equity": equity_list[i],
                "Gain": gain_list[i],
                "Yield": round(yield_list[i],2)
            }
            result.append(curve_data)
        return result, 200           

class portfolio_correlation(Resource):
    @jwt_required
    def get(self, username, portfolio_id):
        if validate_user(username) is False:
            return not_authorized_message, 401
        
        portfolio = system.portfolio.Portfolio(portfolio_id)
        portfolio.initialize() 
        correlation_df = portfolio.get_correlation_within_portfolio()
        correlation_dict = correlation_df.to_dict()
        serie_list = []
        for key, value in zip(correlation_dict.keys(), correlation_dict.values()):
            x_list = list(value.keys())
            y_list = list(value.values())
            data_list = []
            for i in range(len(x_list)):                
                data = {
                    "x": x_list[i],
                    "y": round(y_list[i],2)
                }
                data_list.append(data)
            
            serie = {
                "name": key,
                "data": data_list
            }
            serie_list.append(serie)
        
        return serie_list, 200   

class portfolio_position_gain(Resource):
    @jwt_required
    def get(self, username, portfolio_id):
        if validate_user(username) is False:
            return not_authorized_message, 401
        
        portfolio = system.portfolio.Portfolio(portfolio_id)
        portfolio.initialize()             
        stocks_summary_dict = portfolio.get_stocks_summary()
        if stocks_summary_dict:
            return stocks_summary_dict, 200
        
        return {"message": "information is not available"}, 400

class portfolio_equity_allocation(Resource):
    @jwt_required
    def get(self, username, portfolio_id):
        if validate_user(username) is False:
            return not_authorized_message, 401
        
        portfolio = system.portfolio.Portfolio(portfolio_id)
        portfolio.initialize() 
        equity_allocation_dict = portfolio.get_equity_allocation_pie_chart_data()
        label_list = []
        series_list = []
        for key, value in equity_allocation_dict.items():
            label_list.append(key)
            series_list.append(value)
        
        return {"labels": label_list, "series": series_list}, 200