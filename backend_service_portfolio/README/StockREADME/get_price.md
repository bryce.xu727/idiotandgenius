# Get stock price data
Sample url: http://127.0.0.1:9222/YI/pricedata
### Method : GET
## Success Responses
### code 200
```json
{
    "daily price": {
        "2019-07-08": {
            "1. open": "5.3900",
            "2. high": "5.3900",
            "3. low": "5.0900",
            "4. close": "5.1600",
            "5. volume": "20560"
        },
        "2019-07-05": {
            "1. open": "5.4700",
            "2. high": "5.4700",
            "3. low": "5.3600",
            "4. close": "5.3600",
            "5. volume": "27396"
        },
    ...
```
## Fail 
### code 404
```json
{
    "message": "The stock is not exist"
}
```