# Get stock company data
Sample url: http://127.0.0.1:9222/YI/stockcompanydata
### Method : GET
## Success Responses
### code 200
```json
{
    "companyName": "111, Inc.",
    "IPO year": "2018",
    "industry": "Medical/Nursing Services",
    "sector": "Health Care",
    "marketCap": "540,580,356",
    "annualize dividend": "N/A",
    "ex-dividend data": "N/A",
    "dividend payment date": "N/A",
    "current yield": "0 %"
}
```
## Fail 
### code 404
```json
{
    "message": "The stock is not exist"
}
```