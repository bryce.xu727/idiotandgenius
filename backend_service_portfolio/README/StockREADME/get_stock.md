# Get stock basic data
Sample url: http://127.0.0.1:9222/YI/stockdata
### Method : GET
## Success Responses
### code 200
```json
{
    "bestBid": "NA",
    "bestAsk": "NA",
    "todayHigh": "$ 6.85 ",
    "todayLow": " $ 6.51",
    "shareVolume": "21,980",
    "fiftyDayAvgDailyVolumn": "27,332",
    "previousClose": "$ 6.62",
    "fiftyTwoWeekHigh": "$ 16.83 ",
    "fiftyTwoWeekLow": " $ 5.60"
}
```
## Fail 
### code 404
```json
{
    "message": "The stock is not exist"
}
```