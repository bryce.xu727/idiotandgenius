# Get User Equity Allocation
Sample url: http://localhost:9222/oliver1/equityallocation
## Method : GET
## Success Responses 200
```json
{
    "labels": [
        "Health Care",
        "Technology"
    ],
    "series": [
        530,
        46080
    ]
}
```