# Get portfolio id and name in pair
http://localhost:9222/users/oliver1/symbols/AAPL
## Method : GET
## Success Responses 200
```json
[
    {
        "portfolioId": "efmnxdugfu",
        "portfolioName": "oliver1",
        "isInportfolio": true
    },
    {
        "portfolioId": "ybkzvozsrl",
        "portfolioName": "oliver1",
        "isInportfolio": false
    }
]
```
## Fail 400
```json
{
    "message": "There is no portoflio for the user"
}
```
