# Get User Volatility
Sample url: http://127.0.0.1:9222/jason2/volatilitytable
## Method : GET
## Success Responses 200
```json
[
    {
        "symbol": "AAPL",
        "volality": 14.22
    },
    {
        "symbol": "GOOG",
        "volality": 371.75
    },
    {
        "symbol": "oliver1",
        "volality": 192.99
    }
]
```