# Create User Portfolio
Sample url: http://127.0.0.1:9222/jason2/portfolio
## Method : POST
## body
```json
{
    "portfolioName": "[pass the name of the portfolio]"
}
```

## Success Responses 201
```json
{
    "message": "add portfolio success"
}
```
## Fail 404
```json
{
    "message": "incorrect username"
}
```