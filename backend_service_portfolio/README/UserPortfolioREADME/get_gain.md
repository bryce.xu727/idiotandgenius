# Get User Gain
Sample url: http://127.0.0.1:9222/jason2/gain
## Method : GET
## Success Responses 200
```json
{
    "marketValue": 305972,
    "dailyGain": 0,
    "dailyGainPercentage": 0,
    "totalGain": 286662,
    "totalGainPercentage": 14.845261522527188
}
```