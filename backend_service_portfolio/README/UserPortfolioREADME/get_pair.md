# Get portfolio id and name in pair
Sample url: http://127.0.0.1:9222/jason2/pair
## Method : GET
## Success Responses 200
```json
[
    {
        "portfolioName": "oliver1",
        "portfolioId": "efmnxdugfu"
    },
    {
        "portfolioName": "oliver1",
        "portfolioId": "ybkzvozsrl"
    },
    {
        "portfolioName": "oliver1",
        "portfolioId": "yhcbxkvmuw"
    }
]
```
## Fail 400
```json
{
    "message": "incorrect username"
}
```