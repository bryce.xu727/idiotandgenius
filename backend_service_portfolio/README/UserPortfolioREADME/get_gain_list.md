# Get User Gain to list
Sample url: http://127.0.0.1:9222/jason2/gainlist
## Method : GET
## Success Responses 200
```json
[
    {
        "portfolioId": "efmnxdugfu",
        "portfolioName": "oliver1",
        "marketValue": 81006,
        "dailyGain": 0,
        "dailyGainPercentage": 0,
        "totalGain": 69956,
        "totalGainPercentage": 6.330859728506788
    },
    {
        "portfolioId": "ybkzvozsrl",
        "portfolioName": "oliver1",
        "marketValue": 0,
        "dailyGain": 0,
        "dailyGainPercentage": 0,
        "totalGain": 0,
        "totalGainPercentage": 0
    },
    {
        "portfolioId": "yhcbxkvmuw",
        "portfolioName": "oliver1",
        "marketValue": 795642,
        "dailyGain": 0,
        "dailyGainPercentage": 0,
        "totalGain": 760082,
        "totalGainPercentage": 21.37463442069741
    }
]
```