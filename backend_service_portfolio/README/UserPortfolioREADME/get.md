# Get User Portfolio
Sample url: http://127.0.0.1:9222/jason2/portfolio
## Method : GET
## Success Responses 200
```json
{\"username\": \"oliver1\", \"portfolioList\": [\"efmnxdugfu\", \"ybkzvozsrl\", \"yhcbxkvmuw\"], \"operationList\": [\"create portfolio efmnxdugfu 2019-07-17 10:25:10.281491\", \"create portfolio ybkzvozsrl 2019-07-17 10:27:31.630249\", \"create portfolio yhcbxkvmuw 2019-07-17 10:48:58.526005\"]}
```
## Fail 404
```json
"{message: incorrect username}"
```