# Get stock holding by symbol
Sample url: http://localhost:9222/oliver1/portfolio/yhcbxkvmuw/stockholding/AAPL
### Method : GET
## Success Responses
### code 200
```json
{
    "portfolio": "yhcbxkvmuw",
    "symbol": "AAPL",
    "size": 500,
    "time": "2019-07-25"
}
```