# Add position to portfolio
Sample url: http://127.0.0.1:9222/jason2/portfolio/<string: portfolioid>/position
(note: the portfolio id is gettting from the get response of user portfolio)
### Method : POST
## body
```json
	"symbol": "YI",
    "positionSize": 100,
    "purchasePrice": 31.3,
```
## Success Responses
### code 200
```json
{"message": "add position success"}
```
## Fail 
### code 400
```json
{
    "message": "incorrect username or portfolio id"
}
```
> If the selling position size is larger than the current holding, it will return this message
```json
{
    "message": "selling size is larger than the exiting one"
}
```