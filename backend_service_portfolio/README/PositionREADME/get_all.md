# Get All Position
Sample url: http://127.0.0.1:9222/jason2/portfolio/<string:username>/portfolio/<string:portfolio_id>/position
(note: the portfolio id and position is gettting from the get response of user portfolio and response from portfolio)
### Method : GET
## Success Responses
### code 200
```json
[
    {
        "positionId": "snarqwvqap",
        "stockSymbol": "AAPL",
        "purchaseTime": "2019-07-22",
        "purchasePrice": 31.3,
        "numberOfShare": 200
    },
    {
        "positionId": "gqiejpufno",
        "stockSymbol": "AAPL",
        "purchaseTime": "2019-07-22",
        "purchasePrice": 31.3,
        "numberOfShare": 200
    }
]
```
## Fail 
### code 400
```json
{
    "message": "position not found"
}
```