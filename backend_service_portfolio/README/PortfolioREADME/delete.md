# Delete Portfolio
Sample url: http://127.0.0.1:9222/jason2/portfolio/<string: portfolioid>
(note: the portfolio id is gettting from the get response of user portfolio)
### Method : DELETE
## Success Responses
### code 200
```json
{
    "message": "delete portfolio success"
}
```
## Fail 
### code 400
```json
{
    "message": "incorrect username or portfolio id"
}
```