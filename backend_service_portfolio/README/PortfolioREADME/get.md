# Get Portfolio Detail
Sample url: http://127.0.0.1:9222/jason2/portfolio/<string: portfolioid>
(note: the portfolio id is gettting from the get response of user portfolio)
### Method : GET
## Success Responses
### code 200
```json
{\"portfolioId\": \"yhcbxkvmuw\", \"owner\": \"oliver1\", \"portfolioName\": \"oliver1\", \"stockWatchList\": [\"AAPL\", \"GOOG\"], \"positionList\": [\"dilxtycjex\", \"glwhvimtip\", \"wiyaihbvfx\", \"snarqwvqap\", \"gqiejpufno\", \"ysmdbirmyh\", \"dxvkcckssk\", \"wkvvtshgqh\", \"inxkzjbfbh\", \"kfgrcssnul\"], \"stockHoldingHistory\": [\"AAPL\", \"GOOG\"], \"equityHistory\": {\"2019-07-23\": 60600.0}, \"isPublic\": null}
```
## Fail 
### code 404
```json
{
    "message": "incorrect username or portfolio id"
}
```