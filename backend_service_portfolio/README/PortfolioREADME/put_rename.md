# Rename Portfolio Detail
Sample url: http://127.0.0.1:9222/jason2/portfolio/<string: portfolioid>
(note: the portfolio id is gettting from the get response of user portfolio)
### Method : PUT
## body
```json
	"operation": "rename",
	"newName": "[enter the new name of the portfolio]",
```
## Success Responses
### code 201
```json
{
	"message": "rename success"
}
```
## Fail 
### code 400
```json
{
    "message": "incorrect username or portfolio id"
}
```