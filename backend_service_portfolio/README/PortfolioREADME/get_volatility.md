# Get votatility Detail
Sample url: http://127.0.0.1:9222/jason2/portfolio/<string: portfolioid>/volatility
(note: the portfolio id is gettting from the get response of user portfolio)
### Method : GET
## Success Responses
### code 200
```json
[
    {
        "symbol": "AAPL",
        "volality": 14.22
    },
    {
        "symbol": "GOOG",
        "volality": 371.75
    },
    {
        "symbol": "oliver1",
        "volality": 192.99
    }
]
```