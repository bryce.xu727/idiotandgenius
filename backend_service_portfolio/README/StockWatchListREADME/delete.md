# Delete stock from stock watch list
Sample url: http://127.0.0.1:9222/jason2/portfolio/<string:portfolioid>/stockwatchlist/<string:symbol>
(note: the portfolio id is gettting from the get response of user portfolio)
### Method : DELETE
## Success Responses
### code 200
```json
{
    "message": "delete stock from stock watch list success"
}
```
## Fail 
### code 400
If there are some related positions in the position list, it will throw the error
```json
{
    "message": "This stock still in position list. Please remove all relative position first"
}
```
Otherwise
```json
{
    "message": "The stock is not exist"
}
```

