# Add Stock to Watch List
Sample url: http://127.0.0.1:9222/jason2/portfolio/<string: portfolioid>/stockwatchlist
(note: the portfolio id is gettting from the get response of user portfolio)
### Method : POST
## body
```json
	"symbol": "YI",
```
## Success Responses
### code 200
```json
{
    "message": "add stock to watch list success"
}
```
## Fail 
### code 400
```json
{
    "message": "incorrect username or portfolio id"
}
```