# Get Portfolio Detail
Sample url: http://127.0.0.1:9222/jason2/portfolio/<string: portfolioid>/gain
(note: the portfolio id is gettting from the get response of user portfolio)
### Method : GET
## Success Responses
### code 200
```json
{
    "marketValue": 1510788,
    "dailyGain": 11152,
    "dailyGainPercentage": 0.18,
    "totalGain": 1450188,
    "totalGainPercentage": 23.93
}
```
