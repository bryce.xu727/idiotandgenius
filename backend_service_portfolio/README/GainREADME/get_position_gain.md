# Get Position Gain
Sample url: http://127.0.0.1:9222/jason2/portfolio/<string: portfolioid>/positiongain
(note: the portfolio id is gettting from the get response of user portfolio)
### Method : GET
## Success Responses
### code 200
```json
{
    "AAPL": {
        "symbol": "AAPL",
        "intraday gain": 0,
        "shares": 800,
        "market value": 160992,
        "equity": 25040,
        "daily gain": 0,
        "daily gain (percentage)": 0,
        "total gain": 135952,
        "total gain (percentage)": 5.43,
        "number of lots": 4,
        "positions": [
            {
                "position id": "dilxtycjex",
                "date": "2019-07-17",
                "shares": 200,
                "daily gain": 0,
                "daily gain (percentage)": 0,
                "total gain": 33988,
                "total gain (percentage)": 5.43,
                "cost base": 6260
            },
            {
                "position id": "snarqwvqap",
                "date": "2019-07-22",
                "shares": 200,
                "daily gain": 0,
                "daily gain (percentage)": 0,
                "total gain": 33988,
                "total gain (percentage)": 5.43,
                "cost base": 6260
            },
            {
                "position id": "gqiejpufno",
                "date": "2019-07-22",
                "shares": 200,
                "daily gain": 0,
                "daily gain (percentage)": 0,
                "total gain": 33988,
                "total gain (percentage)": 5.43,
                "cost base": 6260
            },
            {
                "position id": "kfgrcssnul",
                "date": "2019-07-23",
                "shares": 200,
                "daily gain": 0,
                "daily gain (percentage)": 0,
                "total gain": 33988,
                "total gain (percentage)": 5.43,
                "cost base": 6260
            }
        ]
    },
    "GOOG": {
        "symbol": "GOOG",
        "intraday gain": 0,
        "shares": 1200,
        "market value": 1349796,
        "equity": 35560,
        "daily gain": 0,
        "daily gain (percentage)": 0,
        "total gain": 1314236,
        "total gain (percentage)": 36.96,
        "number of lots": 6,
        "positions": [
            {
                "position id": "glwhvimtip",
                "date": "2019-07-17",
                "shares": 200,
                "daily gain": 0,
                "daily gain (percentage)": 0,
                "total gain": 218706,
                "total gain (percentage)": 34.94,
                "cost base": 6260
            },
            {
                "position id": "wiyaihbvfx",
                "date": "2019-07-22",
                "shares": 200,
                "daily gain": 0,
                "daily gain (percentage)": 0,
                "total gain": 218706,
                "total gain (percentage)": 34.94,
                "cost base": 6260
            },
            {
                "position id": "ysmdbirmyh",
                "date": "2019-07-22",
                "shares": 200,
                "daily gain": 0,
                "daily gain (percentage)": 0,
                "total gain": 220706,
                "total gain (percentage)": 51.81,
                "cost base": 4260
            },
            {
                "position id": "dxvkcckssk",
                "date": "2019-07-23",
                "shares": 200,
                "daily gain": 0,
                "daily gain (percentage)": 0,
                "total gain": 218706,
                "total gain (percentage)": 34.94,
                "cost base": 6260
            },
            {
                "position id": "wkvvtshgqh",
                "date": "2019-07-23",
                "shares": 200,
                "daily gain": 0,
                "daily gain (percentage)": 0,
                "total gain": 218706,
                "total gain (percentage)": 34.94,
                "cost base": 6260
            },
            {
                "position id": "inxkzjbfbh",
                "date": "2019-07-23",
                "shares": 200,
                "daily gain": 0,
                "daily gain (percentage)": 0,
                "total gain": 218706,
                "total gain (percentage)": 34.94,
                "cost base": 6260
            }
        ]
    }
}
```