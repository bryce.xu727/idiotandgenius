from flask_restful import reqparse
from flask_jwt_extended import get_jwt_identity
import time, datetime

def validate_user(username):
    """A function to validate the token
    
    Arguments:
        username {[string]} -- [username]
    
    Returns:
        [Boolean] -- [valid or invalid]
    """
    token_username = get_jwt_identity().split(":")[1].split(",")[0].replace('"','').replace("}","").strip()
    if token_username == username:
        return True
    else:
        return False

# add arguments
parser = reqparse.RequestParser()
parser.add_argument('portfolioName')
parser.add_argument('newName')
parser.add_argument('symbol')
parser.add_argument('operation')
parser.add_argument('positionSize')
parser.add_argument('purchasePrice')

def dataframe_to_list(dataframe, column):
    return dataframe[column].values.tolist()

def extract_summary(dataframe, column_list):
    result = []
    for num in range(1, len(column_list)-1):
        for i in range((dataframe.shape[0])):
            value_list = dataframe_to_list(dataframe, column_list[num]) 
            portfolio = {
                "portfolioName": column_list[num],
                "Value": value_list[i],
            }       
            result.append(portfolio)
    
    return result

def get_trade_day():
    return "2019-11-25"

def extract_chart_data(chart_dict):
    date_list = chart_dict['Date']
    open_list = chart_dict['Open']
    close_list = chart_dict['Close']
    high_list = chart_dict['High']
    low_list = chart_dict['Low']

    result = []
    for i in range(len(date_list)):
        date = time.mktime(datetime.datetime.strptime(date_list[i], "%Y-%m-%d").timetuple())
        result.append({"x": date, "y": [open_list[i], high_list[i],low_list[i],close_list[i]]}) 

    return result   

def extract_chart_date(date_string):
    return time.mktime(datetime.datetime.strptime(date_string, "%Y-%m-%d").timetuple())