from flask import Flask
from flask_restful import Resource, request
import json
from flask_jwt_extended import JWTManager, jwt_required, get_jwt_identity
from portfolio_resource import *
from utils import *
import system.portfolio_manager

class user_portfolio(Resource):
    @jwt_required
    def get(self,username):
        if validate_user(username) is False:
            return not_authorized_message, 401    
        portfolio_manager = system.portfolio_manager.PortfolioManager(username)        
        if portfolio_manager:
            return json.dumps({
                "username": username,
                "portfolioList": portfolio_manager.get_portfolio_list_api(username),
                "operationList": portfolio_manager.get_operation_history_api(username)
            }), 200
        
        return {"message": "incorrect username"}, 400

    @jwt_required
    def post(self, username):
        if validate_user(username) is False:
            return not_authorized_message, 401  
        args = parser.parse_args()
        portfolio_name = args.get('portfolioName')
        portfolio_manager = system.portfolio_manager.PortfolioManager(username)
        if portfolio_manager.create_portfolio(portfolio_name):
            return {"message": "add portfolio success"}, 200
        return {"message": "incorrect username or server error"}, 400

class user_equity_allocation(Resource):
    @jwt_required
    def get(self,username):
        if validate_user(username) is False:
            return not_authorized_message, 401    
        portfolio_manager = system.portfolioManager.PortfolioManager(username)
        portfolio_manager.initialize()
        equity_allocation_dict = portfolio_manager.get_equity_allocation_pie_chart_data()
        label_list = []
        series_list = []        
        for key, value in equity_allocation_dict.items():
            label_list.append(key)
            series_list.append(value)
        
        return {"labels": label_list, "series": series_list}, 200
        
class user_gain(Resource):
    @jwt_required
    def get(self, username):
        if validate_user(username) is False:
            return not_authorized_message, 401        
        portfolio_manager = system.portfolio_manager.PortfolioManager(username)
        portfolio_manager.initialize()
        market_value = portfolio_manager.get_market_value()
        daily_gain, daily_gain_percentage = portfolio_manager.get_daily_gain()
        total_gain, total_gain_percentage = portfolio_manager.get_total_gain()
        return {
            "marketValue": market_value,
            "dailyGain": daily_gain,
            "dailyGainPercentage": daily_gain_percentage,
            "totalGain": total_gain,
            "totalGainPercentage": total_gain_percentage
        }, 200

class user_gain_tolist(Resource):
    @jwt_required
    def get(self, username):
        if validate_user(username) is False:
            return not_authorized_message, 401        
        portfolio_manager = system.portfolio_manager.PortfolioManager(username)
        portfolio_list = portfolio_manager.get_portfolio_list_api(username)               
        result = []
        if portfolio_list is None:
            return result, 200
        for i in range (len(portfolio_list)):
            portfolio = system.portfolio.Portfolio(portfolio_list[i])
            portfolio.initialize()
            daily_gain, daily_gain_percentage = portfolio.get_daily_gain()
            total_gain, total_gain_percentage = portfolio.get_total_gain()
            result.append({
                "portfolioId": portfolio_list[i],
                "portfolioName": portfolio.portfolio_name,
                "numberofStocks": len(portfolio.stock_watch_list),
                "marketValue": round(portfolio.get_market_value(),2),
                "dailyGain": round(daily_gain,2),
                "dailyGainPercentage": round(daily_gain_percentage,2),
                "totalGain": total_gain,
                "totalGainPercentage": round(total_gain_percentage,2)
            })  

        if result:
            return result, 200

        return bad_request_message, 400                      

class user_volatility_table(Resource):
    @jwt_required
    def get(self, username):
        if validate_user(username) is False:
            return not_authorized_message, 401 
        portfolio_manager = system.portfolio_manager.PortfolioManager(username)
        portfolio_manager.initialize()
        volality_dict = portfolio_manager.get_volatility_table_data()
        result = []
        for key, value, in volality_dict.items():
            key_list = list(value.keys())
            value_list = list(value.values())                                 
            for i in range(len(key_list)):
                volality = {
                    "symbol": key_list[i],
                    "volality": round(value_list[i],2)
                }
                result.append(volality)

        return result, 200    

class user_correlation_table(Resource):
    @jwt_required
    def get(self, username):
        if validate_user(username) is False:
            return not_authorized_message, 401 
        portfolio_manager = system.portfolio_manager.PortfolioManager(username)
        portfolio_manager.initialize()
        correlation_dict = portfolio_manager.get_correlation_table_data()
        correlation_list = list(correlation_dict.values())
        correlation_dict = correlation_list[0].to_dict()
        serie_list = []
        for key, value in zip(correlation_dict.keys(), correlation_dict.values()):
            x_list = list(value.keys())
            y_list = list(value.values())
            data_list = []
            for i in range(len(x_list)):                
                data = {
                    "x": x_list[i],
                    "y": round(y_list[i],2)
                }
                data_list.append(data)
            
            serie = {
                "name": key,
                "data": data_list
            }
            serie_list.append(serie)
        return serie_list, 200

class user_curve_data(Resource):
    @jwt_required
    def get(self, username):
        if validate_user(username) is False:
            return not_authorized_message, 401 
        portfolio_manager = system.portfolio_manager.PortfolioManager(username)
        portfolio_manager.initialize()
        equity_df, market_value_df, gain_df, yield_df = portfolio_manager.get_curve_data()

        equity_column_list = list(equity_df.columns.values)
        equity_list = extract_summary(equity_df, equity_column_list)
        total_equity_list = dataframe_to_list(equity_df, equity_column_list[-1])

        market_value_column_list = list(market_value_df.columns.values)
        market_value_list = extract_summary(market_value_df, market_value_column_list)
        total_market_value_list = dataframe_to_list(market_value_df, market_value_column_list[-1])

        gain_column_list = list(gain_df.columns.values)
        gain_list = extract_summary(gain_df, gain_column_list)
        total_gain_list = dataframe_to_list(gain_df, gain_column_list[-1])

        yield_column_list = list(yield_df.columns.values)
        yield_list = extract_summary(yield_df, yield_column_list)
        total_yield_list = dataframe_to_list(yield_df, yield_column_list[-1])

        result = []
        for i in range(len(total_equity_list)):
            equity = {
                "equityList": equity_list,
                "totalEquity": total_equity_list[i]
            }
            marker_value = {
                "marketValueList": market_value_list,
                "totalMarketValue": total_market_value_list[i]
            }
            gain = {
                "gainList": gain_list,
                "totalGain": total_gain_list[i]
            }
            user_yield = {
                "yieldList": yield_list,
                "totalYield": total_yield_list[i]
            }
            result.append(equity)
            result.append(marker_value)
            result.append(gain)
            result.append(user_yield)
        
        return result, 200

class pair_name_id(Resource):
    @jwt_required
    def get(self,username):
        if validate_user(username) is False:
            return not_authorized_message, 401    
        portfolio_manager = system.portfolio_manager.PortfolioManager(username)
        
        if portfolio_manager:
            portfolio_list = portfolio_manager.get_portfolio_list_api(username)
            result = []
            if portfolio_list is not None:
                for i in range(len(portfolio_list)):
                    portfolio_id = portfolio_list[i]
                    result.append(({
                        "portfolioName": portfolio_manager.get_portfolio_name_api(portfolio_id),
                        "portfolioId": portfolio_id
                    }))

                return result, 200        

        return {"message": "incorrect username"}, 400

class add_stock_precheck(Resource):
    @jwt_required
    def get(self,username, symbol):
        if validate_user(username) is False:
            return not_authorized_message, 401    
        portfolio_manager = system.portfolio_manager.PortfolioManager(username)
        
        if portfolio_manager:    
            portfolio_manager.initialize()
            portfolio_list = portfolio_manager.portfolio_list
            if portfolio_list:
                result_list = []
                for i in range(len(portfolio_list)):
                    portfolio = portfolio_list[i]
                    stock_watch_list = portfolio.get_stock_watch_list_api(portfolio.portfolio_id)
                    if stock_watch_list is None:
                        is_in_portfolio = False
                    elif symbol in stock_watch_list:
                        is_in_portfolio = True
                    else:
                        is_in_portfolio = False
                    result = {
                        "portfolioId": portfolio.portfolio_id,
                        "portfolioName": portfolio.portfolio_name,
                        "isInportfolio": is_in_portfolio
                    }
                    result_list.append(result)

                return result_list, 200
            else:
                return {"message": "There is no portoflio for the user"}, 400