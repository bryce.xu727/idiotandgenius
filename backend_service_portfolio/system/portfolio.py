from .stock import Stock
from .position import Position
from .utilis import get_trade_day
from datetime import datetime, timedelta
import string, random
import numpy as np
import pandas as pd
import sys
import csv
from .db.portfolio_collection import PortfolioCollection
from .db.position_collection import PositionCollection
from .db.stock_holiding_collection import StockHoldingCollection

portfolio_collection = PortfolioCollection()
position_collection = PositionCollection()
stock_holding_collection = StockHoldingCollection()

def randomString(string_length = 10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(string_length))

class Portfolio(object):
    """docstring for ClassName"""
    def __init__(self, portfolio_id):
        self.portfolio_id = portfolio_id
        self.portfolio_name = portfolio_collection.get_portfolio_name(portfolio_id)
        self.owner = portfolio_collection.get_owner(portfolio_id)
        self.stock_watch_list = []
        self.position_list = [] 
        self.stock_holding_history = {} 
        self.equity_history = {}

    def initialize(self):
        if portfolio_collection.collection.find_one({"portfolioId": self.portfolio_id}):
            self.portfolio_name = portfolio_collection.get_portfolio_name(self.portfolio_id)
            self.equity_history = portfolio_collection.get_equity_history(self.portfolio_id)
            self.stock_holding_history = portfolio_collection.get_stock_holding_list(self.portfolio_id)
            if portfolio_collection.get_stock_watch_list(self.portfolio_id):
                for stock_symbol in portfolio_collection.get_stock_watch_list(self.portfolio_id):
                    stock = Stock(stock_symbol)
                    stock.initialize()
                    self.stock_watch_list.append(stock)

            if portfolio_collection.get_position_list(self.portfolio_id):
                for position_id in portfolio_collection.get_position_list(self.portfolio_id):
                    metadata = position_collection.get_position_metadata(position_id)
                    if metadata is not None:
                        symbol, purchase_time, purchase_price, number_of_shares = \
                            metadata["stockSymbol"],metadata["purchaseTime"], metadata["purchasePrice"], metadata["numberOfShare"]
                        position = Position(position_id, symbol, purchase_time, purchase_price, number_of_shares)
                        self.position_list.append(position)

            return True

    def get_share_position_size(self, symbol):
        position_size = 0
        for position in self.position_list:
            if position.symbol == symbol:
                position_size = position_size + int(position.number_of_shares)
        return position_size

    def rename(self, new_name):
        self.portfolio_name = new_name
        return portfolio_collection.rename_portfolio(self.portfolio_id, new_name)

    def find_stock_in_watch_list(self, stock_symbol):
        for stock in self.stock_watch_list:
            if stock.symbol == stock_symbol:
                return stock
        return None

    def find_position_in_position_list(self, position_id):
        for position in self.position_list:
            if position.position_id == position_id:
                return position
        return None

    def add_position(self, stock_symbol, purchase_price, position_size):            
        if self.find_stock_in_watch_list(stock_symbol) is None:
            #add stock if it is not in watchlist 
            self.add_stock_api(stock_symbol)
        if purchase_price < 0:
            print('purchase price must be larger than 0')
            return
        purchase_time = get_trade_day()   
        position_id = randomString()
        portfolio_collection.add_position(self.portfolio_id, position_id, stock_symbol, purchase_time, purchase_price, position_size)
        stock_holding_dict = self.get_stock_holding_by_symbol_api(stock_symbol)
        if stock_holding_dict:
            latest_stock_holding = list(stock_holding_dict.values())[-1]
        else:
            latest_stock_holding = 0
        new_number_of_shares = latest_stock_holding + position_size
        self.position_list.append(Position(position_id, stock_symbol, purchase_time, purchase_price, position_size))
        self.update_equity_history_api(purchase_time, self.get_equity())
        return self.update_stock_holding_history_api(stock_symbol, purchase_time, new_number_of_shares)
        

    def delete_position(self, position_id):
        position = self.find_position_in_position_list(position_id)
        if position is None:
            return
        self.position_list.remove(position)
        result = position_collection.delete_position(position_id)
        sell_time = get_trade_day()   
        stock_holding_dict = self.get_stock_holding_by_symbol_api(position.symbol)
        if stock_holding_dict:
            latest_stock_holding = list(stock_holding_dict.values())[-1]
        else:
            latest_stock_holding = 0
        new_number_of_shares = latest_stock_holding - position.number_of_shares
        self.update_equity_history_api(sell_time, self.get_equity())
        self.update_stock_holding_history_api(position.symbol, sell_time, new_number_of_shares)
        if result:            
            return portfolio_collection.delete_position(self.portfolio_id, position_id)

    def get_market_value(self):
        market_value = 0
        for position in self.position_list:
            stock = self.find_stock_in_watch_list(position.symbol)
            if stock is not None:
                market_value = market_value + float(position.number_of_shares) * float(stock.get_market_price())
        return market_value

    def get_equity(self):
        equity = 0
        for position in self.position_list:
            equity = equity + float(position.number_of_shares) * float(position.purchase_price)
        return equity

    def get_position_gain(self):
        stock_list = []

        previous_market_value_dict = {}
        today_market_value_dict = {}
        equity_dict = {}

        position_daily_gain_dict = {}
        position_total_gain_dict = {}

        for position in self.position_list:
            stock_symbol = position.symbol

            stock = self.find_stock_in_watch_list(stock_symbol)
            previous_market_price = stock.get_previous_market_price()
            today_market_price = stock.get_market_price()

            equity = position.number_of_shares * position.purchase_price
            previous_market_value = position.number_of_shares * previous_market_price
            today_market_value = position.number_of_shares * today_market_price

            if stock_symbol not in stock_list:
                stock_list.append(stock_symbol)
                previous_market_value_dict[stock_symbol] = previous_market_value
                today_market_value_dict[stock_symbol] = today_market_value
                equity_dict[stock_symbol] = equity
            else:
                previous_market_value_dict[stock_symbol] = previous_market_value_dict[stock_symbol] + previous_market_value
                today_market_value_dict[stock_symbol] = today_market_value_dict[stock_symbol] + today_market_value
                equity_dict[stock_symbol] = equity_dict[stock_symbol] + equity

            position_gain_value = today_market_value - previous_market_value
            total_gain_value = today_market_value - equity
            if equity == 0:
                position_gain_percentage = 0
                total_gain_percentage = 0
            else:
                position_gain_percentage = position_gain_value / equity
                total_gain_percentage = total_gain_value / equity

            position_daily_gain_dict[position.position_id] = (position_gain_value, position_gain_percentage)
            position_total_gain_dict[position.position_id] = (total_gain_value, total_gain_percentage)

        for stock_symbol in stock_list:
            previous_market_value = previous_market_value_dict[stock_symbol]
            today_market_value = today_market_value_dict[stock_symbol]
            equity = equity_dict[stock_symbol]

            position_gain_value = today_market_value - previous_market_value
            total_gain_value = today_market_value - equity
            if equity == 0:
                position_gain_percentage = 0
                total_gain_percentage = 0
            else:
                position_gain_percentage = position_gain_value / equity
                total_gain_percentage = total_gain_value / equity

            position_daily_gain_dict[stock_symbol] = (position_gain_value, position_gain_percentage)
            position_total_gain_dict[stock_symbol] = (total_gain_value, total_gain_percentage)
        return position_daily_gain_dict, position_total_gain_dict

    def get_daily_gain(self):
        market_value = self.get_market_value()
        previous_market_value = 0
        for position in self.position_list:
            stock = self.find_stock_in_watch_list(position.symbol)
            previous_market_value = previous_market_value + float(position.number_of_shares) * float(stock.get_previous_market_price())
        daily_gain_value = market_value - previous_market_value
        daily_gain_percentage = 0
        equity = self.get_equity()
        if equity != 0:
            daily_gain_percentage = daily_gain_value / equity
        return daily_gain_value, daily_gain_percentage

    def get_total_gain(self):
        equity = self.get_equity()
        market_value = self.get_market_value()
        total_gain_value = market_value - equity
        total_gain_percentage = 0
        if equity != 0:
            total_gain_percentage = total_gain_value / equity
        return total_gain_value, total_gain_percentage

    # api call method
    def get_stock_watch_list_api(self, portfolio_id):
        return portfolio_collection.get_stock_watch_list(portfolio_id)

    def get_position_list_api(self, portfolio_id):
        return portfolio_collection.get_position_list(portfolio_id)

    def get_stock_holding_list_api(self, portfolio_id):
        return portfolio_collection.get_stock_holding_list(portfolio_id)
    
    def get_equity_history_api(self, portfolio_id):
        return portfolio_collection.get_equity_history(portfolio_id)

    def add_stock_api(self, stock_symbol):
        return portfolio_collection.add_stock_to_watch_list(self.portfolio_id, stock_symbol)        

    def update_equity_history_api(self, purchase_time, position_size):
        return portfolio_collection.update_equity_history(self.portfolio_id, purchase_time, position_size)

    def update_stock_holding_history_api(self, stock_symbol, purchase_time, position_size):
        return portfolio_collection.update_stock_holding_history(self.portfolio_id, stock_symbol, purchase_time, position_size)

    def get_stock_holding_api(self):
        return stock_holding_collection.get_stock_holding(self.portfolio_id)

    def get_stock_holding_by_symbol_api(self, symbol):
        return stock_holding_collection.get_stock_holding_by_symbol(self.portfolio_id, symbol)

    def delete_stock_api(self, symbol):
        return portfolio_collection.delete_stock_from_watch_list(self.portfolio_id, symbol)

    def find_position_by_symbol(self, symbol):
        return position_collection.get_position_group_by_symbol(self.portfolio_id, symbol)

    # portfolio analysis method
    def get_portfolio_volatility(self):
        if len(self.stock_watch_list) == 0:
            return None
        portfolio_volatility_dict = {}
        stock_weight = 1 / len(self.stock_watch_list)
        portfolio_volatility = 0
        for stock in self.stock_watch_list:
            stock_volatility = stock.get_last_n_days_stock_volatility()
            portfolio_volatility = portfolio_volatility + stock_volatility * stock_weight
            portfolio_volatility_dict[stock.symbol] = stock_volatility
        portfolio_volatility_dict[self.portfolio_name] = portfolio_volatility
        return portfolio_volatility_dict

    def get_correlation_within_portfolio(self):
        if len(self.stock_watch_list) == 0:
            return None
        stocks_price_list = []
        stock_symbol_list = []
        for stock in self.stock_watch_list:
            stock_symbol_list.append(stock.symbol)
            stocks_price_list.append(stock.get_last_n_days_price(30))
        stocks_price_array = np.array(stocks_price_list).transpose()
        stock_price_df = pd.DataFrame(stocks_price_array, columns=stock_symbol_list)
        return stock_price_df.corr()

    @staticmethod
    def convert_history_dict_to_dataframe(dictionary, col_name):
        history_info_list = list(dictionary.items())
        history_info_table = pd.DataFrame()
        for i in range(0, len(history_info_list)):
            if i != len(history_info_list) - 1:
                begin_time = history_info_list[i][0]
                end_time = history_info_list[i + 1][0]
                dates = pd.date_range(begin_time, end_time, closed='left')
                df = pd.DataFrame(dates, columns=['Date'])
                df[col_name] = history_info_list[i][1]
                history_info_table = history_info_table.append(df, ignore_index=True)
            else:
                begin_time = history_info_list[i][0]
                end_time = get_trade_day()
                dates = pd.date_range(begin_time, end_time)
                df = pd.DataFrame(dates, columns=['Date'])
                df[col_name] = history_info_list[i][1]
                history_info_table = history_info_table.append(df, ignore_index=True)
        return history_info_table

    def get_historical_equity_dataframe(self):
        equity_history = self.get_equity_history_api(self.portfolio_id)
        if equity_history is None:
            return None
        historical_equity_dataframe = self.convert_history_dict_to_dataframe(equity_history, 'Equity')
        return historical_equity_dataframe

    def get_historical_market_value_dataframe(self):
        holding_history = self.get_stock_holding_api()
        if holding_history is None:
            return None

        market_value_df = pd.DataFrame(columns=['Date'])

        while holding_history != {}:
            stock_holding_history_dict = holding_history.popitem()
            stock_symbol = stock_holding_history_dict[0]
            stock_holding_history_df = self.convert_history_dict_to_dataframe(stock_holding_history_dict[1], 'Position_size')
            stock = Stock(stock_symbol)
            stock.initialize()
            stock_holding_history_df['Market Price'] = stock.get_price_from_certain_day(stock_holding_history_df['Date'][0].strftime('%Y-%m-%d'))
            stock_holding_history_df[stock_symbol] = stock_holding_history_df['Market Price'] * stock_holding_history_df['Position_size']
            market_value_df = pd.merge(market_value_df, stock_holding_history_df[['Date', stock_symbol]], on='Date', how='outer', sort=True)

        market_value_df = market_value_df.fillna(0)
        market_value_df['Market Value'] = market_value_df[list(market_value_df.columns)[1:]].sum(axis=1)
        self.historical_market_value_dataframe = market_value_df[['Date', 'Market Value']]
        return self.historical_market_value_dataframe

    #  dataframes that can be used for drawing equity curve, market value curve, gain curve and yield curve
    def get_portfolio_curve_data(self):
        historical_equity_df = self.get_historical_equity_dataframe()
        historical_market_value_df = self.get_historical_market_value_dataframe()
        if historical_equity_df is None or historical_market_value_df is None:
            return None
        df = pd.merge(historical_equity_df, historical_market_value_df, on='Date', how='outer', sort=True)
        df['Gain'] = df['Market Value'] - df['Equity']
        df['Yield'] = df['Gain']/df['Equity']
        df = df.replace([np.inf, -np.inf], np.nan).fillna(0)
        output_df = df[['Date', 'Market Value', 'Equity', 'Gain', 'Yield']]
        output_df.columns = ['Date', 'Market_Value_' + self.portfolio_name, 'Equity_' + self.portfolio_name, 'Gain_' + self.portfolio_name, 'Yield_' + self.portfolio_name]
        return output_df

    def get_stocks_summary(self):
        stock_summary_dict = {}
        previous_market_value_dict = {}

        for stock in self.stock_watch_list:
            stock_summary_dict[stock.symbol] = {'symbol': stock.symbol, 'intraday gain': stock.intraday_gain,
                                                'shares': 0,'market value': 0, 'equity': 0, 'daily gain': 0, 'daily gain (percentage)': 0,
                                               'total gain': 0, 'total gain (percentage)': 0,
                                                'number of lots': 0, 'positions': []}
            previous_market_value_dict[stock.symbol] = 0

        for position in self.position_list:
            stock_symbol = position.symbol

            stock = self.find_stock_in_watch_list(stock_symbol)
            previous_market_price = stock.get_previous_market_price()
            today_market_price = stock.get_market_price()

            equity = position.number_of_shares * position.purchase_price
            previous_market_value = position.number_of_shares * previous_market_price
            today_market_value = position.number_of_shares * today_market_price

            previous_market_value_dict[stock_symbol] = previous_market_value_dict[stock_symbol] + previous_market_value
            stock_summary_dict[stock_symbol]['market value'] = round(stock_summary_dict[stock_symbol]['market value'] + today_market_value,2)
            stock_summary_dict[stock_symbol]['equity'] = round(stock_summary_dict[stock_symbol]['equity'] + equity,2)

            position_gain_value = today_market_value - previous_market_value
            total_gain_value = today_market_value - equity
            if equity == 0:
                position_gain_percentage = 0
                total_gain_percentage = 0
            else:
                position_gain_percentage = position_gain_value / equity
                total_gain_percentage = total_gain_value / equity

            d = {'position id': position.position_id, 'date': position.purchase_time, 'shares': position.number_of_shares,
                 'daily gain': round(position_gain_value,2), 'daily gain (percentage)': round(position_gain_percentage,4),
                 'total gain': round(total_gain_value,2), 'total gain (percentage)': round(total_gain_percentage,4), 'cost base': equity}

            stock_summary_dict[stock_symbol]['shares'] = stock_summary_dict[stock_symbol]['shares'] + position.number_of_shares
            stock_summary_dict[stock_symbol]['number of lots'] = stock_summary_dict[stock_symbol]['number of lots'] + 1
            stock_summary_dict[stock_symbol]['positions'].append(d)

        for stock_symbol in list(stock_summary_dict.keys()):
            previous_market_value = previous_market_value_dict[stock_symbol]
            today_market_value = stock_summary_dict[stock_symbol]['market value']
            equity = stock_summary_dict[stock_symbol]['equity']

            position_gain_value = today_market_value - previous_market_value
            total_gain_value = today_market_value - equity
            if equity == 0:
                position_gain_percentage = 0
                total_gain_percentage = 0
            else:
                position_gain_percentage = position_gain_value / equity
                total_gain_percentage = total_gain_value / equity

            stock_summary_dict[stock_symbol]['daily gain'] = round(position_gain_value,2)
            stock_summary_dict[stock_symbol]['daily gain (percentage)'] = round(position_gain_percentage,4 )
            stock_summary_dict[stock_symbol]['total gain'] = round(total_gain_value,2)
            stock_summary_dict[stock_symbol]['total gain (percentage)'] = round(total_gain_percentage,4)

        return stock_summary_dict

    def get_equity_allocation_pie_chart_data(self):
        sector_list = []
        equity_allocation = {}
        for position in self.position_list:
            sector = self.find_stock_in_watch_list(position.symbol).sector
            if sector in sector_list:
                equity_allocation[sector] = equity_allocation[sector] + float(position.number_of_shares) * float(position.purchase_price)
            else:
                equity_allocation[sector] = float(position.number_of_shares) * float(position.purchase_price)
                sector_list.append(sector)
        return equity_allocation