import sys
import numpy as np
from datetime import datetime
import talib as ta
from .db.stock_collection import StockCollection

db = StockCollection()

class Stock(object):
    """docstring for ClassName"""
    def __init__(self, symbol):
        self.symbol = symbol
        # stock data
        self.best_bid = 0
        self.best_ask = 0
        self.today_high = 0
        self.today_low = 0
        self.intraday_gain = db.get_intraday_gain(symbol)
        self.share_volume = 0
        self.fifty_day_avg_daily_volume = 0
        self.previous_close = 0
        self.fifty_two_week_high = 0
        self.fifty_two_weekLow = 0
        # company data
        self.company_name = ''
        self.ipo_year = None
        self.industry = ''
        self.sector = ''
        self.market_cap = 0
        self.annualized_dividend = 0
        self.ex_dividend_date = None
        self.dividend_payment_date = None
        self.current_yield = 0
        # price data
        self.daily_price = self.get_daily_price(self.symbol)

    def initialize(self):
        self.get_stock_data()
        self.get_company_data()
        self.get_daily_price(self.symbol)
        self.get_market_price()

    def refresh(self):
        self.initialize()

    def get_stock_data(self):
        self.best_bid, self.best_ask, self.today_high, self.today_low, self.share_volume, \
            self.fifty_day_avg_daily_volume, self.previous_close, \
            self.fifty_two_week_high, self.fifty_two_weekLow = db.get_stock_data(self.symbol)

    def get_company_data(self):
        if db.get_company_data(self.symbol):
            self.company_name, self.ipo_year, self.industry, self.sector, self.market_cap, self.annualized_dividend, \
            self.ex_dividend_date, self.dividend_payment_date, self.current_yield = db.get_company_data(self.symbol)

    def get_daily_price(self, symbol):
        self.daily_price = db.get_daily_price_data(symbol)

    def get_market_price(self):
        self.get_stock_data()
        if self.best_ask == 0 or self.best_bid == 0:
            if len(self.daily_price) >= 1:
                return self.daily_price[-1]
            return 0
        elif self.best_ask == 0:
            return self.best_bid
        elif self.best_bid == 0:
            return self.best_ask
        else:
            return (self.best_ask+self.best_bid)/2

    def get_stock_price_daily_gain(self):
        market_price = self.get_market_price()
        previous_market_price = self.get_previous_market_price()
        if market_price == 0 or previous_market_price == 0:
            return 0, 0
        gain_value = market_price - previous_market_price
        gain_percentage = (market_price - previous_market_price) / previous_market_price
        return gain_value, gain_percentage

    def get_previous_market_price(self):
        if len(self.daily_price) < 2:
            return 0
        return self.daily_price[-2]  # note taht daily_price[-1] is today's price

    def get_last_n_days_price(self, number_of_day):
        if len(self.daily_price) < number_of_day:
            return [0]* number_of_day
        return self.daily_price[-number_of_day:]

    def get_price_on_certain_day(self, date):
        date = datetime.strptime(date, '%Y-%m-%d')
        startTime = datetime.strptime("2019-11-25 11:08:00", "%Y-%m-%d %H:%M:%S")
        day_count = (startTime - date).days + 1
        if len(self.daily_price) < day_count:
            return 0
        return self.daily_price[-day_count]

    def get_price_from_certain_day(self, date):
        start_day = datetime.strptime(date, '%Y-%m-%d')
        startTime = datetime.strptime("2019-11-25 11:08:00", "%Y-%m-%d %H:%M:%S")
        day_count = (startTime - start_day).days + 1
        return self.get_last_n_days_price(day_count)

    def get_last_n_days_stock_gain_and_yield_list(self, days = 352):  # there are 352 trading days in a year
        price_list_last_n_days = self.get_last_n_days_price(days)
        stock_yield_list = []
        stock_gain_list = []
        for i in range(0, days - 1):
            stock_gain = 0
            stock_yield = 0
            if price_list_last_n_days[i] != 0:
                stock_gain = price_list_last_n_days[i + 1] - price_list_last_n_days[i]
                stock_yield = price_list_last_n_days[i + 1]/price_list_last_n_days[i] - 1
            stock_gain_list.append(stock_gain)
            stock_yield_list.append(stock_yield)
        return stock_gain_list, stock_yield_list

    def get_last_n_days_stock_volatility(self, days = 30):  # there are 352 trading days in a year
        return np.array(self.get_last_n_days_price(days)).var()


    # return a list, note that the first timeperiod item is nan
    def get_sma(self, timeperiod = 30):
        return ta.SMA(self.daily_price, timeperiod)

    def get_bollinger_band(self, timeperiod=5):
        upperband, middleband, lowerband = ta.BBANDS(self.daily_price, timeperiod)
        return upperband, middleband, lowerband

    def get_macd(self, fastperiod=12, slowperiod=26, signalperiod=9):
        macd, macdsignal, macdhist = ta.MACD(self.daily_price, fastperiod, slowperiod, signalperiod)
        return macd, macdsignal, macdhist

    def get_rsi(self, timeperiod = 14):
        rsi = ta.RSI(self.daily_price, timeperiod)
        return rsi

    # def get_adx(self, timeperiod=14):
    #     adx = ta.ADX(self.high, self.low, self.close, timeperiod)
    #     return adx

    def get_momentum(self, timeperiod=5):
        mom = ta.MOM(self.daily_price, timeperiod)
        return mom

    def get_daily_gain(self, timeperiod=1):
        return ta.ROC(np.array(self.daily_price), timeperiod)