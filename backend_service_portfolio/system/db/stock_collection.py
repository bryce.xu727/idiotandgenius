from .dbconfig import *
import requests
import pandas as pd
from datetime import datetime, timedelta

def data_extraction(json_data, json_field):
    if "N/A" in json_data[json_field]:
        front_value = 0
        back_value = 0
    elif "/" in json_data[json_field]:
        split_arr = json_data[json_field].split("/")
        front_value = split_arr[0].replace("$","").strip()
        back_value = split_arr[1].replace("$","").strip()
    return front_value, back_value    

class StockCollection(MongoCollection):
    
    def __init__(self):
        super().__init__()
        self._collection = self.db["stock"]

    @property
    def db(self):
        return self._db
    
    @property
    def collection(self):
        return self._collection


    def get_stock_data(self, stock_symbol):
        url = 'http://127.0.0.1:9333/stocks/{}/information'.format(stock_symbol)
        metaData = requests.get(url)
        if metaData.status_code == 404:
            return False        
        metaData_json = metaData.json()
        try:
            best_bid, best_ask = data_extraction(metaData_json, "Best Bid / Ask")
        except:
            best_bid, best_ask = 0, 0
        today_high, today_low = data_extraction(metaData_json, "Today's High / Low")
        share_volume = metaData_json["Share Volume"]
        fifty_day_avg_daily_volume = metaData_json["50 Day Avg Daily Volume"]
        previous_close = metaData_json["Previous Close"].replace("$","").strip()
        fifty_two_week_high, fifty_two_weekLow = data_extraction(metaData_json, "52 Week High / Low")
        return float(best_bid), float(best_ask), today_high, today_low, share_volume, fifty_day_avg_daily_volume, \
            previous_close, fifty_two_week_high, fifty_two_weekLow

    def get_daily_price_data(self, stock_symbol):
        url = 'http://127.0.0.1:8100/stocks/{}/dailyQuote'.format(stock_symbol)
        daily_data = requests.get(url)
        if daily_data.status_code == 404:
            return False
        daily_data_json = daily_data.json()
        daily_data_json.pop('Symbol')
        key_list = list(daily_data_json.keys())
        daily_data_list = list(daily_data_json.values())
        new_dict = {}
        list_len= len(key_list)        
        for i in range(list_len):
            new_dict[key_list[list_len-i-1]] = float(daily_data_list[list_len-i-1]["close"])
        # to fill the data up to current date
        clean_df = self.convert_history_dict_to_dataframe(new_dict, "Close")
        close_price_list = clean_df['Close'].values.tolist()
        return close_price_list

    def get_company_data(self, stock_symbol):
        url = 'http://127.0.0.1:9333/stocks/{}/information'.format(stock_symbol)
        metaData = requests.get(url)
        if metaData.status_code == 404:
            return False        
        metaData_json = metaData.json()
        company_name = metaData_json['Name']
        ipo_year = metaData_json['IPO year']
        industry = metaData_json['Industry']
        sector = metaData_json['Sector']
        market_cap = metaData_json['Market Cap']
        annualized_dividend = metaData_json['Annualized Dividend']
        ex_dividend_date = metaData_json['Ex Dividend Date']
        dividend_payment_date = metaData_json['Dividend Payment Date']
        current_yield = metaData_json['Current Yield']
        return company_name, ipo_year, industry, sector, market_cap, annualized_dividend, \
            ex_dividend_date, dividend_payment_date, current_yield

    def convert_history_dict_to_dataframe(self, dictionary, col_name):
        history_info_list = list(dictionary.items())
        history_info_table = pd.DataFrame()
        for i in range(0, len(history_info_list)):
            if i != len(history_info_list) - 1:
                begin_time = history_info_list[i][0]
                end_time = history_info_list[i + 1][0]
                dates = pd.date_range(begin_time, end_time, closed='left')
                df = pd.DataFrame(dates, columns=['Date'])
                df[col_name] = history_info_list[i][1]
                history_info_table = history_info_table.append(df, ignore_index=True)
            else:
                begin_time = history_info_list[i][0]
                end_time = get_trade_day()
                dates = pd.date_range(begin_time, end_time)
                df = pd.DataFrame(dates, columns=['Date'])
                df[col_name] = history_info_list[i][1]
                history_info_table = history_info_table.append(df, ignore_index=True)
        return history_info_table   

    def get_intraday_gain(self, symbol):
        url = 'http://127.0.0.1:8100/stocks/{}/quote_summary'.format(symbol)
        metaData = requests.get(url)
        if metaData.status_code == 404:
            return 0
        metaData_json = metaData.json()
        return metaData_json['changePercentage']
        