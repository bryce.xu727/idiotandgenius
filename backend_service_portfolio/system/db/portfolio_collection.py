from .dbconfig import *
from .position_collection import PositionCollection
from .stock_holiding_collection import StockHoldingCollection
from pymongo import ASCENDING

class PortfolioCollection(MongoCollection):
    
    def __init__(self):
        super().__init__()
        self._collection = self.db["portfolio"]
        self._collection.ensure_index([('portfolioName', ASCENDING )])
        self._collection.ensure_index([('owner', ASCENDING )])
        self._collection.ensure_index([('stockWatchList', ASCENDING )])
        self._collection.ensure_index([('stockPositionList', ASCENDING )])
        self._collection.ensure_index([('stockHoldingHistory', ASCENDING )])
        self._collection.ensure_index([('equityHistory', ASCENDING )])

    @property
    def db(self):
        return self._db
    
    @property
    def collection(self):
        return self._collection

    def rename_portfolio(self, portfolio_id, new_name):
        result = None
        query = {"portfolioId": portfolio_id}
        cursor = self.collection.find_one(query)
        if cursor:
            result = self.collection.update(query,{"$set": {"portfolioName": new_name}})
        return result

    def get_portfolio_name(self, portfolio_id):
        portfolio_name = None
        query = {"portfolioId": portfolio_id}
        if self.collection.find_one(query):
            for i in self.collection.find(query):
                portfolio_name = i["portfolioName"]
        return portfolio_name  
    
    def get_stock_watch_list(self, portfolio_id):
        query = {"portfolioId": portfolio_id}
        return get_data(query, "stockWatchList", self.collection)              
    
    def get_position_list(self, portfolio_id):
        query = {"portfolioId": portfolio_id}
        return get_data(query, "stockPositionList", self.collection)              

    def get_stock_holding_list(self, portfolio_id):
        query = {"portfolioId": portfolio_id}
        return get_data(query, "stockHoldingHistory", self.collection)       

    def get_equity_history(self, portfolio_id):
        query = {"portfolioId": portfolio_id}
        return get_data(query, "equityHistory", self.collection)                      

    def add_stock_to_watch_list(self, portfolio_id, symbol):
        result = None
        query = {"portfolioId": portfolio_id}
        if self.collection.find_one(query):
            return self.collection.update(query, {"$addToSet" : {"stockWatchList" : symbol}})
        return result
            
    def delete_stock_from_watch_list(self, portfolio_id, symbol):
        result = None
        query = {"portfolioId":portfolio_id}
        if self.collection.find_one(query):
            return self.collection.update(query, {"$pull" : {"stockWatchList" : symbol}})
        return result                

    def add_position(self, portfolio_id, position_id, symbol, purchase_time, purchase_price, number_of_shares):
        result = None
        query = {"portfolioId": portfolio_id}
        cursor = self.collection.find_one(query)
        positionId = randomString()
        if cursor:
            result = self.collection.update(query, {"$addToSet" : {"stockPositionList" : positionId}})
            if result: 
                new_position = {
                    "positionId" : positionId,
                    "portfolioId": portfolio_id,
                    "stockSymbol": symbol,
                    "purchaseTime": purchase_time,
                    "purchasePrice": purchase_price,
                    "numberOfShare": number_of_shares
                }
                position_collection = PositionCollection()
                return position_collection.collection.insert_one(new_position)
        
        return result

    def delete_portfolio(self,portfolio_id):
        query = {"portfolioId": portfolio_id}
        if self.collection.find_one(query):
            cursor = self.collection.find(query)
            position_list = []
            for i in cursor:
                position_list = i["stockPositionList"]

            # delete in position  
            number_of_share = {}
            if position_list:
                position_collection = PositionCollection()                
                for position in position_list:
                    if position_collection.delete_position(position):
                        self.collection.update(query,{"$pull": {"stockPositionList": position}})                    
            
            #delete in stock holding
            stock_holding_collection = StockHoldingCollection()
            stock_holding_collection.collection.delete_many(query)
        
            self.collection.delete_one(query)  
            return True 
        else:
            return False

    def get_owner(self, portfolio_id):
        result = None
        query = {"portfolioId": portfolio_id}
        if self.collection.find_one(query):
            result = get_data(query, "owner", self.collection)
        
        return result         

    def update_equity_history(self,portfolio_id, purchase_time, number_of_equity):
        query = {"portfolioId": portfolio_id}
        if self.collection.find_one(query):
            return self.collection.update(query, {"$set" : {"equityHistory.{}".format(purchase_time) : number_of_equity}})                                      

    def update_stock_holding_history(self, portfolio_id, symbol, purchase_time, position_size):
        query = {"portfolioId": portfolio_id}
        if self.collection.find_one(query):
            stock_holding_collection = StockHoldingCollection()
            stock_holding_collection.update_stock_holding(portfolio_id, symbol, position_size, purchase_time)
            return self.collection.update(query, {"$addToSet" : {"stockHoldingHistory" : symbol}})

    def delete_position(self, portfolio_id, position_id):
        query = {"portfolioId": portfolio_id}
        if self.collection.find_one(query):
            return self.collection.update(query, {"$pull": {"stockPositionList": position_id}})
