from .dbconfig import *
from pymongo import ASCENDING

class StockHoldingCollection(MongoCollection):
    
    def __init__(self):
        super().__init__()
        self._collection = self.db["stockHolding"] 
        self._collection.ensure_index([('portfolio', ASCENDING )])
        self._collection.ensure_index([('symbol', ASCENDING )])
        self._collection.ensure_index([('size', ASCENDING )])
        self._collection.ensure_index([('time', ASCENDING )])

    @property
    def collection(self):
        return self._collection

    @property
    def db(self):
        return self._db
        

    def update_stock_holding(self, portfolio, symbol, size, time):
        query = {
            "portfolio": portfolio,
            "symbol": symbol,
            "time": time
        }
        if self.collection.find_one(query):
            return self.collection.update(query, {"$set": {"size": size}})
        else:
            query["size"] = size
            return self.collection.insert_one(query)

    def get_stock_holding_by_symbol(self, portfolio, symbol):
        query = {"portfolio": portfolio, "symbol": symbol}
        cursor = self.collection.find(query)
        if cursor:
            stock_holding_history_dict = {}
            for i in cursor:
                time = i["time"]
                size = i["size"]
                stock_holding_history_dict[time] = size
            
            return stock_holding_history_dict

    def get_stock_holding(self, portfolio):
        query = {"portfolio": portfolio}
        if self.collection.find_one(query):
            cursor = self.collection.find(query)
            symbol_list = []
            stock_holding_history_dict = {}
            for i in cursor:
                symbol = i["symbol"]
                if symbol not in symbol_list:
                    symbol_list.append(symbol)
            for symbol in symbol_list:
                time_dict = {}
                for i in self.collection.find({"portfolio": portfolio, "symbol": symbol}):
                    time = i["time"]
                    size = i["size"]
                    time_dict[time] = i["size"]
                stock_holding_history_dict[symbol] = time_dict
            
            return stock_holding_history_dict            