from pymongo import MongoClient
import pymongo

class Importer:
    def __init__(self):
        self._client = MongoClient("mongodb+srv://DBAdmin:GeniusFinanceDBAdmin@geniusfinance-p7ayy.gcp.mongodb.net/test?retryWrites=true&w=majority")
        self._db = self._client['comp9900_genius_finance']

    def init_db(self):
        user = {"username" : "user", "password" : "pbkdf2:sha256:150000$DeuLa0lA$42b3d8d9d863fc998c31ed4ad1d85826eabb7fe3ef91144b6541ffa3f946238e" }
        self._db['user'].update_one({"username" : "user"},{"$set":user},upsert=True)

        user_portfolio={"username" : "user", "portfolioList" : [ "bqenssevbg" ], "operationHistory" : [  "create portfolio bqenssevbg 2019-08-04 14:48:13.415000" ] }
        self._db['userPortfolio'].update_one({"username" : "user"},{"$set":user_portfolio},upsert=True)
        
        portfolio = {"portfolioId" : "bqenssevbg", "owner" : "user", "portfolioName" : "new portfolio", "stockWatchList" : [ "ATVI", "GOOG" ], "stockPositionList" : [ "wufiafxmwk", "cuzwnygulq" ], "stockHoldingHistory" : [ "ATVI", "GOOG" ], "equityHistory" : {  "2019-11-25" : 1125 ,"2019-11-25" : 3125 } }
        self._db['portfolio'].update_one({"portfolioId" : "bqenssevbg"},{"$set":portfolio},upsert=True)
        
        position1 = { "positionId" : "wufiafxmwk", "portfolioId" : "bqenssevbg", "stockSymbol" : "ATVI", "purchaseTime" : "2019-11-25", "purchasePrice" : 50, "numberOfShare" : 40 }
        position2 = {  "positionId" : "cuzwnygulq", "portfolioId" : "bqenssevbg", "stockSymbol" : "GOOG", "purchaseTime" : "2019-11-25", "purchasePrice" : 1125, "numberOfShare" : 1 }
        self._db['position'].update_one(position1,{"$set":position1},upsert=True)
        self._db['position'].update_one(position2,{"$set":position2},upsert=True)
        
        holding1 = {"portfolio" : "bqenssevbg", "symbol" : "ATVI", "time" : "2019-11-25", "size" : 40 }
        holding2 = {  "portfolio" : "bqenssevbg", "symbol" : "GOOG", "time" : "2019-11-257", "size" : 1 }
        self._db['stockHolding'].update_one(holding1,{"$set":holding1},upsert=True)
        self._db['stockHolding'].update_one(holding2,{"$set":holding2},upsert=True)