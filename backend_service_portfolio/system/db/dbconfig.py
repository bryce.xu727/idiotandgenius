from pymongo import MongoClient
import pymongo
import json, random, string

def randomString(stringLength=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))

def get_trade_day():
    return "2019-11-25"

def get_data(query, fieldname, collection):
    """A function to get the data
    
    Arguments:
        query {[dict]} -- [reference query]
        fieldname {[string]} -- [the name of the field]
        collection {[Collection]} -- [data collection in MongoDb]
    
    Returns:
        [object] -- [data]
    """
    data = None
    cursor = collection.find_one(query)
    if cursor:
        result = collection.find(query).sort('_id', pymongo.ASCENDING).limit(1)
        for i in result:
            data = i[fieldname]
    if not data:
        return None       
    return data     

class MongoCollection(object):
    def __init__(self):
        self._client = MongoClient("mongodb+srv://DBAdmin:GeniusFinanceDBAdmin@geniusfinance-p7ayy.gcp.mongodb.net/test?retryWrites=true&w=majority")
        self._db = self._client['comp9900_genius_finance']