from .dbconfig import *
from pymongo import ASCENDING

class PositionCollection(MongoCollection):
    
    def __init__(self):
        super().__init__()
        self._collection = self.db["position"] 
        self._collection.ensure_index([('symbol', ASCENDING )])
        self._collection.ensure_index([('purchaseTime', ASCENDING )])
        self._collection.ensure_index([('purchasePrice', ASCENDING )])
        self._collection.ensure_index([('positionSize', ASCENDING )])

    @property
    def db(self):
        return self._db
    
    @property
    def collection(self):
        return self._collection
    
    def get_position_metadata(self,position_id):
        position = self.collection.find({"positionId" : position_id})
        for i in position:
            if i["positionId"]:
                return {
                    "positionId": i["positionId"],
                    "stockSymbol": i["stockSymbol"],
                    "purchaseTime": i["purchaseTime"],
                    "purchasePrice": i["purchasePrice"],
                    "numberOfShare": i["numberOfShare"]
                }
        return None

    def delete_position(self, position_id):
        query = {"positionId": position_id}
        if self.collection.find(query):
            self.collection.delete_one(query)
            return True

        return None

    def get_all_position(self, portfolio_id):
        query = {"portfolioId": portfolio_id}
        result = self.collection.find(query)
        positions = []
        if result:
            for i in result:
                position = {
                    "positionId": i["positionId"],
                    "stockSymbol": i["stockSymbol"],
                    "purchaseTime": i["purchaseTime"],
                    "purchasePrice": i["purchasePrice"],
                    "numberOfShare": i["numberOfShare"]
                }
                positions.append(position)
        
        return positions

    def get_position_group_by_symbol(self, portfolio_id, symbol):
        result = self.get_all_position(portfolio_id)
        if result:
            positions = []
            for i in result:
                if i["stockSymbol"]==symbol:
                    positions.append(i)

            return positions

    def find_position_by_symbol(self, portfolio_id, symbol):
        query = {"portfolioId": portfolio_id}
        result = self.collection.find(query)
        return result