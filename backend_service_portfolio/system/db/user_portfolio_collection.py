from .dbconfig import *
from .portfolio_collection import PortfolioCollection
from pymongo import ASCENDING

class UserPortfolioCollection(MongoCollection):
    
    def __init__(self):
        super().__init__()
        self._collection = self.db["userPortfolio"]
        self._collection.ensure_index([('portfolioList', ASCENDING )])
        self._collection.ensure_index([('operationHistory', ASCENDING )])
        
    @property
    def db(self):
        return self._db
    
    @property
    def collection(self):
        return self._collection

    def get_portfolio_list(self, username):
        query = {"username":username}
        porttfolio_list = []
        porttfolio_list = get_data(query,"portfolioList",self.collection)
        return porttfolio_list

    def add_portfolio(self, username, portfolio_name, portfolio_id):
        result = None
        query = {"username":username} 
        new_portfolio = {
            "portfolioId": portfolio_id,
            "owner": username,
            "portfolioName" : portfolio_name,
            "stockWatchList": [],
            "stockPositionList": [],
            "stockHoldingHistory": [],
            "equityHistory": {},
        }       
        if self.collection.find_one(query):            
            result = self.collection.update(query, {"$addToSet" : {"portfolioList" : portfolio_id}})
            if result:
                portfolio_collection = PortfolioCollection()
                result = portfolio_collection.collection.insert_one(new_portfolio, bypass_document_validation=True)
        else:
            if self.collection.insert_one({
                "username" : username,
                "portfolioList": [],
                "operationHistory": []
            }):
                result = self.collection.update(query, {"$addToSet" : {"portfolioList" : portfolio_id}})
                if result:
                    portfolio_collection = PortfolioCollection()
                    result = portfolio_collection.collection.insert_one(new_portfolio, bypass_document_validation=True)                
        return result  
    
    def get_operation_history(self, username):
        query = {"username":username}
        operation_history_list = []
        operation_history_list = get_data(query,"operationHistory", self.collection)        
        return operation_history_list  

    def delete_portfolio(self, username, portfolio_id):
        query = {"username":username}
        # delete in portfolio_collection
        portfolio_collection = PortfolioCollection()
        if portfolio_collection.delete_portfolio(portfolio_id):
            self.collection.update(query,{"$pull": {"portfolioList": portfolio_id}})                    
            return True

        return False

    def add_operation_history(self, username, statement):
        result = None
        if self.collection.find_one({"username":username}):
            result = self.collection.update({"username":username}, {"$addToSet" : {"operationHistory" : statement}})
                 
        return result 