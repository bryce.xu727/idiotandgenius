from datetime import datetime
import string, random
from .portfolio import Portfolio
from .db.user_portfolio_collection import UserPortfolioCollection
from .db.portfolio_collection import PortfolioCollection
import numpy as np
import pandas as pd

db = UserPortfolioCollection()
portfolio_collection = PortfolioCollection()

def randomString(string_length = 10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(string_length))

class PortfolioManager(object):
    def __init__(self, user_id):
        self.user_id = user_id
        self.portfolio_list = []
        self.operation_history = []

    def initialize(self):
        if db.get_portfolio_list(self.user_id):
            for portfolio_id in db.get_portfolio_list(self.user_id):
                portfolio = Portfolio(portfolio_id)
                portfolio.initialize()
                self.portfolio_list.append(portfolio)
            
            self.operation_history = db.get_operation_history(self.user_id)
        

    def print_portfolio_list(self):
        for p in self.portfolio_list:
            print(p.portfolio_name)

    def print_operation_history(self):
        for h in self.operation_history:
            print(h)

    def find_portfolio(self, portfolio_id):
        for p in self.portfolio_list:
            if p.portfolio_id == portfolio_id:
                return p
        return None

    def create_portfolio(self, portfolio_name):
        portfolio_id = randomString()
        p = Portfolio(portfolio_id)
        self.portfolio_list.append(p)
        result = db.add_portfolio(self.user_id, portfolio_name, portfolio_id)
        statement = 'create portfolio {} '.format(portfolio_id)
        self.add_operation_history(statement)
        return result

    def delete_portfolio(self, portfolio_id):
        p = self.find_portfolio(portfolio_id)
        if p is None:
            print('Portfolio is not found')
            return
        self.portfolio_list.remove(p)

    def rename_portfolio(self, portfolio_id, new_name):
        p = self.find_portfolio(portfolio_id)
        if p is None:
            print('Portfolio is not found')
            return
        p.rename(new_name)
        db.rename_portfolio(self.user_id, p.portfolio_id)
        return self.add_operation_history('rename portfolio %s' % p.portfolio_name)

    def get_market_value(self):
        total_market_value = 0
        for portfolio in self.portfolio_list:
            total_market_value = total_market_value + portfolio.get_market_value()
        return total_market_value

    def get_equity(self):
        total_equity = 0
        for portfolio in self.portfolio_list:
            total_equity = total_equity + portfolio.get_equity()
        return total_equity

    def get_daily_gain(self):
        total_market_value = self.get_market_value()
        total_equity = self.get_equity()
        total_daily_gain = 0
        for portfolio in self.portfolio_list:
            portfolio_daily_gain, portfolio_daily_gain_percentage = portfolio.get_daily_gain()
            total_daily_gain = total_daily_gain + portfolio_daily_gain
        total_daily_gain_percentage = 0
        if total_equity != 0:
            total_daily_gain_percentage = total_daily_gain / total_equity
        return total_daily_gain, total_daily_gain_percentage

    def get_total_gain(self):
        total_market_value = self.get_market_value()
        total_equity = self.get_equity()
        total_gain_value = total_market_value - total_equity
        total_gain_percentage = 0
        if total_equity != 0:
            total_gain_percentage = total_gain_value / total_equity
        return total_gain_value, total_gain_percentage

    def add_operation_history(self, operation):
        if self.operation_history is None:
            self.operation_history = []
        self.operation_history.append((str(datetime.now()), operation))
        db.add_operation_history(self.user_id, operation+str(datetime.now()))

    def get_portfolio_list_api(self, user_id):
        return db.get_portfolio_list(self.user_id)

    def get_operation_history_api(self, user_id):
        return db.get_operation_history(user_id)

    def delete_portfolio_api(self, portfolio_id):
        self.add_operation_history('delete portfolio %s ' % portfolio_id) 
        return db.delete_portfolio(self.user_id, portfolio_id)

    def get_portfolio_name_api(self, portfolio_id):
        return portfolio_collection.get_portfolio_name(portfolio_id)

    # portfolio characteristic
    def get_equity_allocation_pie_chart_data(self):
        sector_list = []
        equity_allocation = {}
        for portfolio in self.portfolio_list:
            for position in portfolio.position_list:
                sector = portfolio.find_stock_in_watch_list(position.symbol).sector
                if sector in sector_list:
                    equity_allocation[sector] = equity_allocation[sector] + float(position.number_of_shares) * float(position.purchase_price)
                else:
                    equity_allocation[sector] = float(position.number_of_shares) * float(position.purchase_price)
                    sector_list.append(sector)
        return equity_allocation

    # risk analysis
    def get_volatility_table_data(self):
        volatility_dict = {}
        for portfolio in self.portfolio_list:
            portfolio_volatility = portfolio.get_portfolio_volatility()
            if portfolio_volatility:
                volatility_dict[portfolio.portfolio_name] = portfolio_volatility
        return volatility_dict

    # risk analysis
    def get_correlation_table_data(self):
        correlation_dict = {}
        for portfolio in self.portfolio_list:
            correlation = portfolio.get_correlation_within_portfolio()
            if correlation is not None:
                correlation_dict[portfolio.portfolio_name] = portfolio.get_correlation_within_portfolio()
        return correlation_dict

    # performance analysis
    # dataframes that can be used for drawing equity curve, market value curve, gain curve and yield curve
    def get_curve_data(self):
        equity_df = pd.DataFrame(columns=['Date'])
        market_value_df = pd.DataFrame(columns=['Date'])
        gain_df = pd.DataFrame(columns=['Date'])
        yield_df = pd.DataFrame(columns=['Date'])

        for portfolio in self.portfolio_list:
            portfolio_df = portfolio.get_portfolio_curve_data()
            if portfolio_df is None:
                continue

            equity_df = pd.merge(equity_df, portfolio_df[['Date', 'Equity_' + portfolio.portfolio_name]], on='Date', how='outer', sort=True)
            market_value_df = pd.merge(market_value_df, portfolio_df[['Date', 'Market_Value_' + portfolio.portfolio_name]], on='Date', how='outer', sort=True)
            gain_df = pd.merge(gain_df, portfolio_df[['Date', 'Gain_' + portfolio.portfolio_name]], on='Date', how='outer', sort=True)
            yield_df = pd.merge(yield_df, portfolio_df[['Date', 'Yield_' + portfolio.portfolio_name]], on='Date', how='outer', sort=True)

        equity_df = equity_df.fillna(0)
        market_value_df = market_value_df.fillna(0)
        gain_df = gain_df.fillna(0)
        yield_df = yield_df.fillna(0)

        equity_df['Total Equity'] = equity_df[list(equity_df.columns)[1:]].sum(axis=1)
        market_value_df['Total Market Value'] = market_value_df[list(market_value_df.columns)[1:]].sum(axis=1)
        gain_df['Total Gain'] = gain_df[list(gain_df.columns)[1:]].sum(axis=1)
        yield_df['Total Yield'] = gain_df['Total Gain'] / equity_df['Total Equity']
        yield_df = yield_df.replace([np.inf, -np.inf], np.nan).fillna(0)

        return equity_df, market_value_df, gain_df, yield_df
