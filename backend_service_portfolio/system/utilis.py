import requests

def get_recent_daily_price(symbol):
    url = "http://localhost:8100/stocks/{}/dailyQuote".format(symbol)

    try:
        r = requests.get(url)
        load_dict = r.json()
        dates = list(load_dict.keys())[0:30][::-1]
        opens=[]
        closes=[]
        highs =[]
        lows=[]
        for date in dates:
            opens.append(float(load_dict[date]['open']))
            closes.append(float(load_dict[date]['close']))
            highs.append(float(load_dict[date]['high']))
            lows.append(float(load_dict[date]['low']))
        return {"date":dates,"open": opens,"high":highs,"low":lows,"close":closes}
    except:
        return None






def get_trade_day():
    return "2019-11-25"