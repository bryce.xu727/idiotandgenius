
class Position(object):
    """docstring for ClassName"""
    def __init__(self, position_id, symbol, purchase_time, purchase_price, position_size):
        self.position_id = position_id
        self.symbol = symbol
        self.purchase_time = purchase_time
        self.purchase_price = purchase_price
        self.number_of_shares = position_size
