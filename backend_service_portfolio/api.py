from flask import Flask
from flask_restful import Resource, Api, reqparse
import datetime, json
from flask_jwt_extended import JWTManager, jwt_required, get_jwt_identity
from system.db.position_collection import PositionCollection
from system.db.stock_holiding_collection import StockHoldingCollection
from portfolio_resource import *
from user_resource import *
from stock_resource import *
#from ai_resource import *
from utils import *
import system.portfolio_manager
import system.stock
from system.db.import_data_to_db import Importer
from flask_cors import CORS

# config the app service
app = Flask(__name__)
app.config['JWT_SECRET_KEY'] = 'Genius user'
api = Api(app)
jwt = JWTManager(app)
CORS(app)

# Initialise the mongo collection
position_collection = PositionCollection()
stock_holding_collection = StockHoldingCollection()

class positions(Resource):
    @jwt_required
    def get(self, username, portfolio_id):
        if validate_user(username) is False:
            return not_authorized_message, 401
        args = parser.parse_args()
        symbol = args.get("symbol") 
        result = None
        if symbol:
            result = position_collection.get_position_group_by_symbol(portfolio_id, symbol)
        else:
            result = position_collection.get_all_position(portfolio_id)
        
        if result:            
            return result
        return {"message": "position not found"}, 404                                       

class position(Resource):
    @jwt_required
    def get(self, username, portfolio_id, position_id):
        if validate_user(username) is False:
            return not_authorized_message, 401           
        result = position_collection.get_position_metadata(position_id)    
        if result:
            return result
        return {"message": "position not found"}, 404

    @jwt_required
    def delete(self, username, portfolio_id, position_id):
        if validate_user(username) is False:
            return not_authorized_message, 401        
        args = parser.parse_args()
        symbol = args.get("symbol")
        position_size = args.get('positionSize')
        purchase_price = args.get('purchasePrice')
        portfolio = system.portfolio.Portfolio(portfolio_id)
        portfolio.initialize() 
        if portfolio.delete_position(position_id):
            return {"message": "delete position success"}, 200
        else:
            return {"message": "position not found"}, 404    

class stock_holding(Resource):
    @jwt_required
    def get(self, username, portfolio_id, symbol):
        if validate_user(username) is False:
            return not_authorized_message, 401        
        result = stock_holding_collection.collection.find({
            "portfolio": portfolio_id,
            "symbol": symbol
        }).sort("time", -1).limit(1)
        for i in result:
            return {
                "portfolio": i['portfolio'], 
                "symbol": i["symbol"],
                "size": i["size"],
                "time": i["time"]
            }, 200
        else:
            return stock_not_found_message, 404

api.add_resource(user_portfolio, 
'/<string:username>/portfolio')

api.add_resource(user_equity_allocation, 
'/<string:username>/equityallocation')

api.add_resource(user_gain, 
'/<string:username>/gain')

api.add_resource(user_gain_tolist, 
'/<string:username>/gainlist')

api.add_resource(user_volatility_table, 
'/<string:username>/volatilitytable')

api.add_resource(user_correlation_table, 
'/<string:username>/correlationtable')

api.add_resource(pair_name_id,
'/<string:username>/pair')

api.add_resource(add_stock_precheck,
'/users/<string:username>/symbols/<string:symbol>')

api.add_resource(user_curve_data, 
'/<string:username>/curvedata')

api.add_resource(portfolio, 
'/<string:username>/portfolio/<string:portfolio_id>')

api.add_resource(portfolio_stock_watch_list, 
'/<string:username>/portfolio/<string:portfolio_id>/stockwatchlist')

api.add_resource(portfolio_position_list, 
'/<string:username>/portfolio/<string:portfolio_id>/position')

api.add_resource(portfolio_stock, 
'/<string:username>/portfolio/<string:portfolio_id>/stockwatchlist/<string:symbol>')

api.add_resource(portfolio_gain, 
'/<string:username>/portfolio/<string:portfolio_id>/gain')

api.add_resource(portfolio_position_gain, 
'/<string:username>/portfolio/<string:portfolio_id>/positiongain')

api.add_resource(portfolio_volatility, 
'/<string:username>/portfolio/<string:portfolio_id>/volatility')

api.add_resource(portfolio_curve_data, 
'/<string:username>/portfolio/<string:portfolio_id>/curvedata')

api.add_resource(portfolio_equity_allocation, 
'/<string:username>/portfolio/<string:portfolio_id>/equityallocation')

api.add_resource(stock_holding, 
'/<string:username>/portfolio/<string:portfolio_id>/stockholding/<string:symbol>')

api.add_resource(positions, 
'/<string:username>/portfolio/<string:portfolio_id>/position')

api.add_resource(position, 
'/<string:username>/portfolio/<string:portfolio_id>/position/<string:position_id>')

api.add_resource(stock_data, 
'/<string:symbol>/stockdata')

api.add_resource(stock_company_data, 
'/<string:symbol>/stockcompanydata')

api.add_resource(price_data, 
'/<string:symbol>/pricedata')



if __name__ == '__main__':
    di=Importer()
    di.init_db()
    app.run(debug=True,port=9222)

