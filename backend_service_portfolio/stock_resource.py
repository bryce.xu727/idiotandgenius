from flask import Flask
from flask_restful import Resource
from flask_jwt_extended import jwt_required, get_jwt_identity
from error import *
from utils import *
import system.portfolio

class stock_data(Resource):
    def get(self, symbol):
        stock = system.stock.Stock(symbol)
        stock.get_stock_data()
        todayHigh = stock.today_high
        if todayHigh:    
            stock_data = {
                "bestBid" : stock.best_bid,
                "bestAsk" : stock.best_ask,
                "todayHigh" : todayHigh,
                "todayLow" : stock.today_low,
                "shareVolume": stock.share_volume,
                "fiftyDayAvgDailyVolumn" : stock.fifty_day_avg_daily_volume,
                "previousClose" : stock.previous_close,
                "fiftyTwoWeekHigh": stock.fifty_two_week_high,
                "fiftyTwoWeekLow" : stock.fifty_two_weekLow
            }
            return stock_data, 200
        else:
            return stock_not_found_message, 404
    
class stock_company_data(Resource):
    def get(self, symbol):
        stock = system.stock.Stock(symbol)
        stock.get_company_data()     
        company_name = stock.company_name     
        if company_name:
            stock_company_data = {
                "companyName" : company_name,
                "IPO year" : stock.ipo_year,
                "industry" : stock.industry,
                "sector" : stock.sector,
                "marketCap" : stock.market_cap,
                "annualize dividend" : stock.annualized_dividend,
                "ex-dividend data" : stock.ex_dividend_date,
                "dividend payment date" : stock.dividend_payment_date,
                "current yield" : stock.current_yield
            }           
            return stock_company_data, 200
        
        return stock_not_found_message, 404
        
class price_data(Resource):
    def get(self, symbol):
        stock = system.stock.Stock(symbol)
        stock.get_daily_price(symbol)
        daily_price = stock.daily_price
        if daily_price:
            price_data = {
                "daily price": daily_price
            }
            return price_data, 200
        
        return stock_not_found_message, 404